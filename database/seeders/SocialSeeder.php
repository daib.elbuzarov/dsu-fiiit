<?php

namespace Database\Seeders;

use App\Models\Social;
use App\Models\Types;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SocialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Представители
        DB::table('socials')->insert([
            ['status_id' => Social::active(),
                'type_id' => Types::getID(Types::CODE_DEAN, Social::table),
                "leader_post" => "Студенческий декан",
                "leader_name" => "Саид Саидов",
                'purpose' => ' Цель СНО - повышение качества подготовки студентов в соответствии с современными требованиями к высшему образованию, эффективному развитию и использованию творческого потенциала студентов в форме научно-исследовательской работы.', 'enter' => "<span>Хочу в Студдеканат!</span>

    					Чтобы вступить в студдеканат, достаточно заполнить форму <a href=\"#\">здесь</a>.",
                "postscript" => "<span>Следите за новостями!</span>

    					Подписывайтесь на <a href=\"#\">нас в Instagram</a> и будьте в курсе всех новостей и событий."],
            ['status_id' => Social::active(), 'type_id' => Types::getID(Types::CODE_KDM, Social::table),
                "leader_post" => "ПРЕДСЕДАТЕЛЬ КДМ ДГУ", "leader_name" => "Асхабали Омаров",
                'purpose' => 'Цель СНО - повышение качества подготовки студентов в соответствии с современными требованиями к высшему образованию, эффективному развитию и использованию творческого потенциала студентов в форме научно-исследовательской работы.', 'enter' => "<span>Как вступить в КДМ IT-факультета?</span>

                        Чтобы вступить в КДМ, достаточно заполнить форму <a href=\"#\">здесь</a>.",
                "postscript" => "               <span>Будьте в курсе!</span>

                       Подписывайтесь на <a href=\"#\">КДМ в Instagram</a> и не пропускайте интересные события ;)"],
            ['status_id' => Social::active(),
                'type_id' => Types::getID(Types::CODE_UNION, Social::table),
                "leader_post" => "Председатель Профбюро ФИиИТ",
                "leader_name" => "Мурад Абдурахманов",
                'purpose' => 'Цель СНО - повышение качества подготовки студентов в соответствии с современными требованиями к высшему образованию, эффективному развитию и использованию творческого потенциала студентов в форме научно-исследовательской работы.',
                'enter' => "	<span>Как вступить в Профбюро IT-факультета?</span>

    					Достаточно заполнить форму <a href=\"#\">здесь</a>.",
                "postscript" => "<span>Следите за новостями!</span>

    					Подписывайтесь на <a href=\"#\">Профком в Instagram</a> и будьте в курсе всех новостей и событий от Профсоюзного комитета."],

        ]);
    }
}
