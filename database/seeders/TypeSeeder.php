<?php

namespace Database\Seeders;

use App\Models\Club;
use App\Models\Contest;
use App\Models\Course;
use App\Models\Document;
use App\Models\Exam;
use App\Models\FacultySenate;
use App\Models\Lesson;
use App\Models\Post;
use App\Models\Program;
use App\Models\Servant;
use App\Models\Social;
use App\Models\Types;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Types::truncate();
        Schema::enableForeignKeyConstraints();

        // Типы записей
        DB::table('types')->insert([
            ['name' => 'Пользователь', 'code' => Types::CODE_USER, 'table_name' => User::table],
            ['name' => 'Администратор', 'code' => Types::CODE_ADMINISTRATOR, 'table_name' => User::table],
            ['name' => 'temp', 'code' => Types::CODE_ADMINISTRATOR, 'table_name' => 'temp'],
            ['name' => 'temp', 'code' => Types::CODE_ADMINISTRATOR, 'table_name' => 'temp'],
            ['name'=>'Проект','code'=>Types::CODE_PROJECT,'table_name'=>Club::table],
            ['name'=>'Клуб','code'=>Types::CODE_CLUB,'table_name'=>Club::table],
            ['name'=>'Кружок','code'=>Types::CODE_CIRCLE,'table_name'=>Club::table],
            ['name'=>'Конкурс','code'=>Types::CODE_CONTEST,'table_name'=>Contest::table],
            ['name'=>'Грант','code'=>Types::CODE_GRANT,'table_name'=>Contest::table],
            ['name'=>'Структура Студенческого деканата','code'=>Types::CODE_DEAN,'table_name'=>Servant::table],
            ['name'=>'Структура кдм фииит','code'=>Types::CODE_KDM,'table_name'=>Servant::table],
            ['name'=>'руководители комиссий','code'=>Types::CODE_COMMISSION,'table_name'=>Servant::table],
            ['name'=>'Структура СНО IT-факультета','code'=>Types::CODE_SNO,'table_name'=>Servant::table],
            ['name'=>'сОСТАВ СОВЕТА СМУ ФИИИТ','code'=>Types::CODE_SMU,'table_name'=>Servant::table],
            ['name'=>'Профсоюзный комитет','code'=>Types::CODE_UNION,'table_name'=>Servant::table],
            ['name'=>'Новость','code'=>Types::CODE_NEWS,'table_name'=>Post::table],
            ['name'=>'Объявление','code'=>Types::CODE_AD,'table_name'=>Post::table],
            ['name'=>'Статья','code'=>Types::CODE_ARTICLE,'table_name'=>Post::table],
            ['name'=>'Совет факультета','code'=>Types::CODE_FACULTY_SENATE,'table_name'=>FacultySenate::table],
            ['name'=>'Учебно-методический совет','code'=>Types::CODE_STUDY_SENATE,'table_name'=>FacultySenate::table],
            ['name'=>'Бакалавриат','code'=>Types::CODE_BACHELOR,'table_name'=>Course::table],
            ['name'=>'Магистратура','code'=>Types::CODE_MASTER,'table_name'=>Course::table],
            ['name'=>'Бакалавриат','code'=>Types::CODE_BACHELOR,'table_name'=>Program::table],
            ['name'=>'Магистратура','code'=>Types::CODE_MASTER,'table_name'=>Program::table],
            ['name'=>'Документ-ссылка','code'=>Types::CODE_DOC_LINK,'table_name'=>Document::table],
            ['name'=>'Документ-файл','code'=>Types::CODE_DOC_FILE,'table_name'=>Document::table],
            ['name'=>'Бакалавриат очная','code'=>Types::CODE_BACHELOR_FULL,'table_name'=>Lesson::table],
            ['name'=>'Магистратура очная','code'=>Types::CODE_CODE_MASTER_FULL,'table_name'=>Lesson::table],
            ['name'=>'Бакалавриат заочная','code'=>Types::CODE_BACHELOR_DIST,'table_name'=>Lesson::table],
            ['name'=>'Магистратура заочная','code'=>Types::CODE_CODE_MASTER_DIST,'table_name'=>Lesson::table],
            ['name'=>'Бакалавриат очная','code'=>Types::CODE_BACHELOR_FULL,'table_name'=>Exam::table],
            ['name'=>'Магистратура очная','code'=>Types::CODE_CODE_MASTER_FULL,'table_name'=>Exam::table],
            ['name'=>'Бакалавриат заочная','code'=>Types::CODE_BACHELOR_DIST,'table_name'=>Exam::table],
            ['name'=>'Магистратура заочная','code'=>Types::CODE_CODE_MASTER_DIST,'table_name'=>Exam::table],
            ['name'=>'Студенческий деканат','code'=>Types::CODE_DEAN,'table_name'=>Social::table],
            ['name'=>'Комитет по делам молодежи','code'=>Types::CODE_KDM,'table_name'=>Social::table],
            ['name'=>'Профсоюзный комитет','code'=>Types::CODE_UNION,'table_name'=>Social::table],
            ['name'=>'Бакалавриат очно-заочная','code'=>Types::CODE_BACHELOR_HALF,'table_name'=>Lesson::table],
            ['name'=>'Магистратура очно-заочная','code'=>Types::CODE_CODE_MASTER_HALF,'table_name'=>Lesson::table],
            ['name'=>'Бакалавриат очно-заочная','code'=>Types::CODE_BACHELOR_HALF,'table_name'=>Exam::table],
            ['name'=>'Магистратура очно-заочная','code'=>Types::CODE_CODE_MASTER_HALF,'table_name'=>Exam::table],
        ]);
    }
}
