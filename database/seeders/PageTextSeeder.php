<?php

namespace Database\Seeders;

use App\Models\PageText;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

// Текста и баннеры на сайте
class PageTextSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Главная
        DB::table('page_texts')->insert([
            ['status_id'=> PageText::active(),'title'=>'Хочу здесь учиться','url' => '/', 'name' => 'want_study', 'value' => "", 'link' => 'https://google.com'],
            ['status_id'=> PageText::active(),'title'=>'Баннер - есть, что рассказать','url' => '/', 'name' => 'main_banner', 'value' => "есть, что рассказать", 'link' => 'https://google.com'],
            ['status_id'=> PageText::active(),'title'=>'Инстаграмм банер','url' => '/', 'name' => 'inst_banner', 'value' => "Подписывайтесь на наш Instagram!", 'link' => 'https://google.com'],
            ['status_id'=> PageText::active(),'title'=>'Задать вопрос','url' => '/', 'name' => 'feedback', 'value' => "По вопросам поступления, сотрудничества и предложений мы ждем ваших писем на <span>fiiitdsu@gmail.com</span>", 'link' => ''],

        ]);


        // Структура
        DB::table('page_texts')->insert([
            ['status_id'=> PageText::active(),'title'=>'Декан имя','url' => 'structure', 'name' => 'dean_name', 'value' => " Исмиханов", 'link' => ''],
            ['status_id'=> PageText::active(),'title'=>'Декан фамилия','url' => 'structure', 'name' => 'dean_surname', 'value' => "Заур", 'link' => ''],
            ['status_id'=> PageText::active(),'title'=>'Декан отчество','url' => 'structure', 'name' => 'dean_last_name', 'value' => "Намединович", 'link' => ''],
            ['status_id'=> PageText::active(),'title'=>'Декан инфо','url' => 'structure', 'name' => 'dean_info', 'value' => "Кандидат экономических наук, доцент, профессор Российской Академий Естествознания ", 'link' => ''],
        ]);

        // Абитуриентам
        DB::table('page_texts')->insert([
            ['status_id'=> PageText::active(),'title'=>'Хочу здесь учится','url' => 'candidate', 'name' => 'read', 'value' => "Подробнее ознакомиться с образовательными программами можно <a href=\"https://google.com\" target=\"_blank\"><b>здесь.</b></a>", 'link' => ''],
            ['status_id'=> PageText::active(),'title'=>'Бакалавр правила','url' => 'candidate', 'name' => 'bachelor_rules', 'value' => "<a href=\"https://google.com\" target=\"_blank\"><p>Правила<br> приема</p></a>", 'link' => '#'],
            ['status_id'=> PageText::active(),'title'=>'Бакалавр цена','url' => 'candidate', 'name' => 'bachelor_cost', 'value' => "<a href=\"https://google.com\" target=\"_blank\"><p>Стоимость <br> обучения</p></a>", 'link' => '#'],
            ['status_id'=> PageText::active(),'title'=>'Бакалавр бланки','url' => 'candidate', 'name' => 'bachelor_blanks', 'value' => "<a href=\"https://google.com\" target=\"_blank\"><p>Бланки <br> документов</p></a>", 'link' => '#'],
            ['status_id'=> PageText::active(),'title'=>'Магистр правила','url' => 'candidate', 'name' => 'master_rules', 'value' => "<a href=\"https://google.com\" target=\"_blank\"><p>Правила<br> приема</p></a>", 'link' => '#'],
            ['status_id'=> PageText::active(),'title'=>'Магистр цена','url' => 'candidate', 'name' => 'master_cost', 'value' => "<a href=\"https://google.com\" target=\"_blank\"><p>Стоимость <br> обучения</p></a>", 'link' => '#'],
            ['status_id'=> PageText::active(),'title'=>'Магистр бланки','url' => 'candidate', 'name' => 'master_blanks', 'value' => "<a href=\"https://google.com\" target=\"_blank\"><p>Правила<br> приема</p></a>", 'link' => '#'],

        ]);

        // Студентам
        DB::table('page_texts')->insert([
            ['status_id'=> PageText::active(),'title'=>'ЕОР ИНФО','url' => 'student/resources', 'name' => 'eor_info', 'value' => " ЭОР ДГУ - это система нормативной и учебно-методической документации, средств обучения и контроля, необходимых и достаточных для качественной организации основных и дополнительных образовательных программ, согласно учебного плана.", 'link' => '#'],
            ['status_id'=> PageText::active(),'title'=>'ЕОР ссылка','url' => 'student/resources', 'name' => 'eor_link', 'value' => " Для перехода в ЭОР ДГУ кликните по ссылке:<br><br> <a class=\"resources__link\" target=\"_blank\" href=\"http://umk.dgu.ru/\">http://umk.dgu.ru/</a>", 'link' => '#'],
            ['status_id'=> PageText::active(),'title'=>'Библиотека инфо','url' => 'student/resources', 'name' => 'lib_info', 'value' => " Научная библиотека ДГУ им. А. А. Абилова функционально ориентирована на обеспечение
                                образовательной деятельности, является информационным структурным подразделением
                                университета.", 'link' => '#'],
            ['status_id'=> PageText::active(),'title'=>'Библиотека ссылка','url' => 'student/resources', 'name' => 'lib_link', 'value' => "Для перехода на сайт научной бибилиотеки кликните по ссылке: <br> <a
                                        class=\"resources__link\" href=\"http://elib.dgu.ru/\"> http://elib.dgu.ru/</a>", 'link' => '#'],
        ]);

        // Научная деятельность
        DB::table('page_texts')->insert([
            ['status_id'=> PageText::active(),'title'=>'Текст слева СНО','url' => 'science/sno', 'name' => 'left_article', 'value' => "Это добровольное общественное объединение студентов ДГУ, активно занимающихся научно-исследовательской деятельностью.", 'link' => '#'],
            ['status_id'=> PageText::active(),'title'=>'Текст справа СНО','url' => 'science/sno', 'name' => 'right_article', 'value' => "Целью существования СНО является повышение качества получения высшего образования студентов, повышение эффективности развития и использования творческого потенциала студентов в форме научно-исследовательской работы.", 'link' => '#'],
            ['status_id'=> PageText::active(),'title'=>'Текст слева СМУ','url' => 'science/smu', 'name' => 'left_article', 'value' => "  Совет молодых ученых Дагестанского государственного университета (СМУ ДГУ)
                 представляет интересы студентов, аспирантов и преподавателей ДГУ –
                кандидатов и докторов наук в возрасте до 40 лет в сфере их научной деятельности.", 'link' => '#'],
            ['status_id'=> PageText::active(),'title'=>'Текст справа СМУ','url' => 'science/smu', 'name' => 'right_article', 'value' => " Деятельность осуществляется в соответствии с федеральным законом
                 «Об образовании в РФ», Уставом ДГУ и на основании <a href=\"#\" style=\"color: black ;\"> <strong>Положения.</strong></a>", 'link' => '#'],
            ['status_id'=> PageText::active(),'title'=>'Проекты баннер','url' => 'science/projects', 'name' => 'banner', 'value' => "", 'link' => 'https://google.com'],
        ]);


    }
}
