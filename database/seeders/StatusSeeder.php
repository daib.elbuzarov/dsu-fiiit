<?php

namespace Database\Seeders;

use App\Models\Banner;
use App\Models\Club;
use App\Models\Contest;
use App\Models\Course;
use App\Models\CourseSector;
use App\Models\Department;
use App\Models\DeputyDirector;
use App\Models\Document;
use App\Models\Exam;
use App\Models\FacultySenate;
use App\Models\JoinImg;
use App\Models\Lesson;
use App\Models\Navigator;
use App\Models\PageText;
use App\Models\Post;
use App\Models\Program;
use App\Models\Section;
use App\Models\SectorItem;
use App\Models\Servant;
use App\Models\Social;
use App\Models\Status;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();
        Status::truncate();
        Schema::enableForeignKeyConstraints();

        DB::table('status')->insert([
            ['text' => 'Кружок активен', 'code' => Status::CODE_ACTIVE, 'table_name' => Club::table],
            ['text' => 'Кружок не активен', 'code' => Status::CODE_NOACTIVE, 'table_name' => Club::table],
            ['text' => 'Конкурс активен', 'code' => Status::CODE_ACTIVE, 'table_name' => Contest::table],
            ['text' => 'Конкурс не активен', 'code' => Status::CODE_NOACTIVE, 'table_name' => Contest::table],
            ['text' => 'Документ активен', 'code' => Status::CODE_ACTIVE, 'table_name' => Document::table],
            ['text' => 'Документ не активен', 'code' => Status::CODE_NOACTIVE, 'table_name' => Document::table],
            ['text' => 'Связка активна', 'code' => Status::CODE_ACTIVE, 'table_name' => JoinImg::table],
            ['text' => 'Связка не активна', 'code' => Status::CODE_NOACTIVE, 'table_name' => JoinImg::table],
            ['text' => 'Новость активна', 'code' => Status::CODE_ACTIVE, 'table_name' => Post::table],
            ['text' => 'Новость не активна', 'code' => Status::CODE_NOACTIVE, 'table_name' => Post::table],
            ['text' => 'Текст активен', 'code' => Status::CODE_ACTIVE, 'table_name' => PageText::table],
            ['text' => 'Текст не активен', 'code' => Status::CODE_NOACTIVE, 'table_name' => PageText::table],
            ['text' => 'Образец документа активен', 'code' => Status::CODE_ACTIVE, 'table_name' => Section::table],
            ['text' => 'Образец документа не активен', 'code' => Status::CODE_NOACTIVE, 'table_name' => Section::table],
            ['text' => 'Образец документа активен', 'code' => Status::CODE_ACTIVE, 'table_name' => Section::table],
            ['text' => 'Образец документа не активен', 'code' => Status::CODE_NOACTIVE, 'table_name' => Section::table],
            ['text' => 'Работник активен', 'code' => Status::CODE_ACTIVE, 'table_name' => Servant::table],
            ['text' => 'Работник не активен', 'code' => Status::CODE_NOACTIVE, 'table_name' => Servant::table],
            ['text' => 'Кафедра активна', 'code' => Status::CODE_ACTIVE, 'table_name' => Department::table],
            ['text' => 'Кафедра не активна', 'code' => Status::CODE_NOACTIVE, 'table_name' => Department::table],
            ['text' => 'Зам. декана активен', 'code' => Status::CODE_ACTIVE, 'table_name' => DeputyDirector::table],
            ['text' => 'Зам. декана не активен', 'code' => Status::CODE_NOACTIVE, 'table_name' => DeputyDirector::table],
            ['text' => 'Совет факультета активен', 'code' => Status::CODE_ACTIVE, 'table_name' => FacultySenate::table],
            ['text' => 'Совет факультета не активен', 'code' => Status::CODE_NOACTIVE, 'table_name' => FacultySenate::table],
            ['text' => 'Направление активно', 'code' => Status::CODE_ACTIVE, 'table_name' => Course::table],
            ['text' => 'Направление не активно', 'code' => Status::CODE_NOACTIVE, 'table_name' => Course::table],
            ['text' => 'Связка направления активна', 'code' => Status::CODE_ACTIVE, 'table_name' => CourseSector::table],
            ['text' => 'Связка направления не активна', 'code' => Status::CODE_NOACTIVE, 'table_name' => CourseSector::table],
            ['text' => 'Запись подраздела направления активна', 'code' => Status::CODE_ACTIVE, 'table_name' => SectorItem::table],
            ['text' => 'Запись подраздела направления не активна', 'code' => Status::CODE_NOACTIVE, 'table_name' => SectorItem::table],
            ['text' => 'Расписание занятий активно', 'code' => Status::CODE_ACTIVE, 'table_name' => Lesson::table],
            ['text' => 'Расписание занятий не активно', 'code' => Status::CODE_NOACTIVE, 'table_name' => Lesson::table],
            ['text' => 'Расписание сессий активно', 'code' => Status::CODE_ACTIVE, 'table_name' => Exam::table],
            ['text' => 'Расписание сессий не активно', 'code' => Status::CODE_NOACTIVE, 'table_name' => Exam::table],
            ['text' => 'Социальная работа активна', 'code' => Status::CODE_ACTIVE, 'table_name' => Social::table],
            ['text' => 'Социальная работа не активна', 'code' => Status::CODE_NOACTIVE, 'table_name' => Social::table],
            ['text' => 'Пользователь активен', 'code' => Status::CODE_ACTIVE, 'table_name' => User::table],
            ['text' => 'Пользователь не активен', 'code' => Status::CODE_NOACTIVE, 'table_name' => User::table],
            ['text' => 'Навигатор активен', 'code' => Status::CODE_ACTIVE, 'table_name' => Navigator::table],
            ['text' => 'Навигатор не активен', 'code' => Status::CODE_NOACTIVE, 'table_name' => Navigator::table],
            ['text' => 'Обр. программа активна', 'code' => Status::CODE_ACTIVE, 'table_name' => Program::table],
            ['text' => 'Обр. программа не активна', 'code' => Status::CODE_NOACTIVE, 'table_name' => Program::table],
            ['text' => 'Баннер активен', 'code' => Status::CODE_ACTIVE, 'table_name' => Banner::table],
            ['text' => 'Баннер не активен', 'code' => Status::CODE_NOACTIVE, 'table_name' => Banner::table],


        ]);

        // Статусы изображений
        DB::table('status')->insert([
            ['text' => 'Маленькое изображение', 'code' => Status::CODE_SMALL_IMG, 'table_name' => JoinImg::table],
            ['text' => 'Среднее изображение', 'code' => Status::CODE_MIDDLE_IMG, 'table_name' => JoinImg::table],
            ['text' => 'Оригинальное изображение', 'code' => Status::CODE_ORIGINAL_IMG, 'table_name' => JoinImg::table],
            ['text' => 'Главное изображение', 'code' => Status::CODE_HEAD_IMG, 'table_name' => JoinImg::table],
            ['text' => 'Слайдер', 'code' => Status::CODE_SLIDER_IMG, 'table_name' => JoinImg::table],
        ]);
    }
}
