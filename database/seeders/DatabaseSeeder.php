<?php

namespace Database\Seeders;

use App\Models\Types;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            StatusSeeder::class,
            TypeSeeder::class,
            PageTextSeeder::class,
            SocialSeeder::class,
            BannerSeeder::class
        ]);

        // Тестовый администратор admin@test.ru 12345678
        DB::table('users')->insert([
            ['type_id' => Types::getID(Types::CODE_ADMINISTRATOR, User::table), 'name' => 'admin', 'email' => 'admin@test.ru', 'password' => bcrypt("12345678"), 'status_id' => User::active()]
        ]);

    }
}
