<?php

namespace Database\Seeders;

use App\Models\Banner;
use App\Models\JoinImg;
use App\Models\Status;
use Illuminate\Database\Seeder;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $id = Banner::createBanner([
        'url'=>'/',
        'name'=>'main_banner',
        'title'=>'Баннер главная',
        'link'=>'www.google.com',
      ]);

      JoinImg::saveImg('/icons/index/inst-banner.svg',"banner.glavnaya",$id,Status::CODE_ORIGINAL_IMG,Status::CODE_HEAD_IMG,Banner::table);
      JoinImg::saveImg('/icons/index/inst-banner.svg',"banner.glavnaya",$id,Status::CODE_SMALL_IMG,Status::CODE_HEAD_IMG,Banner::table);

      $id = Banner::createBanner([
        'url'=>'/',
        'name'=>'footer_banner',
        'title'=>'Баннер главная футер',
        'link'=>'www.google.com',
      ]);

      JoinImg::saveImg('/icons/index/fiiit_banner.svg',"banner.footer",$id,Status::CODE_ORIGINAL_IMG,Status::CODE_HEAD_IMG,Banner::table);
      JoinImg::saveImg('/icons/index/fiiit_banner2.svg',"banner.footer",$id,Status::CODE_SMALL_IMG,Status::CODE_HEAD_IMG,Banner::table);

      $id = Banner::createBanner([
        'url'=>'science/projects',
        'name'=>'science_banner',
        'title'=>'Баннер Проектная деятельность',
        'link'=>'www.google.com',
      ]);

      JoinImg::saveImg('/icons/science/banner.jpg',"banner.projects",$id,Status::CODE_ORIGINAL_IMG,Status::CODE_HEAD_IMG,Banner::table);
      JoinImg::saveImg('/icons/science/banner-mob.jpg',"banner.projects",$id,Status::CODE_SMALL_IMG,Status::CODE_HEAD_IMG,Banner::table);

    }
}
