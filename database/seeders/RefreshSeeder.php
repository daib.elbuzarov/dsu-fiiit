<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RefreshSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            StatusSeeder::class,
            TypeSeeder::class,
        ]);
    }
}
