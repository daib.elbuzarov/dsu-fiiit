<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNavigatorTable extends Migration
{
    public function up()
    {
        Schema::create('navigators', function (Blueprint $table) {

            $table->id();
            $table->foreignId('type_id')->constrained('types');
            $table->integer('parent_id')->nullable();
            $table->string('title');
            $table->integer('position')->nullable();
            $table->string('link')->nullable();
            //$table->integer('system_id');
            $table->foreignId('status_id')->constrained('status');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

        });
    }

    public function down()
    {
        Schema::dropIfExists('navigator');
    }
}
