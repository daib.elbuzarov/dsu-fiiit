<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageTextsTable extends Migration
{
    public function up()
    {
        Schema::create('page_texts', function (Blueprint $table) {

            $table->id();
            $table->string('url');
            $table->string('name');
            $table->string('title')->nullable();
            $table->string('link')->nullable();
            $table->text('value')->nullable();
            $table->foreignId('status_id')->constrained('status');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

        });
    }

    public function down()
    {
        Schema::dropIfExists('page_texts');
    }
}