<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContestsTable extends Migration
{
    public function up()
    {
        Schema::create('contests', function (Blueprint $table) {

            $table->id();
            $table->integer('type_id');
            $table->string('title');
            $table->string('tag')->nullable();
            $table->string('author')->nullable();
            $table->string('url')->nullable();
            $table->string('prize')->nullable();
            $table->text('description')->nullable();
            $table->timestamp('date_end')->nullable();
            $table->integer('status_id');
            $table->timestamps();


        });
    }

    public function down()
    {
        Schema::dropIfExists('contests');
    }
}
