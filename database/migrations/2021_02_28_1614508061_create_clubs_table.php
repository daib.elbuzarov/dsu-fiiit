<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClubsTable extends Migration
{
    public function up()
    {
        Schema::create('clubs', function (Blueprint $table) {

            $table->id();
            $table->integer('type_id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('author')->nullable();
            $table->integer('status_id');
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::dropIfExists('clubs');
    }
}
