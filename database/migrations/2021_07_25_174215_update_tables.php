<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deputy_directors', function (Blueprint $table) {
            $table->string('description')->nullable()->change();
            $table->integer('status_id')->nullable()->change();
        });

        Schema::table('departments', function (Blueprint $table) {
            $table->string('link')->nullable()->change();
        });
        Schema::table('faculty_senate', function (Blueprint $table) {
            $table->integer('type_id')->nullable()->change();
            $table->text('text')->nullable()->change();
            $table->text('link')->nullable()->change();
        });
        Schema::table('courses', function (Blueprint $table) {
            $table->text('description')->nullable()->change();
            $table->integer('status_id')->nullable()->change();
        });
        Schema::table('course_sectors', function (Blueprint $table) {
            $table->string('number')->nullable()->change();
            $table->integer('status_id')->nullable()->change();
        });
        Schema::table('clubs', function (Blueprint $table) {
            $table->text('description')->nullable()->change();
            $table->string('author')->nullable()->change();
        });
        Schema::table('clubs', function (Blueprint $table) {
            $table->text('description')->nullable()->change();
            $table->string('author')->nullable()->change();
        });
        Schema::table('contests', function (Blueprint $table) {
            $table->string('tag')->nullable()->change();
            $table->string('author')->nullable()->change();
            $table->string('url')->nullable()->change();
            $table->string('prize')->nullable()->change();
            $table->text('description')->nullable()->change();
        });
        Schema::table('posts', function (Blueprint $table) {
            $table->string('tag')->nullable()->change();
            $table->text('description')->nullable()->change();
            $table->integer('status_id')->nullable()->change();
        });
        Schema::table('sections', function (Blueprint $table) {
            $table->integer('status_id')->nullable()->change();
        });
        Schema::table('servants', function (Blueprint $table) {
            $table->string('work_type')->nullable()->change();
            $table->text('description')->nullable()->change();
            $table->integer('status_id')->nullable()->change();
        });
        Schema::table('documents', function (Blueprint $table) {
            $table->string('url')->nullable()->change();
        });
        Schema::table('navigators', function (Blueprint $table) {
            $table->integer('parent_id')->nullable()->change();
            $table->integer('position')->nullable()->change();
            $table->string('link')->nullable()->change();
        });
        Schema::table('page_texts', function (Blueprint $table) {
            $table->string('title')->nullable()->change();
            $table->string('link')->nullable()->change();
            $table->text('value')->nullable()->change();
        });
        Schema::table('programs', function (Blueprint $table) {
            $table->text('description')->nullable()->change();
        });
        Schema::table('lessons', function (Blueprint $table) {
            $table->string('file')->nullable()->change();
            $table->string('url')->nullable()->change();
        });
        Schema::table('exams', function (Blueprint $table) {
            $table->string('file')->nullable()->change();
            $table->string('url')->nullable()->change();
        });
        Schema::table('sector_items', function (Blueprint $table) {
            $table->string('file')->nullable()->change();
            $table->string('url')->nullable()->change();
        });
        Schema::table('banners', function (Blueprint $table) {
            $table->string('url')->nullable()->change();
            $table->string('link')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
