<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {

            $table->id();
            $table->string('title');
            $table->foreignId('section_id')->constrained();
            $table->foreignId('type_id')->constrained();
            $table->string('url')->nullable();
            $table->integer('status_id');
            $table->timestamps();


        });
    }

    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
