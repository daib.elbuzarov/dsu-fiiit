<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJoinImgTable extends Migration
{
    public function up()
    {
        Schema::create('join_img', function (Blueprint $table) {

            $table->id();
            $table->integer('item_id');
            $table->integer('img_id');
            $table->integer('img_res_code');
            $table->integer('img_head_code');
            $table->string('table_name');
            $table->foreignId('status_id')->constrained('status');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    public function down()
    {
        Schema::dropIfExists('join_img');
    }
}