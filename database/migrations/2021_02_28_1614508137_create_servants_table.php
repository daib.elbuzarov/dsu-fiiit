<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServantsTable extends Migration
{
    public function up()
    {
        Schema::create('servants', function (Blueprint $table) {

            $table->id();
            $table->integer('type_id');
            $table->string('title');
            $table->string('work_type')->nullable();
            $table->text('description')->nullable();
            $table->integer('status_id')->nullable();
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::dropIfExists('servants');
    }
}
