<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 16.06.2020
 * Time: 17:09
 */

namespace App\Traits;

use App\Models\Banner;
use App\Models\Club;
use App\Models\Contest;
use App\Models\Course;
use App\Models\CourseSector;
use App\Models\Department;
use App\Models\DeputyDirector;
use App\Models\Exam;
use App\Models\FacultySenate;
use App\Models\Lesson;
use App\Models\Post;
use App\Models\Program;
use App\Models\Section;
use App\Models\SectorItem;
use App\Models\Servant;
use App\Models\Social;
use App\Models\Status;
use App\Models\Types;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Методы которые нужны во всех моделях
 * Trait ModelTools
 * @package App\Traits
 */
trait ModelTools
{
    /**
     * Получение записи статуса
     * @return BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Status::class, "status_id", "id");
    }

    /**
     * Скоп для исключения полей
     * https://stackoverflow.com/questions/23612221/how-to-exclude-certains-columns-while-using-eloquent/43885078
     * @param $query
     * @param array $value
     * @return mixed
     */
    public function scopeOnly($query, $value = array())
    {
        return $query->select((array)$value);
    }

    /**
     * Получение неактивного статуса
     * @param $table - таблица для которой нужно получить неактивный статус
     * @return integer - удаленный статус
     */
    public static function inactive($table = self::table)
    {
        return Status::getId(Status::CODE_NOACTIVE, $table);
    }

    /**
     * Получение активного статуса
     * @param $table - таблица для которой нужно получить активный статус
     * @return integer - активный статус
     */
    public static function active($table = self::table)
    {
        return Status::getId(Status::CODE_ACTIVE, $table);
    }

    /**
     * Получение статуса
     * @param $code
     * @return integer - активный статус
     */
    public static function getStatusId($code)
    {
        return Status::getId($code, self::table);
    }

    /**
     * Получение типа
     * @param $code
     * @return mixed|null
     */
    public static function getTypeId($code)
    {
        return Types::getID($code, self::table);
    }


    /**
     * Перевод текста в английский
     * @param $s
     * @return bool|false|mixed|string|string[]|null
     */
    public function translit($s)
    {
        $s = (string)$s; // преобразуем в строковое значение
        $s = strip_tags($s); // убираем HTML-теги
        $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
        $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
        $s = trim($s); // убираем пробелы в начале и конце строки
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
        $s = strtr($s, array('а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'j', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 'ы' => 'y', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', 'ъ' => '', 'ь' => ''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
        $s = str_replace(" ", "-", $s); // заменяем пробелы знаком минус
        return $s; // возвращаем результат
    }

    public static function fixStatuses()
    {
        $models = [
            Lesson::class,
            Contest::class,
            FacultySenate::class,
            Exam::class,
            Banner::class,
            Course::class,
            CourseSector::class,
            Department::class,
            Club::class,
            DeputyDirector::class,
            Post::class,
            Section::class,
            SectorItem::class,
            Servant::class,
            Social::class,
        ];

        foreach ($models as $model) {
            logger($model::whereNotNull('id')->update([
                'status_id'=>$model::active()
            ]));
        }

    }

}
