<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Ramsey\Collection\Collection;

/**
 * App\Models\Types
 *
 * @property int $id
 * @property string $name
 * @property int $code
 * @property string $table_name
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|Types newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Types newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Types query()
 * @method static \Illuminate\Database\Eloquent\Builder|Types whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Types whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Types whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Types whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Types whereTableName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Types whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Types extends Model
{
    use Notifiable;

    protected $fillable = array('*');

    protected $table = 'types';
    const table = 'types';


    /**
     * Стандартные коды пользователей
     */
    const CODE_ADMINISTRATOR = 1;
    const CODE_USER = 2;


    /**
     * Статусы для навигатора
     */
    const CODE_TOP = 1;
    const CODE_BOTTOM = 2;
    const CODE_MAIN = 3;

    const CODE_SYSTEM = 90;
    const CODE_NOT_SYSTEM = 91;
    const CODE_HIDDEN_SYSTEM = 92;

    /**
     * Коды клубов
     */
    const CODE_PROJECT = 1;
    const CODE_CLUB = 2;
    const CODE_CIRCLE = 3;

    /**
     * Коды конкурсов
     */
    const CODE_CONTEST = 1;
    const CODE_GRANT = 2;

    /**
     * Коды новостей
     */
    const CODE_NEWS = 1;
    const CODE_AD = 2;
    const CODE_ARTICLE = 3;

    /**
     * Коды работников
     */
    const CODE_DEAN = 1;
    const CODE_KDM = 2;
    const CODE_COMMISSION = 3;
    const CODE_SNO = 4;
    const CODE_SMU = 5;
    const CODE_UNION = 6; // Профбюро

    /**
     * Коды советов
     */
    const CODE_FACULTY_SENATE = 1;
    const CODE_STUDY_SENATE = 2;

    /**
     * Коды направлений
     */
    const CODE_BACHELOR = 1;
    const CODE_MASTER = 2;

    /**
     * Коды направлений расширенные
     */
    const CODE_BACHELOR_FULL =1;
    const CODE_CODE_MASTER_FULL =2;
    const CODE_BACHELOR_DIST=3;
    const CODE_CODE_MASTER_DIST=4;
    const CODE_CODE_MASTER_HALF=5;
    const CODE_BACHELOR_HALF=6;

    /**
     * Коды расписаний
     */
    const CODE_LESSONS = 1;
    const CODE_SESSIONS = 2;

    /**
     * Коды документов
     */
    const CODE_DOC_FILE =1;
    const CODE_DOC_LINK =2;

    /**
     * @param int $code
     * @param string $table_name
     * @return |null
     */
    public static function getID(int $code, string $table_name)
    {
        $type = Types::where([['code', $code], ['table_name', $table_name]])->first(['id']);
        return $type->id ?? null;
    }

    /**
     * @param string $table_name
     * @return |null
     */
    public static function getTypes(string $table_name)
    {
        return Types::where([['table_name', $table_name]])->get();
    }


}
