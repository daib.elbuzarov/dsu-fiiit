<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\SectorItem
 *
 * @property int $id
 * @property string $title
 * @property int $sector_id
 * @property string|null $file
 * @property string|null $url
 * @property int $status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\CourseSector|null $sector
 * @property-read \App\Models\Status $status
 * @method static \Illuminate\Database\Eloquent\Builder|SectorItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SectorItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SectorItem only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|SectorItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|SectorItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SectorItem whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SectorItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SectorItem whereSectorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SectorItem whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SectorItem whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SectorItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SectorItem whereUrl($value)
 * @mixin \Eloquent
 */
class SectorItem extends Model
{
    use ModelTools;
    const table = 'sector_items';
    protected $fillable = array('*');
    protected $table = 'sector_items';


    /**
     * Получение статуса
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status(){
        return $this->belongsTo(Status::class, 'status_id', 'id');
    }

    /**
     * Получение направления
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function sector()
    {
        return $this->hasOne(CourseSector::class, 'id', 'sector_id');
    }


    /**
     * Получение записи направления
     */
    static public function getSectorItems()
    {
        return self::where('status_id', self::active(self::table))->with([
            'status',
        ])->get();
    }


    /**
     * Получение одного записи направления
     *
     * @param $id
     *
     * @return mixed
     */
    static public function getOneSectorItem($id)
    {
        return self::where([
            ['id', $id],
            ['status_id', '<>', self::inactive(self::table)]
        ])->with([
            'status',
        ])
            ->orderBy('id', 'desc')->first();
    }


    /**
     * Получение ограниченного количества документов
     *
     * @param $count
     *
     * @return mixed
     */
    static public function getPaginateSectorItem($count)
    {
        return self::where([
            ['status_id', self::active(self::table)]
        ])->orderBy('id', 'desc')->paginate($count);
    }


    /**
     * Создание записи
     *
     * @param $array
     */
    static public function createSectorItem($array)
    {
        //Загрузка файла
        if (isset($array['file'])) {
            $path_file = public_path('/files/sector_items/');
            $path = "/files/sector_items/";
            $fileName = Str::random(30) . '.' . $array['file']->extension();
            $array['file']->move($path_file, $fileName);
            $file = $path . $fileName;

        } else if (isset($array['url'])){
            $url = $array['url'];

        }


        $sector_items = new self();

        $sector_items->title =  isset($array['title']) ? $array['title'] : null;
        $sector_items->sector_id =  isset($array['sector_id']) ? $array['sector_id'] : null;;
        $sector_items->url = isset($url) ? $url : null;
        $sector_items->file = isset($file) ? $file : null;
        $sector_items->status_id =  self::active();
        $sector_items->save();
    }


    /**
     * Редактирование записи
     *
     * @param $array
     */
    static public function editSectorItem($array)
    {
        $sector_item = self::where('id',$array['id'])->first();

        //Загрузка файла
        if (isset($array['file'])) {
            $path_file = public_path('/files/sector_items/');
            $path = "/files/sector_items/";
            $fileName = Str::random(30) . '.' . $array['file']->extension();
            $array['file']->move($path_file, $fileName);
            $file = $path . $fileName;
            //ЕСЛИ ДОБАВЛЕН ФАЙЛ
            $sector_item->file = $file;
            $sector_item->url = null;

        } else if (isset($array['url'])){
            // ЕСЛИ ВВЕДЕНО ПОЛЕ ССЫЛКИ
            $url = $array['url'];
            $sector_item->file = null;
            $sector_item->url = $url;

        }

        $sector_item->title = isset($array['title']) ? $array['title'] : $sector_item->title;
        $sector_item->sector_id = isset($array['sector_id']) ? $array['sector_id'] : $sector_item->sector_id;
        $sector_item->save();


    }



    /**
     * Удаление записи направления
     * @param $id
     */
    public static function deleteSectorItem($id){
        self::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->update([
            'status_id'=>self::inactive()
        ]);
    }
}
