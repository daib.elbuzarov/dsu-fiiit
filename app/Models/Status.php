<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * App\Models\Status
 *
 * @property int $id
 * @property string $text
 * @property int $code
 * @property string $table_name
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|Status newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Status newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Status query()
 * @method static \Illuminate\Database\Eloquent\Builder|Status whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Status whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Status whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Status whereTableName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Status whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Status whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Status extends Model
{
    use Notifiable;

    protected $fillable = array('*');

    protected $table = 'status';
    const table = 'status';

    /**
     * Стандартные коды статусов
     */
    const CODE_ACTIVE = 1;
    const CODE_NOACTIVE = 2;
    const CODE_NOT_PUBLISHED = 3;
    const CODE_HIDDEN = 4;

    /**
     * Статусы для фотографий
     */
    const CODE_SMALL_IMG = 90;
    const CODE_MIDDLE_IMG = 91;
    const CODE_ORIGINAL_IMG = 92;
    const CODE_HEAD_IMG = 100;
    const CODE_SLIDER_IMG = 101;

    /**
     * @param int $code
     * @param string $table_name
     * @return |null
     */
    public static function getID(int $code,string $table_name){
        $status = Status::where([['code',$code],['table_name',$table_name]])->first(['id']);
        return $status->id ?? null;
    }





}
