<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Contest
 *
 * @property int $id
 * @property int $type_id
 * @property string $title
 * @property string|null $tag
 * @property string|null $author
 * @property string|null $url
 * @property string|null $prize
 * @property string|null $description
 * @property string|null $date_end
 * @property int $status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\JoinImg $headImg
 * @property-read \App\Models\Status|null $status
 * @property-read \App\Models\Types|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|Contest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Contest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Contest only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Contest query()
 * @method static \Illuminate\Database\Eloquent\Builder|Contest whereAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contest whereDateEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contest whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contest wherePrize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contest whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contest whereTag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contest whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contest whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contest whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contest whereUrl($value)
 * @mixin \Eloquent
 */
class Contest extends Model
{
    use ModelTools;

    protected $table = 'contests';
    const table = 'contests';



    public function headImg()
    {
        return $this->belongsTo(JoinImg::class,'id','item_id')->where([
            ['status_id',self::active(JoinImg::table)],
            ['img_head_code',Status::getID(Status::CODE_HEAD_IMG,JoinImg::table)],
            ['table_name',self::table]
        ]);
    }

    public function type()
    {
        return $this->hasOne(Types::class, 'id', 'type_id');
    }

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    // Получение конкурсов
    public static function getContests()
    {

        return Contest::where([
            ['status_id',self::active()],
        ])->with([
            'headImg.img',
            'type'
        ])->get();
    }

    /**
     * Получение всех конкурсов
     *
     * @return mixed
     */
    public static function getAllContests()
    {
        return Contest::where([
            ['status_id',self::active()],
        ])->with([
            'headImg.img',
            'type'
        ])->get();
    }

    /**
     * Получение конкурсов по типу
     *
     * @param $code
     * @return mixed
     */
    public static function getContestsByType($code,$paginate = 3)
    {
        return Contest::where([
            ['status_id',self::active()],
            ['type_id',Types::getID($code,Contest::table)]
        ])->with([
            'headImg.img',
            'type'
        ])->paginate($paginate);
    }

    /**
     * Получение одного конкурса
     * @param $id
     * @return mixed
     */
    public static function getOneContest($id)
    {
        return Contest::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->first();
    }

    /**
     * Получение одного конкурса
     * @param $id
     * @return mixed
     */
    public static function getOneContestByType($code,$id)
    {
        return Contest::where([
            ['status_id',self::active()],
            ['type_id',Types::getID($code,self::table)],
            ['id',$id]
        ])->first();
    }

    /**
     * Создание конкурса
     * @param array $data
     */
    static public function createContest(array $data)
    {
        $contest = new self();

        $contest->title = $data['title'];
        $contest->tag = $data['tag'];
        $contest->author = $data['author'];
        $contest->type_id = $data['type_id'];
        $contest->url = $data['url'];
        $contest->description = $data['description'];
        $contest->prize = $data['prize'];
        $contest->date_end = $data['date_end'];
        $contest->status_id = self::active();

        $contest->save();



    }

    /**
     * Редактирование конкурса
     * @param array $data
     */
    public static function updateContest(array $data)
    {
        $contest = self::where([
            ['status_id',self::active()],
            ['id',$data['id']]
        ])->first();

        self::where([
            ['status_id',self::active()],
            ['id',$data['id']]
        ])->update([
            'title' => isset($data['title']) ? $data['title'] : $contest->title,
            'tag' => isset($data['tag']) ? $data['tag'] : $contest->tag,
            'url' => isset($data['url']) ? $data['url'] : $contest->url,
            'prize' => isset($data['prize']) ? $data['prize'] : $contest->prize,
            'date_end' => isset($data['date_end']) ? $data['date_end'] : $contest->date_end,
            'description' => isset($data['description']) ? $data['description'] : $contest->description,
            'author' => isset($data['author']) ? $data['author'] : $contest->author,
            'type_id' => isset($data['type_id']) ? $data['type_id'] : $contest->type_id,
        ]);



    }

    /**
     * Удаление конкурса
     * @param $id
     */
    public static function deleteContest($id){
        self::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->update([
            'status_id'=>self::inactive()
        ]);
    }
}
