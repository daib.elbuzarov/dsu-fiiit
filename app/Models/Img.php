<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\Img
 *
 * @property int $id
 * @property string $url
 * @property string $origin_name
 * @property string|null $alt
 * @property string|null $size
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|Img newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Img newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Img query()
 * @method static \Illuminate\Database\Eloquent\Builder|Img whereAlt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Img whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Img whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Img whereOriginName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Img whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Img whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Img whereUrl($value)
 * @mixin \Eloquent
 */
class Img extends Model
{
    use Notifiable;

    protected $fillable = array('*');

    protected $table = 'img';
    const table = 'img';
    /**
     * @param $img_url
     * @param $name
     * @param  null  $alt
     * @param  null  $size
     * @return mixed
     */
    static public function saveImg($img_url, $name, $alt = null, $size = null)
    {
        $img = new Img();
        $img->url = $img_url;
        $img->origin_name = $name;
        $img->alt = $alt;
        $img->size = $size;
        $img->save();
        return $img->id;
    }

    /**
     * @param $img_id
     * @param $img_url
     * @param $name
     * @param  null  $alt
     * @param  null  $size
     * @return mixed
     */
    static public function updateImg($img_id, $img_url = null, $name = null, $alt = null, $size = null)
    {
        $img = Img::find($img_id);
        Img::where('id', $img_id)->update([
            'url' => isset($img_url) ? $img_url : $img->url,
            'origin_name' => isset($name) ? $name : $img->origin_name,
            'alt' => isset($alt) ? $alt : $img->alt,
            'size' => isset($size) ? $size : $img->size,
        ]);
    }
}
