<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Routing\Route;

/**
 * App\Models\President
 *
 * @property int $id
 * @property string $surname
 * @property string $firstname
 * @property string $middle_name
 * @property string $description
 * @property int $image_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\JoinImg $headImg
 * @property-read \App\Models\Status $status
 * @method static \Illuminate\Database\Eloquent\Builder|President newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|President newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|President only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|President query()
 * @method static \Illuminate\Database\Eloquent\Builder|President whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|President whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|President whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|President whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|President whereImageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|President whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|President whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|President whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class President extends Model
{
    use ModelTools;

    protected $table = 'presidents';
    const table = 'presidents';

    protected $guarded = [];


    public function headImg()
    {
        return $this->belongsTo(JoinImg::class, 'id', 'item_id')->where([
            ['status_id', self::active(JoinImg::table)],
            ['img_head_code', Status::getID(Status::CODE_HEAD_IMG, JoinImg::table)],
            ['table_name', self::table]
        ]);
    }

    public static function createPresident()
    {

        $president = self::create([
            'surname' => 'Исмиханов',
            'firstname' => 'Заур',
            'middle_name' => 'Намединович',
            'description' => 'Кандидат эконочмических наук, доцент, Професоор Российской Акакдемии Естествознания',
        ]);
        return $president;
    }

    public static function updatePresident(array $data)
    {
        $president = self::first();

        $president->update([
            'surname' => isset($data['surname']) ? $data['surname'] : $president->surname,
            'firstname' => isset($data['firstname']) ? $data['firstname'] : $president->firstname,
            'middle_name' => isset($data['middle_name']) ? $data['middle_name'] : $president->middle_name,
            'description' => isset($data['description']) ? $data['description'] : $president->description,
        ]);

        if (isset($data['img'])) {

            $origin_name = $data['img']->getClientOriginalName();
            $img_url = Upload::uploadImage($data['img'], self::table . "/" . 'president')[0];
            JoinImg::updateImg($president->id, Status::CODE_ORIGINAL_IMG, Status::CODE_HEAD_IMG, self::table, $img_url, $origin_name);

        }


    }
}
