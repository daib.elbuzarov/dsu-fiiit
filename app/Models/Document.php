<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\Document
 *
 * @property int $id
 * @property string $title
 * @property int $section_id
 * @property int $type_id
 * @property string|null $url
 * @property int $status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Section $section
 * @property-read \App\Models\Status $status
 * @property-read \App\Models\Types $type
 * @method static \Illuminate\Database\Eloquent\Builder|Document newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Document newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Document only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Document query()
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereSectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Document whereUrl($value)
 * @mixin \Eloquent
 */
class Document extends Model
{
    use ModelTools;
    const table = 'documents';
    protected $fillable = array('*');
    protected $table = 'documents';


    /**
     * Получение статуса
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status(){
        return $this->belongsTo(Status::class, 'status_id', 'id');
    }

    /**
     * Получение статуса
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type(){
        return $this->belongsTo(Types::class, 'type_id', 'id');
    }

    /**
     * Получение статуса
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function section(){
        return $this->belongsTo(Section::class, 'section_id', 'id');
    }



    /**
     * Получение документации
     */
    static public function getDocuments()
    {
        return self::where('status_id', self::active(self::table))->with([
            'status',
        ])->get();
    }


    /**
     * Получение одного документа
     *
     * @param $id
     *
     * @return mixed
     */
    static public function getOneDocument($id)
    {
        return self::where([
            ['id', $id],
            ['status_id', '<>', self::inactive(self::table)]
        ])->with([
            'status',
        ])
            ->orderBy('id', 'desc')->first();
    }


    /**
     * Получение ограниченного количества документов
     *
     * @param $count
     *
     * @return mixed
     */
    static public function getPaginateDocument($count)
    {
        return self::where([
            ['status_id', self::active(self::table)]
        ])->orderBy('id', 'desc')->paginate($count);
    }


    /**
     * Создание документа
     *
     * @param $array
     */
    static public function createDocument($array)
    {
        //Загрузка документа
        if (isset($array['document'])) {
            $path_file = public_path('/files/documents/');
            $path = "/files/documents/";
            $fileName = Str::random(30) . '.' . $array['document']->extension();
            $array['document']->move($path_file, $fileName);
            $url = $path . $fileName;

            $type_id = Types::getID(Types::CODE_DOC_FILE,Document::table);
        } else {
            $url = $array['url'];

            $type_id = Types::getID(Types::CODE_DOC_LINK,Document::table);
        }


        $documents = new self();

        $documents->title =  isset($array['title']) ? $array['title'] : null;
        $documents->section_id =  isset($array['section_id']) ? $array['section_id'] : null;
        $documents->type_id =  $type_id;
        $documents->url = $url;
        $documents->status_id =  self::active();
        $documents->save();
    }


    /**
     * Редактирование документа
     *
     * @param $array
     */
    static public function editDocument($array)
    {
        $document = self::where('id',$array['id'])->first();

        //Загрузка документа
        if (isset($array['document'])) {
            $path_file = public_path('/files/documents/');
            $path = "/files/documents/";
            $fileName = Str::random(30) . '.' . $array['document']->extension();
            $array['document']->move($path_file, $fileName);
            $url = $path . $fileName;

            $type_id = Types::getID(Types::CODE_DOC_FILE,Document::table);
        } else  if (isset($array['url'])){
            $url = $array['url'];

            $type_id = Types::getID(Types::CODE_DOC_LINK,Document::table);
        }


        self::where('id', $array['id'])->update([
            'title' => isset($array['title']) ? $array['title'] : $document->title,
            'section_id' => isset($array['section_id']) ? $array['section_id'] : $document->section_id,
            'type_id' => isset($type_id) ? $type_id : $document->type_id,
            'url' => isset($url) ? $url : $document->url,

        ]);
    }



    /**
     * Удаление документа
     * @param $id
     */
    public static function deleteDocument($id){
        self::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->update([
            'status_id'=>self::inactive()
        ]);
    }
}
