<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Search
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Search newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Search newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Search query()
 * @mixin \Eloquent
 */
class Search extends Model
{
    use HasFactory;

    /**
     * Запрос на поиск
     *
     * @param array $data
     * @return array
     */
    public static function getSearch(array $data)
    {
        $result = collect();

        if (isset($data['title'])) {
            $result = $result->concat(self::getSearchByTitle($data['item'], $data['value']));
        }

        if (isset($data['key_words'])) {

            $result = $result->concat(self::getSearchByKeyWords($data['item'], $data['value']));

        }

        // Убираем дубли
        $items = $result->unique();


        return self::getFiltered($items);
    }


    /**
     * Поиск по заголовкам
     *
     * @param $item
     * @param $value
     * @return \Illuminate\Support\Collection
     */
    private static function getSearchByTitle($item, $value)
    {
        $result = collect();

        if (isset($item['news'])) {
            $items = Post::where([
                ['status_id', Post::active()],
                ['type_id', Types::getID(Types::CODE_NEWS, Post::table)],
                ['title', 'LIKE', "%$value%"]
            ])->get();

            $result = $result->concat($items);
        }

        if (isset($item['ads'])) {
            $items = Post::where([
                ['status_id', Post::active()],
                ['type_id', Types::getID(Types::CODE_AD, Post::table)],
                ['title', 'LIKE', "%$value%"]
            ])->get();

            $result = $result->concat($items);
        }

        if (isset($item['contests'])) {
            $items = Contest::where([
                ['status_id', Contest::active()],
                ['title', 'LIKE', "%$value%"]
            ])->get();

            $result = $result->concat($items);
        }

        return $result;
    }

    /**
     * Поиск по description и tag
     *
     * @param $item
     * @param $value
     * @return \Illuminate\Support\Collection
     */
    private static function getSearchByKeyWords($item, $value)
    {
        $result = collect();

        if (isset($item['news'])) {
            $items = Post::where([
                ['status_id', Post::active()],
                ['type_id', Types::getID(Types::CODE_NEWS, Post::table)],
                ['tag', 'LIKE', "%$value%"]
            ])
                ->orWhere([
                    ['status_id', Post::active()],
                    ['type_id', Types::getID(Types::CODE_NEWS, Post::table)],
                    ['description', 'LIKE', "%$value%"]
                ])
                ->get();

            $result = $result->concat($items);
        }

        if (isset($item['ads'])) {
            $items = Post::where([
                ['status_id', Post::active()],
                ['type_id', Types::getID(Types::CODE_AD, Post::table)],
                ['tag', 'LIKE', "%$value%"]
            ])
                ->orWhere([
                    ['status_id', Post::active()],
                    ['type_id', Types::getID(Types::CODE_AD, Post::table)],
                    ['description', 'LIKE', "%$value%"]
                ])
                ->get();

            $result = $result->concat($items);
        }

        if (isset($item['contests'])) {
            $items = Contest::where([
                ['status_id', Contest::active()],
                ['tag', 'LIKE', "%$value%"]
            ])
                ->orWhere([
                    ['status_id', Contest::active()],
                    ['description', 'LIKE', "%$value%"]
                ])
                ->get();
            $result = $result->concat($items);
        }
        return $result;
    }

    /**
     * Сортировка по разным массивам
     *
     * @param $items
     * @return array
     */
    private static function getFiltered($items)
    {
        $news = [];
        $ads = [];
        $contests = [];
        if (count($items)) {

            foreach ($items as $item) {
                if ($item['type_id'] == Types::getID(Types::CODE_NEWS, Post::table)) {
                    $news[] = $item;
                } else if ($item['type_id'] == Types::getID(Types::CODE_AD, Post::table)) {
                    $ads[] = $item;
                } else {
                    $contests[] = $item;
                }
            }

            return [
                'news' => $news,
                'ads' => $ads,
                'contests' => $contests,
            ];
        } else {
            return [];
        }

    }


}
