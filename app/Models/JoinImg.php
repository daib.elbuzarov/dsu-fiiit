<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * App\Models\JoinImg
 *
 * @property int $id
 * @property int $item_id
 * @property int $img_id
 * @property int $img_res_code
 * @property int $img_head_code
 * @property string $table_name
 * @property int $status_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\Models\Img $img
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\Status $status
 * @method static \Illuminate\Database\Eloquent\Builder|JoinImg newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JoinImg newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JoinImg only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|JoinImg query()
 * @method static \Illuminate\Database\Eloquent\Builder|JoinImg whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JoinImg whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JoinImg whereImgHeadCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JoinImg whereImgId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JoinImg whereImgResCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JoinImg whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JoinImg whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JoinImg whereTableName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JoinImg whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class JoinImg extends Model
{
    use Notifiable;
    use ModelTools;

    protected $fillable = array('*');

    protected $table = 'join_img';
    const table = 'join_img';


    /**
     * Получение фотографии
     */
    public function img()
    {
        return $this->belongsTo(Img::class, "img_id", "id");
    }

    /**
     * Удаление главной фотографии
     * @param $item_id
     * @param $img_res_code
     * @param $img_head_code
     */
    public static function deleteImg($item_id, $img_res_code, $img_head_code)
    {
        JoinImg::where([
            ['item_id', $item_id],
            ['img_res_code', $img_res_code],
            ['img_head_code', $img_head_code],
            ['status_id', self::active(JoinImg::table)],
        ])->update([
            'status_id' => self::inactive(JoinImg::table)
        ]);
    }

    /**
     * Сохранение одной фотографии
     * @param $img_url
     * @param $img_name
     * @param $item_id
     * @param $img_res_code
     * @param $img_head_code
     * @param null $alt
     * @param null $size
     */
    public static function saveImg($img_url, $img_name, $item_id, $img_res_code, $img_head_code, $table_name, $alt = null, $size = null)
    {
        $img_id = Img::saveImg($img_url, $img_name, $alt, $size);
        $img_tour = new JoinImg();
        $img_tour->item_id = $item_id;
        $img_tour->img_id = $img_id;
        $img_tour->img_res_code = Status::getID($img_res_code, self::table);
        $img_tour->img_head_code = Status::getID($img_head_code, self::table);
        $img_tour->status_id = self::active(JoinImg::table);
        $img_tour->table_name = $table_name;
        $img_tour->save();
    }

    /**
     * Обновление главной фотографии
     * @param $item_id
     * @param $img_res_code
     * @param $img_head_code
     * @param null $img_url
     * @param null $img_name
     * @param null $alt
     * @param null $size
     */
    public static function updateImg($item_id, $img_res_code, $img_head_code, $table_name, $img_url = null, $img_name = null, $alt = null, $size = null)
    {
        $join = self::where([
            ['item_id', $item_id],
            ['img_res_code', Status::getID($img_res_code, self::table)],
            ['img_head_code', Status::getID($img_head_code, self::table)],
            ['table_name', $table_name],
            ['status_id', self::active(self::table)],
        ])->first();
        if ($join) {
            Img::updateImg($join->img_id, $img_url, $img_name, $alt, $size);
        } else {
            JoinImg::saveImg($img_url, $img_name, $item_id, $img_res_code, $img_head_code, $table_name);
        }
    }

    /**
     * Сохраняем несколько фотографий
     * @param $images
     * @param $tour_id
     */
    public static function saveImages($images, $item_id, $name_directory = 'items')
    {
        foreach ($images as $img) {
            /**
             * Загружаем фото и получаем путь вместе с названием
             */
            $img_array = Upload::uploadImage($img, $name_directory);
            /**
             * Сохраняем фото за туром
             */
            JoinImg::saveImgTour($img_array[0], $img_array[1], $item_id, Status::CODE_ORIGINAL_IMG, Status::CODE_SLIDER_IMG);
        }
    }
}
