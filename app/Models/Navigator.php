<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;

/**
 * Class Navigator
 *
 * @package App\Models
 * @mixin Eloquent
 * @property int $id
 * @property int $type_id
 * @property int|null $parent_id
 * @property string $title
 * @property int|null $position
 * @property string|null $link
 * @property int $status_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Navigator[] $child
 * @property-read int|null $child_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Navigator[] $childCategory
 * @property-read int|null $child_category_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Navigator[] $parents
 * @property-read int|null $parents_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Navigator[] $parentsCategory
 * @property-read int|null $parents_category_count
 * @property-read \App\Models\Status $status
 * @property-read \App\Models\Types|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|Navigator newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Navigator newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Navigator only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Navigator query()
 * @method static \Illuminate\Database\Eloquent\Builder|Navigator whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Navigator whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Navigator whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Navigator whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Navigator wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Navigator whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Navigator whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Navigator whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Navigator whereUpdatedAt($value)
 */
class Navigator extends Model
{
    use ModelTools;

//  protected $fillable = array('*');

    protected $guarded = array();

    protected $table = 'navigators';
    const table = 'navigators';

    /**
     * Получение дочерних элементов навигации
     *
     * @return HasMany
     */
    public function child()
    {
        return $this->hasMany(Navigator::class, 'parent_id', 'id')
            ->where('status_id', '<>', self::inactive(self::table));
    }

    /**
     * Получение дочерних элементов навигации
     *
     * @return HasMany
     */
    public function childCategory()
    {
        return $this->hasMany(Navigator::class, 'parent_id', 'id')
            ->where('status_id', '<>', self::inactive(self::table))->with(['child']);
    }


    /**
     * Получение родительских элементов навигации
     *
     * @return HasMany
     */
    public function parents()
    {
        return $this->hasMany(Navigator::class, 'id', 'parent_id')
            ->where('status_id', '<>', self::inactive(self::table));
    }

    /**
     * Получение родительских элементов навигации
     *
     * @return HasMany
     */
    public function parentsCategory()
    {
        return $this->hasMany(Navigator::class, 'id', 'parent_id')
            ->where('status_id', '<>', self::inactive(self::table))
            ->with(['parents']);
    }

    public function type()
    {
        return $this->hasOne(Types::class, 'id', 'type_id');
    }

    /**
     * Получение всех навигации
     *
     * @return mixed
     */
    static public function getAllNavigators()
    {
        return self::where([
            ['status_id', '<>', self::inactive(self::table)]
        ])->with([
            'childCategory',
            'parentsCategory',
            'status',
            'type'
        ])->orderBy('position', 'asc')->get();
    }


    /**
     * Получение всех навигации по типу
     *
     * @param $type_id
     *
     * @return mixed
     */
    static public function getNavigatorsForType($type_id)
    {
        return self::where([
            ['type_id', $type_id],
            ['status_id', '<>', self::inactive(self::table)],
        ])->with([
            'childCategory',
            'parentsCategory',
            'status',
        ])->orderBy('position', 'asc')->get();
    }


    /**
     * Получение одного элемента навигатора
     *
     * @param $id
     *
     * @return mixed
     */
    static public function getOneNavigator($id)
    {
        return self::where([
            ['id', $id],
            ['status_id', '<>', self::inactive(self::table)],
        ])->with([
            'childCategory',
            'status',
            'parentsCategory',
        ])->first();
    }


    /**
     * Добавление навигации
     *
     * @param $request
     */
    static public function create($request)
    {
        $add = new self([
            'title' => isset($request['title']) ? $request['title'] : null,
            'parent_id' => isset($request['parent_id']) ? $request['parent_id'] : null,
            'position' =>  self::where('type_id',$request['type_id'])->orderBy('position','desc')->pluck('position')->first() +1 ?? 1,
            'type_id' => isset($request['type_id']) ? $request['type_id'] : null,
            'link' => isset($request['link']) ? $request['link'] : null,
            'status_id' => self::active()
        ]);
        $add->save();
    }


    /**
     * Редактирование навигации
     *
     * @param $request
     */
    static public function edit($request)
    {
        $navigator  = self::where([
            ['id', $request['id']],
            ['status_id', '<>', self::inactive(self::table)],
        ])->first();
        self::where([
            ['id', $request['id']],
            ['status_id', '<>', self::inactive(self::table)],
        ])->update([
            'title' => isset($request['title']) ? $request['title'] : null,
            //'parent_id' => isset($request['parent_id']) ? $request['parent_id'] : null,
            'type_id' => isset($request['type_id']) ? $request['type_id'] : null,
            'position' => $request['type_id'] == $navigator->type_id ? $navigator->position :  self::where('type_id',$request['type_id'])->orderBy('position','desc')->pluck('position')->first() +1,
            'link' => isset($request['link']) ? $request['link'] : null,
        ]);
    }

    /**
     * Включение отключение навигации
     *
     * @param $request
     */
    static public function switchNav($request)
    {
        self::where([
            ['id', $request['id']],
            ['status_id', '<>', self::inactive(self::table)],
        ])->update([
            'status_id' => isset($request['published']) ? self::active(self::table)
                : self::notPublished(self::table),
        ]);
    }

    /**
     * Удаление элемента навигации
     *
     * @param $id
     */
    static public function dropNavigator($id)
    {
        self::where('id', $id)
            ->update(['status_id' => self::inactive(self::table)]);
    }

    /**
     * Скрытие элемента навигации
     *
     * @param $id
     */
    static public function hiddenNavigator($id)
    {
        self::where('id', $id)->update(['status_id' => self::hidden(self::table)]);
    }

    /**
     * Не опубликованно элемента навигации
     *
     * @param $id
     */
    static public function notPublishedNavigator($id)
    {
        self::where('id', $id)
            ->update(['status_id' => self::notPublished(self::table)]);
    }

    /**
     * Активирование элемента навигации
     *
     * @param $id
     */
    static public function activeNavigator($id)
    {
        self::where('id', $id)->update(['status_id' => self::active(self::table)]);
    }

    public static function positionNavigator($request)
    {
        foreach ($request['data'] as $nav) {
            self::where(['id' => $nav['id']])->update([
                'position' => $nav['position']
            ]);
        }
    }

    /**
     * Перенос позиции соц. сети вниз
     * @param $id
     */
    public function changePosDown($id)
    {
        $navigator = self::getOneNavigator($id);
        $navigator_up = self::where([
            ['position','>',$navigator->position],
            ['status_id',self::active(self::table)],
            ['type_id',$navigator->type_id]
        ])
            ->orderBy('position','asc')
            ->first();
        $change = $navigator->position;
        $navigator->position = $navigator_up->position;
        $navigator_up->position = $change;
        $navigator->save();
        $navigator_up->save();

    }

    /**
     * Перенос позиции соц. сети вверх
     * @param $id
     */
    public function changePosUp($id)
    {
        $navigator = self::getOneNavigator($id);
        $navigator_down = self::where([
            ['position','<',$navigator->position],
            ['status_id',self::active(self::table)],
            ['type_id',$navigator->type_id]
        ])
            ->orderBy('position','desc')
            ->first();
        $change = $navigator->position;
        $navigator->position = $navigator_down->position;
        $navigator_down->position = $change;
        $navigator->save();
        $navigator_down->save();
    }

}
