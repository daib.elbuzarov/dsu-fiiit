<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\Lesson
 *
 * @property int $id
 * @property string $title
 * @property int $type_id
 * @property string|null $file
 * @property string|null $url
 * @property int $status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Status $status
 * @property-read \App\Models\Types $type
 * @method static \Illuminate\Database\Eloquent\Builder|Lesson newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Lesson newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Lesson only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Lesson query()
 * @method static \Illuminate\Database\Eloquent\Builder|Lesson whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lesson whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lesson whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lesson whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lesson whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lesson whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lesson whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lesson whereUrl($value)
 * @mixin \Eloquent
 */
class Lesson extends Model // Расписание занятий
{
    use ModelTools;
    const table = 'lessons';
    protected $fillable = array('*');
    protected $table = 'lessons';


    /**
     * Получение статуса
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status(){
        return $this->belongsTo(Status::class, 'status_id', 'id');
    }

    /**
     * Получение типа
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type(){
        return $this->belongsTo(Types::class, 'type_id', 'id');
    }

    /**
     * Получение расписания занятий
     */
    static public function getLessons()
    {
        return self::where('status_id', self::active(self::table))->with([
            'status',
        ])->get();
    }

    /**
     * Получение одного расписания занятий по типу
     */
    static public function getLessonsByType($code,$paginate=10)
    {
        return self::where([
            ['status_id', self::active(self::table)],
            ['type_id',Types::getID($code,self::table)]
        ])->with([
            'status',
        ])->paginate($paginate);
    }


    /**
     * Получение одного расписания
     *
     * @param $id
     *
     * @return mixed
     */
    static public function getOneLesson($id)
    {
        return self::where([
            ['id', $id],
            ['status_id', '<>', self::inactive(self::table)]
        ])->with([
            'status',
        ])
            ->orderBy('id', 'desc')->first();
    }


    /**
     * Получение ограниченного количества расписаний
     *
     * @param $count
     *
     * @return mixed
     */
    static public function getPaginateLesson($count)
    {
        return self::where([
            ['status_id', self::active(self::table)]
        ])->orderBy('id', 'desc')->paginate($count);
    }


    /**
     * Создание расписания
     *
     * @param $array
     */
    static public function createLesson($array)
    {
        //Загрузка расписания
        if (isset($array['lesson'])) {
            $path_file = public_path('/files/lessons/');
            $path = "/files/lessons/";
            $fileName = Str::random(30) . '.' . $array['lesson']->extension();
            $array['lesson']->move($path_file, $fileName);
            $file = $path . $fileName;

        } else if (isset($array['url'])){
            $url = $array['url'];

        }


        $lessons = new self();

        $lessons->title =  isset($array['title']) ? $array['title'] : null;
        $lessons->type_id =  isset($array['type_id']) ? $array['type_id'] : null;;
        $lessons->url = isset($url) ? $url : null;
        $lessons->file = isset($file) ? $file : null;
        $lessons->status_id =  self::active();
        $lessons->save();
    }


    /**
     * Редактирование расписания занятий
     *
     * @param $array
     */
    static public function editLesson($array)
    {
        $lesson = self::where('id',$array['id'])->first();

        //Загрузка расписания
        if (isset($array['lesson'])) {
            $path_file = public_path('/files/lessons/');
            $path = "/files/lessons/";
            $fileName = Str::random(30) . '.' . $array['lesson']->extension();
            $array['lesson']->move($path_file, $fileName);
            $file = $path . $fileName;
            //ЕСЛИ ДОБАВЛЕН ФАЙЛ
            $lesson->file = $file;
            $lesson->url = null;

        } else if (isset($array['url'])){
            // ЕСЛИ ВВЕДЕНО ПОЛЕ ССЫЛКИ
            $url = $array['url'];
            $lesson->file = null;
            $lesson->url = $url;

        }

        $lesson->title = isset($array['title']) ? $array['title'] : $lesson->title;
        $lesson->type_id = isset($array['type_id']) ? $array['type_id'] : $lesson->type_id;
        $lesson->save();


    }



    /**
     * Удаление расписания
     * @param $id
     */
    public static function deleteLesson($id){
        self::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->update([
            'status_id'=>self::inactive()
        ]);
    }
}
