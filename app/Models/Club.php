<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Club
 *
 * @property int $id
 * @property int $type_id
 * @property string $title
 * @property string|null $description
 * @property string|null $author
 * @property int $status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\JoinImg $headImg
 * @property-read \App\Models\Status|null $status
 * @property-read \App\Models\Types|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|Club newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Club newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Club only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Club query()
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Club extends Model
{
    use ModelTools;

    protected $table = 'clubs';
    const table = 'clubs';

    public function headImg()
    {
        return $this->belongsTo(JoinImg::class,'id','item_id')->where([
            ['status_id',self::active(JoinImg::table)],
            ['img_head_code',Status::getID(Status::CODE_HEAD_IMG,JoinImg::table)],
            ['table_name',self::table]
        ]);
    }

    public function type()
    {
        return $this->hasOne(Types::class, 'id', 'type_id');
    }

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    // Получение клубов
    public static function getClubs()
    {

        return Club::where([
            ['status_id',self::active()],
        ])->with([
            'headImg.img',
            'type'
        ])->get();
    }

    /**
     * Получение клубов с пагинацией
     *
     * @param int $paginate
     * @return mixed
     */
    public static function getClubsPaginate($paginate = 20)
    {
        return Club::where([
            ['status_id',self::active()],
        ])->with([
            'headImg.img',
            'type'
        ])->paginate($paginate);
    }


    /**
     * Получение всех клубов
     *
     * @return mixed
     */
    public static function getAllClubs()
    {
        return Club::where([
            ['status_id',self::active()],
        ])->with([
            'headImg.img',
            'type'
        ])->get();
    }

    /**
     * Получение клубов по типу
     *
     * @param $code
     * @return mixed
     */
    public static function getClubsByType($code,$paginate = 4)
    {
        return Club::where([
            ['status_id',self::active()],
            ['type_id',Types::getID($code,Club::table)]
        ])->with([
            'headImg.img',
            'type'
        ])->paginate($paginate);
    }

    /**
     * Получение одного клуба
     * @param $id
     * @return mixed
     */
    public static function getOneClub($id)
    {
        return Club::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->first();
    }

    /**
     * Создание клуба
     * @param array $data
     */
    static public function createClub(array $data)
    {
        $club = new self();

        $club->title = $data['title'];
        $club->description = $data['description'];
        $club->author = $data['author'];
        $club->type_id = $data['type_id'];
        $club->status_id = self::active();

        $club->save();

        if (isset($data['logo'])){
            $origin_name = $data['logo']->getClientOriginalName();
            $img_url = Upload::uploadImage($data['logo'],self::table)[0];

            JoinImg::saveImg($img_url,$origin_name,$club->id,Status::CODE_SMALL_IMG,Status::CODE_HEAD_IMG,self::table);

        }

    }

    /**
     * Редактирование клуба
     * @param array $data
     */
    public static function updateClub(array $data)
    {
        $club = self::where([
            ['status_id',self::active()],
            ['id',$data['id']]
        ])->first();

        self::where([
            ['status_id',self::active()],
            ['id',$data['id']]
        ])->update([
            'title' => isset($data['title']) ? $data['title'] : $club->title,
            'description' => isset($data['description']) ? $data['description'] : $club->description,
            'author' => isset($data['author']) ? $data['author'] : $club->author,
            'type_id' => isset($data['type_id']) ? $data['type_id'] : $club->type_id,
        ]);

        if (isset($data['logo'])){

            $origin_name = $data['logo']->getClientOriginalName();
            $img_url = Upload::uploadImage($data['logo'],self::table)[0];

            JoinImg::updateImg($club->id,Status::CODE_SMALL_IMG,Status::CODE_HEAD_IMG,self::table,$img_url,$origin_name);

        }


    }

    /**
     * Удаление клуба
     * @param $id
     */
    public static function deleteClub($id){
        self::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->update([
            'status_id'=>self::inactive()
        ]);
    }
}
