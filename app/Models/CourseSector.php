<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CourseSector
 *
 * @property int $id
 * @property int $course_id
 * @property string|null $number
 * @property string $title
 * @property int|null $status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Course|null $course
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SectorItem[] $sectorItems
 * @property-read int|null $sector_items_count
 * @property-read \App\Models\Status|null $status
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSector newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSector newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSector only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSector query()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSector whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSector whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSector whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSector whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSector whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSector whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSector whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CourseSector extends Model
{
    use ModelTools;

    protected $table = 'course_sectors';
    const table = 'course_sectors';

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    public function course()
    {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

    /**
     * Получение записей направления
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sectorItems()
    {
        return $this->hasMany(SectorItem::class, 'sector_id', 'id')->where([
            ['status_id',self::active(SectorItem::table)]
        ]);
    }

    // Получение подразделов направлений
    public static function getCourseSectors()
    {

        return CourseSector::where([
            ['status_id',self::active()],
        ])->with([
            'headImg.img',
            'type'
        ])->get();
    }

    /**
     * Получение всех подразделов направлений
     *
     * @return mixed
     */
    public static function getAllCourseSectors()
    {
        return CourseSector::where([
            ['status_id',self::active()],
        ])->with([
            'headImg.img',
            'type'
        ])->get();
    }

    /**
     * Получение одного подраздела
     * @param $id
     * @return mixed
     */
    public static function getOneCourseSector($id)
    {
        return CourseSector::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->first();
    }

    /**
     * Создание подраздела
     * @param array $data
     */
    static public function createCourseSector(array $data)
    {
        $course_sector = new self();

        $course_sector->title = $data['title'];
        $course_sector->number = $data['number'];
        $course_sector->course_id = $data['course_id'];
        $course_sector->status_id = self::active();

        $course_sector->save();


    }

    /**
     * Редактирование подраздела
     * @param array $data
     */
    public static function updateCourseSector(array $data)
    {
        $course_sector = self::where([
            ['status_id',self::active()],
            ['id',$data['id']]
        ])->first();

        self::where([
            ['status_id',self::active()],
            ['id',$data['id']]
        ])->update([
            'title' => isset($data['title']) ? $data['title'] : $course_sector->title,
            'number' => isset($data['number']) ? $data['number'] : $course_sector->number,
        ]);




    }

    /**
     * Удаление подраздела
     * @param $id
     */
    public static function deleteCourseSector($id){
        self::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->update([
            'status_id'=>self::inactive()
        ]);
    }
}
