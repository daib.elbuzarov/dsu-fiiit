<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Banner
 *
 * @property int $id
 * @property string|null $url
 * @property string $name
 * @property string $title
 * @property string|null $link
 * @property int $status_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\Models\JoinImg $headImg
 * @property-read \App\Models\JoinImg $smallImg
 * @property-read \App\Models\Status $status
 * @method static \Illuminate\Database\Eloquent\Builder|Banner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Banner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Banner only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Banner query()
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereUrl($value)
 * @mixin \Eloquent
 */
class Banner extends Model
{
    use ModelTools;

    protected $table = 'banners';
    const table = 'banners';

    public function headImg()
    {
        return $this->belongsTo(JoinImg::class,'id','item_id')->where([
            ['status_id',self::active(JoinImg::table)],
            ['img_head_code',Status::getID(Status::CODE_HEAD_IMG,JoinImg::table)],
            ['img_res_code',Status::getID(Status::CODE_ORIGINAL_IMG,JoinImg::table)],
            ['table_name',self::table]
        ]);
    }

    public function smallImg()
    {
        return $this->belongsTo(JoinImg::class,'id','item_id')->where([
            ['status_id',self::active(JoinImg::table)],
            ['img_head_code',Status::getID(Status::CODE_HEAD_IMG,JoinImg::table)],
            ['img_res_code',Status::getID(Status::CODE_SMALL_IMG,JoinImg::table)],
            ['table_name',self::table]
        ]);
    }



    // Получение баннеров определенного адреса
    public static function getBanners($url)
    {
        if ($url=='api'){
            $url='api/';
        }
        return Banner::where([
            ['status_id',self::active()],
            ['url',substr($url,3)]
        ])->get();
    }

    // Получение текстов определенного адреса
    public static function getPageBanners()
    {
      $url = request()->path();

      $banners = self::where([
        ['status_id',self::active()],
        ['url',$url]
      ])->get();

      $result = [];

      foreach ($banners as $banner) {
        $result[$banner->name] = $banner;
      }

      return $result;
    }

    /**
     * Получение всех баннеров
     *
     * @return mixed
     */
    public static function getAllBanners()
    {
        return Banner::where([
            ['status_id',self::active()],
        ])->get();
    }

    /**
     * Получение одного баннера
     * @param $id
     * @return mixed
     */
    public static function getOneBanner($id)
    {
        return Banner::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->first();
    }

    /**
     * Создание баннера
     * @param array $data
     * @return int
     */
    static public function createBanner(array $data)
    {
        $banner = new self();

        $banner->url = $data['url'];
        $banner->name = $data['name'];
        $banner->title = $data['title'];
        $banner->link = $data['link'];
        $banner->status_id = self::active();



        $banner->save();

        if (isset($data['img'])){

            $origin_name = $data['img']->getClientOriginalName();
            $img_url = Upload::uploadImage($data['img'],self::table)[0];

            JoinImg::saveImg($img_url,$origin_name,$banner->id,Status::CODE_ORIGINAL_IMG,Status::CODE_HEAD_IMG,self::table);

        }
        if (isset($data['small_img'])){

            $origin_name = $data['small_img']->getClientOriginalName();
            $small_img_url = Upload::uploadImage($data['small_img'],self::table)[0];

            JoinImg::saveImg($small_img_url,$origin_name,$banner->id,Status::CODE_SMALL_IMG,Status::CODE_HEAD_IMG,self::table);

        }

        return $banner->id;

    }

    /**
     * Редактирование баннера
     * @param array $data
     */
    public static function updateBanner(array $data)
    {
        $banner = self::where([
            ['status_id',self::active()],
            ['id',$data['id']]
        ])->first();

        self::where([
            ['status_id',self::active()],
            ['id',$data['id']]
        ])->update([
         //   'name' => isset($data['name']) ? $data['name'] : $banner->name,
         //  'url' => isset($data['url']) ? $data['url'] : $banner->url,
            'title' => isset($data['title']) ? $data['title'] : $banner->title,
            'link' => isset($data['link']) ? $data['link'] : $banner->title,
        ]);

        if (isset($data['img'])){

            $origin_name = $data['img']->getClientOriginalName();
            $img_url = Upload::uploadImage($data['img'],self::table)[0];

            JoinImg::updateImg($banner->id,Status::CODE_ORIGINAL_IMG,Status::CODE_HEAD_IMG,self::table,$img_url,$origin_name);

        }
        if (isset($data['small_img'])){

            $origin_name = $data['small_img']->getClientOriginalName();
            $small_img_url = Upload::uploadImage($data['small_img'],self::table)[0];

            JoinImg::updateImg($banner->id,Status::CODE_SMALL_IMG,Status::CODE_HEAD_IMG,self::table,$small_img_url,$origin_name);

        }


    }

    /**
     * Удаление баннера
     * @param $id
     */
    public static function deleteBanner($id){
        self::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->update([
            'status_id'=>self::inactive()
        ]);
    }
}
