<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Department
 *
 * @property int $id
 * @property string $title
 * @property string|null $link
 * @property int $status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Status|null $status
 * @method static \Illuminate\Database\Eloquent\Builder|Department newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Department newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Department only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Department query()
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Department extends Model
{
    use ModelTools;

    protected $table = 'departments';
    const table = 'departments';

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    // Получение клубов
    public static function getDepartments($paginate =3)
    {

        return Department::where([
            ['status_id',self::active()],
        ])->get();
      //  ])->paginate($paginate);
    }

    /**
     * Получение всех кафедр
     *
     * @return mixed
     */
    public static function getAllDepartments()
    {
        return Department::where([
            ['status_id',self::active()],
        ])->get();
    }

    /**
     * Получение одного кафедры
     * @param $id
     * @return mixed
     */
    public static function getOneDepartment($id)
    {
        return Department::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->first();
    }

    /**
     * Создание кафедры
     * @param array $data
     */
    static public function createDepartment(array $data)
    {
        $department = new self();

        $department->title = $data['title'];
        $department->link = $data['link'];
        $department->status_id = self::active();

        $department->save();
    }

    /**
     * Редактирование кафедры
     * @param array $data
     */
    public static function updateDepartment(array $data)
    {
        $department = self::where([
            ['status_id',self::active()],
            ['id',$data['id']]
        ])->first();

        self::where([
            ['status_id',self::active()],
            ['id',$data['id']]
        ])->update([
            'title' => isset($data['title']) ? $data['title'] : $department->title,
            'link' => isset($data['link']) ? $data['link'] : $department->link,
        ]);

    }

    /**
     * Удаление кафедры
     * @param $id
     */
    public static function deleteDepartment($id){
        self::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->update([
            'status_id'=>self::inactive()
        ]);
    }
}
