<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DeputyDirector
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property int|null $status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\JoinImg $headImg
 * @property-read \App\Models\Status|null $status
 * @method static \Illuminate\Database\Eloquent\Builder|DeputyDirector newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DeputyDirector newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DeputyDirector only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|DeputyDirector query()
 * @method static \Illuminate\Database\Eloquent\Builder|DeputyDirector whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeputyDirector whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeputyDirector whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeputyDirector whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeputyDirector whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeputyDirector whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DeputyDirector extends Model
{
    use ModelTools;

    protected $table = 'deputy_directors';
    const table = 'deputy_directors';

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }


  public function headImg()
  {
    return $this->belongsTo(JoinImg::class,'id','item_id')->where([
      ['status_id',self::active(JoinImg::table)],
      ['img_head_code',Status::getID(Status::CODE_HEAD_IMG,JoinImg::table)],
      ['img_res_code',Status::getID(Status::CODE_ORIGINAL_IMG,JoinImg::table)],
      ['table_name',self::table]
    ]);
  }

    // Получение замов
    public static function getDeputyDirectors($paginate =3)
    {

        return DeputyDirector::where([
            ['status_id',self::active()],
        ])->with([
          'headImg.img'
          ])->get();
        //])->paginate($paginate);
    }

    /**
     * Получение всех зама
     *
     * @return mixed
     */
    public static function getAllDeputyDirectors()
    {
        return DeputyDirector::where([
            ['status_id',self::active()],
        ])->with([
          'headImg.img'
          ])->get();
    }

    /**
     * Получение одного зама
     * @param $id
     * @return mixed
     */
    public static function getOneDeputyDirector($id)
    {
        return DeputyDirector::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->with([
          'headImg.img'
        ])->first();
    }

    /**
     * Создание зама
     * @param array $data
     */
    static public function createDeputyDirector(array $data)
    {
        $deputy_director = new self();

        $deputy_director->title = $data['title'];
        $deputy_director->description = $data['description'];
        $deputy_director->status_id = self::active();

        $deputy_director->save();

      if (isset($data['img'])){

        $origin_name = $data['img']->getClientOriginalName();
        $img_url = Upload::uploadImage($data['img'],self::table)[0];

        JoinImg::saveImg($img_url,$origin_name,$deputy_director->id,Status::CODE_ORIGINAL_IMG,Status::CODE_HEAD_IMG,self::table);

      }
    }

    /**
     * Редактирование зама
     * @param array $data
     */
    public static function updateDeputyDirector(array $data)
    {
        $deputy_director = self::where([
            ['status_id',self::active()],
            ['id',$data['id']]
        ])->first();

        self::where([
            ['status_id',self::active()],
            ['id',$data['id']]
        ])->update([
            'title' => isset($data['title']) ? $data['title'] : $deputy_director->title,
            'description' => isset($data['description']) ? $data['description'] : $deputy_director->description,
        ]);

      if (isset($data['img'])){

        $origin_name = $data['img']->getClientOriginalName();
        $img_url = Upload::uploadImage($data['img'],self::table)[0];

        JoinImg::updateImg($deputy_director->id,Status::CODE_ORIGINAL_IMG,Status::CODE_HEAD_IMG,self::table,$img_url,$origin_name);

      }

    }

    /**
     * Удаление зама
     * @param $id
     */
    public static function deleteDeputyDirector($id){
        self::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->update([
            'status_id'=>self::inactive()
        ]);
    }
}
