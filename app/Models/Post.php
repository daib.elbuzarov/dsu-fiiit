<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Post
 *
 * @property int $id
 * @property int $type_id
 * @property string $title
 * @property string|null $tag
 * @property string|null $description
 * @property int|null $status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\JoinImg $headImg
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\JoinImg[] $sliderImg
 * @property-read int|null $slider_img_count
 * @property-read \App\Models\Status|null $status
 * @property-read \App\Models\Types|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Post only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Post query()
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereTag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Post extends Model
{
    use ModelTools;

    protected $table = 'posts';
    const table = 'posts';



    /**
     * Получение главного фото
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function headImg()
    {
        return $this->belongsTo(JoinImg::class, 'id', 'item_id')->where([
            ['status_id', self::active(JoinImg::table)],
            ['img_head_code', Status::getID(Status::CODE_HEAD_IMG, JoinImg::table)],
            ['table_name', self::table]
        ]);
    }

    /**
     * Получение слайдера
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sliderImg()
    {
        return $this->hasMany(JoinImg::class,'item_id','id')->where([
           ['status_id',self::active(JoinImg::table)],
           ['img_head_code',Status::getID(Status::CODE_SLIDER_IMG,JoinImg::table)],
           ['table_name',self::table]
        ]);
    }

    public function type()
    {
        return $this->hasOne(Types::class, 'id', 'type_id');
    }

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    // Получение постов
    public static function getPosts()
    {

        return Post::where([
            ['status_id', self::active()],
        ])->with([
            'headImg.img',
            'type'
        ])->get();
    }

    // Получение постов по type_id
    public static function getPostsByTypeId($id,$paginate=8)
    {

        return Post::where([
            ['status_id', self::active()],
            ['type_id', $id]
        ])->with([
            'headImg.img',
            'type'
        ])->orderBy('created_at','desc')->paginate($paginate);
    }

    // Получение постов по type_id
    public static function getPostsByTypeIdPaginate($id,$paginate = 6)
    {

        return Post::where([
            ['status_id', self::active()],
            ['type_id', $id]
        ])->with([
            'headImg.img',
            'type'
        ])->orderBy('created_at','desc')->paginate($paginate);
    }

    // Получение постов по type_id
    public static function getSciencePostsByTypeIdPaginate($id,$paginate = 6)
    {

        return Post::where([
            ['status_id', self::active()],
            ['type_id', $id],
            ['tag',"Научная деятельность"]
        ])->with([
            'headImg.img',
            'type'
        ])->orderBy('created_at','desc')->paginate($paginate);
    }

    /**
     * Получение последней новости
     *
     * @return mixed
     */
    public static function getLastNews($paginate = 3)
    {
        return self::where([
            ['status_id',self::active()],
            ['type_id',Types::getID(Types::CODE_NEWS,Post::table)]
        ])->with([
            'headImg.img',
            'type'
        ])->orderBy('created_at','DESC')
            ->paginate($paginate);
    }

    /**
     * Получение последней объявления
     *
     * @return mixed
     */
    public static function getLastAds($paginate = 4)
    {
        return self::where([
            ['status_id',self::active()],
            ['type_id',Types::getID(Types::CODE_AD,Post::table)]
        ])->with([
            'headImg.img',
            'type'
        ])->orderBy('created_at','DESC')
            ->paginate($paginate);
    }

    /**
     * Получение последней объявления
     *
     * @return mixed
     */
    public static function getLastArticle($paginate = 4)
    {
        return self::where([
            ['status_id',self::active()],
            ['type_id',Types::getID(Types::CODE_ARTICLE,Post::table)]
        ])->with([
            'headImg.img',
            'type'
        ])->orderBy('created_at','DESC')
            ->paginate($paginate);
    }

    /**
     * Получение всех постов
     *
     * @return mixed
     */
    public static function getAllPosts()
    {
        return Post::where([
            ['status_id', self::active()],
        ])->with([
            'headImg.img',
            'type'
        ])->get();
    }

    /**
     * Получение одного поста
     * @param $id
     * @return mixed
     */
    public static function getOnePost($id)
    {
        return Post::where([
            ['status_id', self::active()],
            ['id', $id]
        ])->with([
            'headImg.img',
            'sliderImg.img',
            'type'
        ])->first();
    }

    /**
     * Получение одного поста
     * @param $id
     * @return mixed
     */
    public static function getOnePostByType($code,$id)
    {
        return Post::where([
            ['status_id', self::active()],
            ['type_id',Types::getID($code,self::table)],
            ['id', $id]
        ])->with([
            'headImg.img',
            'sliderImg.img',
            'type'
        ])->first();
    }

    /**
     * Создание поста
     * @param array $data
     */
    static public function createPost(array $data)
    {
        $post = new self();

        $post->title = $data['title'];
        $post->description = $data['description'];
        $post->tag = $data['tag'];
        $post->type_id = $data['type_id'];
        $post->status_id = self::active();

        $post->save();
        if (isset($data['img'])) {
            foreach ($data['img'] as $key => $img) {
                if ($key == 0) {
                    $origin_name = $img->getClientOriginalName();
                    $img_url = Upload::uploadImage($img, self::table)[0];

                    JoinImg::saveImg($img_url, $origin_name, $post->id, Status::CODE_ORIGINAL_IMG, Status::CODE_HEAD_IMG, self::table);
                } else {

                    $origin_name = $img->getClientOriginalName();
                    $img_url = Upload::uploadImage($img, self::table)[0];

                    JoinImg::saveImg($img_url, $origin_name, $post->id, Status::CODE_ORIGINAL_IMG, Status::CODE_SLIDER_IMG, self::table);
                }
            }
        }

    }

    /**
     * Редактирование поста
     * @param array $data
     */
    public static function updatePost(array $data)
    {
        $post = self::where([
            ['status_id', self::active()],
            ['id', $data['id']]
        ])->first();

        self::where([
            ['status_id', self::active()],
            ['id', $data['id']]
        ])->update([
            'title' => isset($data['title']) ? $data['title'] : $post->title,
            'description' => isset($data['description']) ? $data['description'] : $post->description,
            'tag' => isset($data['tag']) ? $data['tag'] : $post->tag,
            'type_id' => isset($data['type_id']) ? $data['type_id'] : $post->type_id,
        ]);

        if (isset($data['img'])) {
            JoinImg::where([
                ['item_id',$post->id]
            ])->update([
                'status_id'=>self::inactive(JoinImg::table)
            ]);
            foreach ($data['img'] as $key => $img) {
                if ($key == 0) {
                    $origin_name = $img->getClientOriginalName();
                    $img_url = Upload::uploadImage($img, self::table)[0];

                    JoinImg::saveImg($img_url, $origin_name, $post->id, Status::CODE_ORIGINAL_IMG, Status::CODE_HEAD_IMG, self::table);
                } else {
                    $origin_name = $img->getClientOriginalName();
                    $img_url = Upload::uploadImage($img, self::table)[0];

                    JoinImg::saveImg($img_url, $origin_name, $post->id, Status::CODE_ORIGINAL_IMG, Status::CODE_SLIDER_IMG, self::table);
                }
            }
        }


    }

    /**
     * Удаление поста
     * @param $id
     */
    public static function deletePost($id)
    {
        self::where([
            ['status_id', self::active()],
            ['id', $id]
        ])->update([
            'status_id' => self::inactive()
        ]);
    }
}
