<?php

namespace App\Models;

use Eloquent;
use File;
use Goutte\Client;
use Intervention\Image\Facades\Image;
use Str;
use Symfony\Component\HttpClient\Exception\TransportException;

// use Intervention\Image\Facades\Image as Image;

/**
 * Class Types
 * @package App
 * @mixin Eloquent
 */
class Upload
{

    /**
     * Парсинг ссылки для EditorJS
     * @param $request
     * @return array|bool
     */
    public static function fetchLink($request)
    {
        // Получение содержимого сайта с помощью DomCrawler
        $client = new Client();
        try {
            $crawler = $client->request('GET', $request['url']);
        } catch (TransportException $exception) {
            return false;
        }

        // Получение заголовка
        $title = $request['url'];
        $crawledTitle = $crawler->filter('head > title')->getNode(0);
        $crawledOGTitle = $crawler->filter('head > meta[property="og:title"]')->getNode(0);
        if ($crawledTitle) {
            $title = $crawledTitle->textContent;
        } elseif ($crawledOGTitle && count($crawledOGTitle->attributes) && $crawledOGTitle->attributes->getNamedItem('content')) {
            $title = $crawledOGTitle->attributes->getNamedItem('content')->nodeValue;
        }

        // Получение описания
        $description = '';
        $crawledDescription = $crawler->filter('head > meta[name="description"]')->getNode(0);
        $crawledOGDescription = $crawler->filter('head > meta[property="og:description"]')->getNode(0);
        if ($crawledDescription && count($crawledDescription->attributes) && $crawledDescription->attributes->getNamedItem('content')) {
            $description = $crawledDescription->attributes->getNamedItem('content')->nodeValue;
        } elseif ($crawledOGDescription && count($crawledOGDescription->attributes) && $crawledOGDescription->attributes->getNamedItem('content')) {
            $description = $crawledOGDescription->attributes->getNamedItem('content')->nodeValue;
        }

        // Получение изображения
        $image = '';
        $crawledOGImage = $crawler->filter('head > meta[property="og:image"]')->getNode(0);
        $crawledImage = $crawler->filter('body img')->getNode(0);
        if ($crawledOGImage && count($crawledOGImage->attributes) && $crawledOGImage->attributes->getNamedItem('content')) {
            $image = $crawledOGImage->attributes->getNamedItem('content')->nodeValue;
        } elseif ($crawledImage && count($crawledImage->attributes) && $crawledImage->attributes->getNamedItem('src')) {
            $image = $crawledImage->attributes->getNamedItem('src')->nodeValue;
        }
        return [
            'title' => $title,
            'description' => $description,
            'image' => [
                'url' => $image
            ]
        ];
    }

    /**
     * Загрузка изображения без сжатий
     *
     * @param $image
     * @param $path
     *
     * @return string
     */
    public static function uploadImage($image, $path)
    {
        $path_file = public_path('files/img/' . '' . $path);
        self::checkDir('files/img/' . '' . $path);
        $imageName = Str::random(50) . '.' . $image->extension();
        $image->move($path_file, $imageName);
        $array[0] = '/files/img/' . $path . '/' . $imageName;
        $array[1] = $imageName;
        return $array;
    }

    /**
     * Загрузка изображения без сжатий
     *
     * @param $img
     * @param $path
     *
     * @param $width
     * @param $height
     * @param $quality
     * @return string
     */
    public static function uploadImageCrop($img, $path, $width, $height, $quality)
    {
        self::checkDir('img/' . $path);
        $name = Str::random(40) . '.' . $img->extension();
        $imageName = 'img/' . $path . '/' . $name;

        $image = Image::make($img);
        $image->fit($width, $height);
        $image->save($imageName, $quality);

        $array[0] = '/' . $imageName;
        $array[1] = $name;
        return $array;
    }

    /**
     * Загрузка изображения для EditorJS
     * @param $request
     * @return string
     */
    public static function uploadEditor($request)
    {
        if (isset($request['image'])) {
            $path = public_path('img/editor');
            self::checkDir('img/editor');
            $imageName = Str::random(23) . '.' . $request['image']->extension();
            $request['image']->move($path, $imageName);
            return 'img/editor/' . $imageName;
        } elseif (isset($request['url'])) {
            return $request['url'];
        } else {
            return false;
        }
    }

    /**
     * Загрузка изображения и обрезка
     * @param $data
     * @return mixed
     */
    public static function uploadCrop($data)
    {
        return self::cropByUser($data, 100);
    }

    /**
     * Загрузка изображения и обрезка
     * @param $data
     * @return mixed
     */
    public static function uploadConfirmCrop($data)
    {
        switch ($data['from']) {
            case 'company':
                return [
                    'cropped' => self::cropBySystem($data, 1920, 1080, 100),
                ];
            case 'tour':
                return [
                    'full' => self::cropBySystem($data, 1920, 1080, 100),
                    'thumbnail' => self::cropBySystem($data, 330, 400, 100),
                ];
            case 'excursion':
                return [
                    'full' => self::cropBySystem($data, 1920, 1080, 100),
                    'thumbnail' => self::cropBySystem($data, 330, 400, 100),
                ];
            case 'hotelroom':
                return [
                    'full' => self::cropBySystem($data, 1920, 1080, 100),
                    'thumbnail' => self::cropBySystem($data, 330, 400, 100),
                ];
            case 'news':
                return [
                    'full' => self::cropBySystem($data, 1920, 1080, 100),
                    'thumbnail' => self::cropBySystem($data, 300, 200, 100),
                    'preview' => self::cropBySystem($data, 700, 500, 100),
                ];
            case 'history':
                return [
                    'full' => self::cropBySystem($data, 1920, 1080, 100),
                    'thumbnail' => self::cropBySystem($data, 300, 200, 100),
                    'preview' => self::cropBySystem($data, 700, 500, 100),
                ];
            default:
                return [
                    'cropped' => self::cropBySystem($data, 1920, 1080, 100),
                ];
        }
    }

    /**
     * Обрезка изображения по размерам пользователя
     * @param $data - validated
     * @param $width
     * @param $height
     * @param int $quality
     * @return mixed
     */
    private static function cropBySystem($data, $width, $height, $quality = 100)
    {
        self::checkDir('img/cropped/');

        $imageName = 'img/cropped/' . Str::random(40) . '.' . $data['image']->extension();

        $image = Image::make($data['image']);
        $image->fit($width, $height);
        $image->save($imageName, $quality);

        return [
            'name' => '/' . $imageName,
            'original_name' => $data['image']->getClientOriginalName(),
            'size' => self::humanReadableSize($data['image']->getSize())
        ];
    }

    /**
     * Обрезка изображения по размерам пользователя
     * @param $data - validated
     * @param int $quality
     * @return mixed
     */
    private static function cropByUser($data, $quality = 100)
    {
        self::checkDir('img/cropped/');

        $imageName = 'img/cropped/' . Str::random(40) . '.' . $data['image']->extension();
        $image = Image::make($data['image']);
        $image->rotate($data['rotate']);
        if ($data['scaleX'] < 0) {
            $image->flip('h');
        }
        if ($data['scaleY'] < 0) {
            $image->flip('v');
        }
        $image->crop($data['width'], $data['height'], $data['x'], $data['y']);
        $image->save($imageName, $quality);

        return [
            'name' => '/' . $imageName,
            'original_name' => $data['image']->getClientOriginalName(),
            'size' => self::humanReadableSize($data['image']->getSize())
        ];
    }

    /**
     * Форматирование размера
     * @param $bytes
     * @return string
     */
    public static function humanReadableSize($bytes)
    {
        $i = floor(log($bytes, 1024));
        return round($bytes / pow(1024, $i), [0, 0, 2, 2, 3][(int)$i]) . ['Б', 'КБ', 'МБ', 'ГБ', 'ТБ'][(int)$i];
    }

    /**
     * Проверка на наличие папки и ее создание в случае отсутствия
     *
     * @param $path
     */
    public static function checkDir($path)
    {
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }
    }

}
