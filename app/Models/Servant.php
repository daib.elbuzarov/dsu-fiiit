<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Servant
 *
 * @property int $id
 * @property int $type_id
 * @property string $title
 * @property string|null $work_type
 * @property string|null $description
 * @property int|null $status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\JoinImg $headImg
 * @property-read \App\Models\Status|null $status
 * @property-read \App\Models\Types|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|Servant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Servant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Servant only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Servant query()
 * @method static \Illuminate\Database\Eloquent\Builder|Servant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servant whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servant whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servant whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servant whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servant whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servant whereWorkType($value)
 * @mixin \Eloquent
 */
class Servant extends Model
{
    use ModelTools;

    protected $table = 'servants';
    const table = 'servants';

    public function headImg()
    {
        return $this->belongsTo(JoinImg::class,'id','item_id')->where([
            ['status_id',self::active(JoinImg::table)],
            ['img_head_code',Status::getID(Status::CODE_HEAD_IMG,JoinImg::table)],
            ['table_name',self::table]
        ]);
    }

    public function type()
    {
        return $this->hasOne(Types::class, 'id', 'type_id');
    }

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    // Получение работников
    public static function getServants()
    {

        return Servant::where([
            ['status_id',self::active()],
        ])->with([
            'headImg.img',
            'type'
        ])->get();
    }

    /**
     *
     * @param int $paginate
     * @return mixed
     */
    public static function getServantsByTypeIdPaginate($type_id,$paginate = 3)
    {
        return Servant::where([
            ['status_id',self::active()],
            ['type_id',$type_id],
        ])->with([
            'headImg.img',
            'type'
        ])->get();
            //->paginate($paginate);
    }

    // Получение работников по type_id
    public static function getServantsByTypeId($id)
    {

        return Servant::where([
            ['status_id',self::active()],
            ['type_id',$id]
        ])->with([
            'headImg.img',
            'type'
        ])->get();
    }

    /**
     * Получение всех работников
     *
     * @return mixed
     */
    public static function getAllServants()
    {
        return Servant::where([
            ['status_id',self::active()],
        ])->with([
            'headImg.img',
            'type'
        ])->get();
    }

    /**
     * Получение одного работника
     * @param $id
     * @return mixed
     */
    public static function getOneServant($id)
    {
        return Servant::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->first();
    }

    /**
     * Создание работника
     * @param array $data
     */
    static public function createServant(array $data)
    {
        $servant = new self();

        $servant->title = $data['title'];
        $servant->description = $data['description'];
        $servant->work_type = $data['work_type'];
        $servant->type_id = $data['type_id'];
        $servant->status_id = self::active();

        $servant->save();

        if (isset($data['img'])){
            $origin_name = $data['img']->getClientOriginalName();
            $img_url = Upload::uploadImage($data['img'],self::table."/".$servant->type->name)[0];

            JoinImg::saveImg($img_url,$origin_name,$servant->id,Status::CODE_ORIGINAL_IMG,Status::CODE_HEAD_IMG,self::table);

        }

    }

    /**
     * Редактирование работника
     * @param array $data
     */
    public static function updateServant(array $data)
    {
        $servant = self::where([
            ['status_id',self::active()],
            ['id',$data['id']]
        ])->first();
        self::where([
            ['status_id',self::active()],
            ['id',$data['id']]
        ])->update([
            'title' => isset($data['title']) ? $data['title'] : $servant->title,
            'description' => isset($data['description']) ? $data['description'] : $servant->description,
            'work_type' => isset($data['work_type']) ? $data['work_type'] : $servant->work_type,
            'type_id' => isset($data['type_id']) ? $data['type_id'] : $servant->type_id,
        ]);

        if (isset($data['img'])){

            $origin_name = $data['img']->getClientOriginalName();
            $img_url = Upload::uploadImage($data['img'],self::table."/".$servant->type->name)[0];
            JoinImg::updateImg($servant->id,Status::CODE_ORIGINAL_IMG,Status::CODE_HEAD_IMG,self::table,$img_url,$origin_name);

        }


    }

    /**
     * Удаление работника
     * @param $id
     */
    public static function deleteServant($id){
        self::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->update([
            'status_id'=>self::inactive()
        ]);
    }
}
