<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Social
 *
 * @property int $id
 * @property int $type_id
 * @property string $purpose
 * @property string $enter
 * @property string $postscript
 * @property string $leader_name
 * @property string $leader_post
 * @property int $status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\JoinImg $headImg
 * @property-read \App\Models\Status|null $status
 * @property-read \App\Models\Types|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|Social newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Social newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Social only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Social query()
 * @method static \Illuminate\Database\Eloquent\Builder|Social whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Social whereEnter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Social whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Social whereLeaderName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Social whereLeaderPost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Social wherePostscript($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Social wherePurpose($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Social whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Social whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Social whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Social extends Model
{
    use ModelTools;

    protected $table = 'socials';
    const table = 'socials';

    public function headImg()
    {
        return $this->belongsTo(JoinImg::class, 'id', 'item_id')->where([
            ['status_id', self::active(JoinImg::table)],
            ['img_head_code', Status::getID(Status::CODE_HEAD_IMG, JoinImg::table)],
            ['table_name', self::table]
        ]);
    }

    public function type()
    {
        return $this->hasOne(Types::class, 'id', 'type_id');
    }

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    // Получение представителей
    public static function getSocials()
    {

        return Social::where([
            ['status_id', self::active()],
        ])->with([
            'headImg.img',
            'type'
        ])->get();
    }

    // Получение представителей по type_id
    public static function getSocialsByTypeId($id)
    {

        return Social::where([
            ['status_id', self::active()],
            ['type_id', $id]
        ])->with([
            'headImg.img',
            'type'
        ])->get();
    }

    /**
     * Получение всех представителей
     *
     * @return mixed
     */
    public static function getAllSocials()
    {
        return Social::where([
            ['status_id', self::active()],
        ])->with([
            'headImg.img',
            'type'
        ])->get();
    }

    /**
     * Получение одного представителя по type_id
     * @param $id
     * @return mixed
     */
    public static function getOneSocialByTypeId($id)
    {
        return Social::where([
            ['type_id', $id]
        ])->first();
    }

    /**
     * Создание представителя
     * @param array $data
     */
    static public function createSocial(array $data)
    {
        $social = new self();

        $social->type_id = $data['type_id'];
        $social->purpose = $data['purpose'];
        $social->enter = $data['enter'];
        $social->postscript = $data['postscript'];
        $social->leader_name = $data['leader_name'];
        $social->leader_post = $data['leader_post'];
        $social->status_id = self::active();

        $social->save();

        if (isset($data['img'])) {
            $origin_name = $data['img']->getClientOriginalName();
            $img_url = Upload::uploadImage($data['img'], self::table)[0];

            JoinImg::saveImg($img_url, $origin_name, $social->id, Status::CODE_SMALL_IMG, Status::CODE_HEAD_IMG, self::table);

        }

    }

    /**
     * Редактирование представителя
     * @param array $data
     */
    public static function updateSocial(array $data)
    {
        $social = self::where([
            ['status_id', self::active()],
            ['id', $data['id']]
        ])->first();

        self::where([
            ['status_id', self::active()],
            ['id', $data['id']]
        ])->update([
            'purpose' => isset($data['purpose']) ? $data['purpose'] : $social->purpose,
            'enter' => isset($data['enter']) ? $data['enter'] : $social->enter,
            'postscript' => isset($data['postscript']) ? $data['postscript'] : $social->postscript,
            'type_id' => isset($data['type_id']) ? $data['type_id'] : $social->type_id,
            'leader_name' => isset($data['leader_name']) ? $data['leader_name'] : $social->leader_name,
            'leader_post' => isset($data['leader_post']) ? $data['leader_post'] : $social->leader_post,
        ]);

        if (isset($data['img'])) {

            if ($social->headImg) {
                $origin_name = $data['img']->getClientOriginalName();
                $img_url = Upload::uploadImage($data['img'], self::table)[0];

                JoinImg::updateImg($social->id, Status::CODE_ORIGINAL_IMG, Status::CODE_HEAD_IMG, self::table, $img_url, $origin_name);
            } else {
                $origin_name = $data['img']->getClientOriginalName();
                $img_url = Upload::uploadImage($data['img'], self::table)[0];

                JoinImg::saveImg($img_url, $origin_name, $social->id, Status::CODE_ORIGINAL_IMG, Status::CODE_HEAD_IMG, self::table);
            }

        }


    }

    /**
     * Удаление представителя
     * @param $id
     */
    public static function deleteSocial($id)
    {
        self::where([
            ['status_id', self::active()],
            ['id', $id]
        ])->update([
            'status_id' => self::inactive()
        ]);
    }
}
