<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\FacultySenate
 *
 * @property int $id
 * @property int|null $type_id
 * @property string|null $text
 * @property string|null $link
 * @property int|null $status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Status|null $status
 * @property-read \App\Models\Types|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|FacultySenate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FacultySenate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FacultySenate only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|FacultySenate query()
 * @method static \Illuminate\Database\Eloquent\Builder|FacultySenate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FacultySenate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FacultySenate whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FacultySenate whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FacultySenate whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FacultySenate whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FacultySenate whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FacultySenate extends Model
{
    use ModelTools;

    protected $table = 'faculty_senate';
    const table = 'faculty_senate';



    public function type()
    {
        return $this->hasOne(Types::class, 'id', 'type_id');
    }

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    // Получение конкурсов
    public static function getSenates()
    {

        return FacultySenate::where([
            ['status_id',self::active()],
        ])->with([
            'type'
        ])->get();
    }

    /**
     * Получение всех конкурсов
     *
     * @return mixed
     */
    public static function getAllSenates()
    {
        return FacultySenate::where([
            ['status_id',self::active()],
        ])->with([
            'type'
        ])->get();
    }

    /**
     * Получение одного совета
     * @param $id
     * @return mixed
     */
    public static function getOneSenate($id)
    {
        return FacultySenate::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->first();
    }


    /**
     * Получение совета по type
     *
     * @param $code
     * @return mixed
     */
    public static function getSenatesByType($code,$paginate = 2)
    {
        return FacultySenate::where([
            ['status_id',self::active()],
            ['type_id',Types::getID($code,FacultySenate::table)]
        ])->with([
            'type'
        ])->paginate($paginate);
    }

    /**
     * Создание совета
     * @param array $data
     */
    static public function createSenate(array $data)
    {

        if (isset($data['file'])) {
            $path_file = public_path('/files/senate/');
            $path = "/files/senate/";
            $fileName = Str::random(30) . '.' . $data['file']->extension();
            $data['file']->move($path_file, $fileName);
            $file = $path . $fileName;

        } else if (isset($data['link'])){
            $link = $data['link'];
        }

        $faculty_senate = new self();

        $faculty_senate->type_id = $data['type_id'];
        $faculty_senate->text = $data['text'];
        $faculty_senate->status_id = self::active();
        $faculty_senate->link = $link ?? null;
        $faculty_senate->file = $file ?? null;

        $faculty_senate->save();

    }

    /**
     * Редактирование совета
     * @param array $data
     */
    public static function updateSenate(array $data)
    {
        $faculty_senate = self::where([
            ['status_id',self::active()],
            ['id',$data['id']]
        ])->first();

        if (isset($data['file'])) {
            $path_file = public_path('/files/senate/');
            $path = "/files/senate/";
            $fileName = Str::random(30) . '.' . $data['file']->extension();
            $data['file']->move($path_file, $fileName);
            $file = $path . $fileName;
            //ЕСЛИ ДОБАВЛЕН ФАЙЛ
            $faculty_senate->file = $file;
            $faculty_senate->link = null;

        } else if (isset($data['link'])){
            // ЕСЛИ ВВЕДЕНО ПОЛЕ ССЫЛКИ
            $link = $data['link'];
            $faculty_senate->file = null;
            $faculty_senate->link = $link;
        }

        $faculty_senate->type_id =$data['type_id'] ?? $faculty_senate->type_id;
        $faculty_senate->text =$data['text'] ?? $faculty_senate->text;

        $faculty_senate->save();
    }

    /**
     * Удаление совета
     * @param $id
     */
    public static function deleteSenate($id){
        self::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->update([
            'status_id'=>self::inactive()
        ]);
    }
}
