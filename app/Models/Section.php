<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Section
 *
 * @property int $id
 * @property string $title
 * @property int|null $status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Document[] $documents
 * @property-read int|null $documents_count
 * @property-read \App\Models\Status|null $status
 * @method static \Illuminate\Database\Eloquent\Builder|Section newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Section newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Section only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Section query()
 * @method static \Illuminate\Database\Eloquent\Builder|Section whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Section whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Section whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Section whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Section whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Section extends Model
{
    use ModelTools;

    protected $table = 'sections';
    const table = 'sections';


    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    /**
     * Получение документов
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function documents()
    {
        return $this->hasMany(Document::class,'section_id','id')->where([
            ['status_id',Document::active()]
        ]);
    }

    // Получение разделов
    public static function getSections()
    {

        return Section::where([
            ['status_id', self::active()],
        ])->with([
            'documents'
        ])->get();
    }

    /**
     * Получение всех разделов
     *
     * @return mixed
     */
    public static function getAllSections()
    {
        return Section::get();
    }

    /**
     * Получение одного раздела
     * @param $id
     * @return mixed
     */
    public static function getOneSection($id)
    {
        return Section::where([
            ['status_id', self::active()],
            ['id', $id]
        ])->first();
    }

    /**
     * Создание раздела
     * @param array $data
     */
    static public function createSection(array $data)
    {
        $section = new self();

        $section->title = $data['title'];
        $section->status_id = self::active();

        $section->save();


    }

    /**
     * Редактирование раздела
     * @param array $data
     */
    public static function updateSection(array $data)
    {
        $section = self::where([
            ['status_id', self::active()],
            ['id', $data['id']]
        ])->first();

        self::where([
            ['status_id', self::active()],
            ['id', $data['id']]
        ])->update([
            'title' => isset($data['title']) ? $data['title'] : $section->title,
        ]);


    }

    /**
     * Удаление раздела
     * @param $id
     */
    public static function deleteSection($id)
    {
        self::where([
            ['status_id', self::active()],
            ['id', $id]
        ])->update([
            'status_id' => self::inactive()
        ]);
    }
}
