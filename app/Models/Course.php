<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Course
 *
 * @property int $id
 * @property int $type_id
 * @property string $title
 * @property string|null $description
 * @property int|null $status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CourseSector[] $sectors
 * @property-read int|null $sectors_count
 * @property-read \App\Models\Status|null $status
 * @property-read \App\Models\Types|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|Course newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Course newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Course only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Course query()
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Course extends Model
{
    use ModelTools;

    protected $table = 'courses';
    const table = 'courses';

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    public function type()
    {
        return $this->hasOne(Types::class, 'id', 'type_id');
    }

    public function sectors()
    {
        return $this->hasMany(CourseSector::class, 'course_id', 'id')->where([
            ['status_id', self::active(CourseSector::table)]
        ]);
    }

    // Получение направлений
    public static function getCourses()
    {

        return Course::where([
            ['status_id', self::active()],
        ])
            ->with([
                'sectors'
            ])
            ->get();
    }

    // Получение направлений
    public static function getCoursesByType($code)
    {

        return Course::where([
            ['status_id', self::active()],
            ['type_id', Types::getID($code, self::table)]
        ])
            ->with([
                'sectors'
            ])
            ->get();
    }

    /**
     * Получение всех направлений
     *
     * @return mixed
     */
    public static function getAllCourses()
    {
        return Course::where([
            ['status_id', self::active()],
        ])->get();
    }

    /**
     * Получение одного направления
     * @param $id
     * @return mixed
     */
    public static function getOneCourse($id)
    {
        return Course::where([
            ['status_id', self::active()],
            ['id', $id]
        ])->first();
    }

    /**
     * Создание направления
     * @param array $data
     */
    static public function createCourse(array $data)
    {
        $course = new self();

        $course->title = $data['title'];
        $course->description = $data['description'];
        $course->type_id = $data['type_id'];
        $course->status_id = self::active();

        $course->save();
    }

    /**
     * Редактирование направления
     * @param array $data
     */
    public static function updateCourse(array $data)
    {
        $course = self::where([
            ['status_id', self::active()],
            ['id', $data['id']]
        ])->first();

        self::where([
            ['status_id', self::active()],
            ['id', $data['id']]
        ])->update([
            'title' => isset($data['title']) ? $data['title'] : $course->title,
            'description' => isset($data['description']) ? $data['description'] : $course->description,
            'type_id' => isset($data['type_id']) ? $data['type_id'] : $course->type_id,
        ]);

    }

    /**
     * Удаление направления
     * @param $id
     */
    public static function deleteCourse($id)
    {
        self::where([
            ['status_id', self::active()],
            ['id', $id]
        ])->update([
            'status_id' => self::inactive()
        ]);
    }
}
