<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PageText
 *
 * @property int $id
 * @property string $url
 * @property string $name
 * @property string|null $title
 * @property string|null $link
 * @property string|null $value
 * @property int $status_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\Models\Status $status
 * @method static \Illuminate\Database\Eloquent\Builder|PageText newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageText newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageText only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|PageText query()
 * @method static \Illuminate\Database\Eloquent\Builder|PageText whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageText whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageText whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageText whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageText whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageText whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageText whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageText whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageText whereValue($value)
 * @mixin \Eloquent
 */
class PageText extends Model
{

    use ModelTools;

    protected $table = 'page_texts';
    const table = 'page_texts';

    // Получение текстов определенного адреса
    public static function getPageTexts()
    {
        $url = request()->path();

        $page_texts = PageText::where([
            ['url',$url]
        ])->get();

        $result = [];

        foreach ($page_texts as $text) {
            $result[$text->name] = $text;
        }

        return $result;
    }

    /**
     * Получение всех текстов
     *
     * @return mixed
     */
    public static function getAllPageTexts()
    {
        return PageText::where([
            ['status_id',self::active()],
        ])->get();
    }

    /**
     * Получение одного текста
     * @param $id
     * @return mixed
     */
    public static function getOnePageText($id)
    {
        return PageText::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->first();
    }

    /**
     * Создание текста
     * @param array $data
     */
    static public function createPageText(array $data)
    {
        $page_text = new self();

        $page_text->url = $data['url'];
        $page_text->name = $data['name'];
        $page_text->title = $data['title'];
        $page_text->value = $data['value'];
        $page_text->link = $data['link'];
        $page_text->status_id = self::active();

        $page_text->save();

    }

    /**
     * Редактирование текста
     * @param array $data
     */
    public static function updatePageText(array $data)
    {
        $page_text = self::where([
            ['status_id',self::active()],
            ['id',$data['id']]
        ])->first();

        self::where([
            ['status_id',self::active()],
            ['id',$data['id']]
        ])->update([
          //  'name' => isset($data['name']) ? $data['name'] : $page_text->name,
          //  'url' => isset($data['url']) ? $data['url'] : $page_text->url,
            'title' => isset($data['title']) ? $data['title'] : null,
            'value' => isset($data['value']) ? $data['value'] : null,
            'link' => isset($data['link']) ? $data['link'] : null,
        ]);


    }

    /**
     * Удаление текста
     * @param $id
     */
    public static function deletePageText($id){
        self::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->update([
            'status_id'=>self::inactive()
        ]);
    }

}
