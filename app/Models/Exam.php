<?php

namespace App\Models;

use App\Traits\ModelTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\Exam
 *
 * @property int $id
 * @property string $title
 * @property int $type_id
 * @property string|null $file
 * @property string|null $url
 * @property int $status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Status $status
 * @property-read \App\Models\Types $type
 * @method static \Illuminate\Database\Eloquent\Builder|Exam newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Exam newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Exam only($value = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Exam query()
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exam whereUrl($value)
 * @mixin \Eloquent
 */
class Exam extends Model // Расписание сессий
{
    use ModelTools;
    const table = 'exams';
    protected $fillable = array('*');
    protected $table = 'exams';


    /**
     * Получение статуса
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status(){
        return $this->belongsTo(Status::class, 'status_id', 'id');
    }

    /**
     * Получение типа
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type(){
        return $this->belongsTo(Types::class, 'type_id', 'id');
    }

    /**
     * Получение сессии
     */
    static public function getExams()
    {
        return self::where('status_id', self::active(self::table))->with([
            'status',
        ])->get();
    }

    /**
     * Получение одной сессии по типу
     */
    static public function getExamsByType($code,$paginate=10)
    {
        return self::where([
            ['status_id', self::active(self::table)],
            ['type_id', Types::getID($code,self::table)],
        ])->with([
            'status',
        ])->paginate($paginate);
    }


    /**
     * Получение одного сессии
     *
     * @param $id
     *
     * @return mixed
     */
    static public function getOneExam($id)
    {
        return self::where([
            ['id', $id],
            ['status_id', '<>', self::inactive(self::table)]
        ])->with([
            'status',
        ])
            ->orderBy('id', 'desc')->first();
    }


    /**
     * Получение ограниченного количества документов
     *
     * @param $count
     *
     * @return mixed
     */
    static public function getPaginateExam($count)
    {
        return self::where([
            ['status_id', self::active(self::table)]
        ])->orderBy('id', 'desc')->paginate($count);
    }


    /**
     * Создание расписания
     *
     * @param $array
     */
    static public function createExam($array)
    {
        //Загрузка расписания
        if (isset($array['exam'])) {
            $path_file = public_path('/files/exams/');
            $path = "/files/exams/";
            $fileName = Str::random(30) . '.' . $array['exam']->extension();
            $array['exam']->move($path_file, $fileName);
            $file = $path . $fileName;

        } else if (isset($array['url'])){
            $url = $array['url'];

        }


        $exams = new self();

        $exams->title =  isset($array['title']) ? $array['title'] : null;
        $exams->type_id =  isset($array['type_id']) ? $array['type_id'] : null;;
        $exams->url = isset($url) ? $url : null;
        $exams->file = isset($file) ? $file : null;
        $exams->status_id =  self::active();
        $exams->save();
    }


    /**
     * Редактирование расписания сессий
     *
     * @param $array
     */
    static public function editExam($array)
    {
        $exam = self::where('id',$array['id'])->first();

        //Загрузка расписания
        if (isset($array['exam'])) {
            $path_file = public_path('/files/exams/');
            $path = "/files/exams/";
            $fileName = Str::random(30) . '.' . $array['exam']->extension();
            $array['exam']->move($path_file, $fileName);
            $file = $path . $fileName;
            //ЕСЛИ ДОБАВЛЕН ФАЙЛ
            $exam->file = $file;
            $exam->url = null;

        } else if (isset($array['url'])){
            // ЕСЛИ ВВЕДЕНО ПОЛЕ ССЫЛКИ
            $url = $array['url'];
            $exam->file = null;
            $exam->url = $url;

        }

        $exam->title = isset($array['title']) ? $array['title'] : $exam->title;
        $exam->type_id = isset($array['type_id']) ? $array['type_id'] : $exam->type_id;
        $exam->save();


    }



    /**
     * Удаление сессии
     * @param $id
     */
    public static function deleteExam($id){
        self::where([
            ['status_id',self::active()],
            ['id',$id]
        ])->update([
            'status_id'=>self::inactive()
        ]);
    }
}
