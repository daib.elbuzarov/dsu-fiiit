<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value' => ['required','string','max:255'],
            'title' => ['nullable','in:1'],
            'key_words' => ['nullable','in:1'],
            'item' => ['required','array','min:1'],
            'item.*.news' => ['nullable','in:1'],
            'item.*.ads' => ['nullable','in:1'],
            'item.*.contests' => ['nullable','in:1'],
        ];
    }
}
