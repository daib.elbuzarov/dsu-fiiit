<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StructureUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dean_img' => ['image'],
            'dean_fio' => ['string'],
            'dean_edu_fio' => ['string'],
            'dean_teach_fio' => ['string'],
            'dean_science_fio' => ['string'],
            'dean_degree_fio' => ['string'],
            'department1_text' => ['string'],
            'department2_text' => ['string'],
            'department3_text' => ['string'],
            'faculty_recom1_text' => ['string'],
            'faculty_recom2_text' => ['string'],
            'faculty_method1_text' => ['string'],
            'faculty_method2_text' => ['string']
        ];
    }
}
