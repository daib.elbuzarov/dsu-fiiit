<?php

namespace App\Http\Requests\Admin\Social;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['nullable', 'integer'],
            'purpose' => ['nullable', 'string'],
            'enter' => ['nullable', 'string'],
            'postscript' => ['nullable', 'string'],
            'leader_name' => ['nullable', 'string','max:255'],
            'leader_post' => ['nullable', 'string','max:255'],
            'img' => ['nullable','file']
        ];
    }
}
