<?php

namespace App\Http\Requests\Admin\DeputyDirector;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>['nullable','string','max:255'],
            'description'=>['nullable','string'],
            'id'=>['nullable','integer'],
            'img' => ['nullable','file'],

        ];
    }
}
