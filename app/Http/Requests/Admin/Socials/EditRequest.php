<?php

namespace App\Http\Requests\Admin\Socials;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  !Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'=>['required','integer'],
            'title' => ['required','string','max:255'],
            'icon' => ['required','string'],
            'link' => ['required','string']
        ];
    }
}
