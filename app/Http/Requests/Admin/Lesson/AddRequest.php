<?php

namespace App\Http\Requests\Admin\Lesson;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type_id' => "required|integer",
            'title' => 'required|string|max:191|min:2',
            //'lesson' => 'nullable|file',
            'lesson' => 'nullable|required_without:url|file',
            'url' => 'nullable|required_without:lesson|string'
        ];
    }
}
