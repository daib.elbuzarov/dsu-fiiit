<?php

namespace App\Http\Requests\Admin\Contest;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['nullable','string'],
            'tag' => ['nullable','string'],
            'author' => ['nullable','string'],
            'type_id' => ['nullable','integer'],
            'url' => ['nullable','string'],
            'prize' => ['nullable','string'],
            'date_end' => ['nullable','date'],
            'id'=> ['nullable','integer'],
        ];
    }
}
