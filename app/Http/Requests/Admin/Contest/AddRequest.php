<?php

namespace App\Http\Requests\Admin\Contest;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type_id' => ['nullable','integer'],
            'title' => ['required','string'],
            'description' => ['nullable','string'],
            'tag' => ['nullable','string'],
            'author' => ['nullable','string'],
            'url' => ['nullable','string'],
            'prize' => ['nullable','string'],
            'date_end' => ['nullable','date']
        ];
    }
}
