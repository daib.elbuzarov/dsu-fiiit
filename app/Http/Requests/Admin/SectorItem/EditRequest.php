<?php

namespace App\Http\Requests\Admin\SectorItem;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'=>'nullable|integer',
            'sector_id' => "nullable|integer",
            'title' => 'nullable|string|max:191|min:2',
            'file' => 'nullable|file',
            'url' => 'nullable|string',
        ];
    }
}
