<?php

namespace App\Http\Requests\Admin\Club;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>['required','string','max:255'],
            'author'=>['nullable','string','max:255'],
            'description'=>['nullable','string'],
            'type_id'=>['nullable','integer'],
            'logo'=>['nullable','file']
        ];
    }
}
