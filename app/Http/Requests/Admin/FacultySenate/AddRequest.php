<?php

namespace App\Http\Requests\Admin\FacultySenate;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type_id' => ['required','integer'],
            'text' => ['required','string'],
            'link' => ['nullable','required_without:file','string'],
            'file' => ['nullable','required_without:link','file'],
        ];
    }
}
