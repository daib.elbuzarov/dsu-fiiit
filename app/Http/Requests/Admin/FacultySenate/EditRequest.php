<?php

namespace App\Http\Requests\Admin\FacultySenate;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required','integer'],
            'type_id' => ['required','integer'],
            'text' => ['required','string'],
            'file' => 'nullable|file',
            'link' => 'nullable|string',
        ];
    }
}
