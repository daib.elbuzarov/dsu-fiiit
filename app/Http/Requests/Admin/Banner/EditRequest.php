<?php

namespace App\Http\Requests\Admin\Banner;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'=>['nullable','integer'],
//            'url'=>['required','string'],
//            'name'=>['required','string','max:255'],
            'link'=>['nullable','string'],
            'title'=>['required','string','max:255'],
            'img' => ['nullable','file'],
            'small_img' => ['nullable','file'],
        ];
    }
}
