<?php

namespace App\Http\Requests\Admin\Banner;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url'=>['nullable','string'],
            'name'=>['nullable','string','max:255'],
            'link'=>['nullable','string'],
            'title'=>['nullable','string','max:255'],
            'img' => ['nullable','file'],
            'small_img' => ['nullable','file'],
        ];
    }
}
