<?php

namespace App\Http\Requests\Admin\PageText;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url'=>['required','string'],
            'name'=>['required','string','max:255'],
            'title'=>['required','string','max:255'],
            'value'=>['nullable','string'],
        ];
    }
}
