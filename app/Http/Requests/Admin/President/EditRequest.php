<?php

namespace App\Http\Requests\Admin\President;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'surname'=>['nullable','string','max:255'],
            'firstname'=>['nullable','string','max:255'],
            'middle_name'=>['nullable','string'],
            'description'=>['nullable','string'],
            'img'=>['nullable','file'],
        ];
    }
}
