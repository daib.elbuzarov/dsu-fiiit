<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'main_img' => ['image'],
            'title' => ['string'],
            'description' => ['string'],
            'type' => ['string','in:news,ad'],
            'img' => ['array'],
            'tag' => ['string'],
        ];
    }
}
