<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InfoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'main_link' => ['string'],
            'banner1_img' => ['image'],
            'banner1_link' => ['string'],
            'banner2_img' => ['image'],
            'banner2_link' => ['string'],
            'feedback_text' => ['string']
        ];
    }
}
