<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Club;
use App\Models\Contest;
use App\Models\PageText;
use App\Models\Post;
use App\Models\Servant;
use App\Models\Types;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Project;

class ScienceController extends Controller
{
    public function sno()
    {
        /**
         * Страничные текста
         */
        $pt = PageText::getPageTexts();

        /**
         *  Структура СНО
         */
        $servants = Servant::getServantsByTypeIdPaginate(Types::getID(Types::CODE_SNO, Servant::table), 3);

        /**
         * Кружки/Клубы/Проекты с пагинацией
         */
        $clubs = Club::getClubsPaginate(3);

        /**
         * Получение проектов с пагинацией
         */
        $projects = Post::getPostsByTypeIdPaginate(Types::getID(Types::CODE_ARTICLE, Post::table), 4);


        /**
         * Получение новостей с тегом Научная деятельность
         */
        $news = Post::getSciencePostsByTypeIdPaginate(Types::getID(Types::CODE_NEWS,Post::table),3);


        return view('users.pages.science.sno', compact('pt', 'servants','clubs','projects','news'));
    }

    public function smu()
    {
        /**
         * Страничные текста
         */
        $pt = PageText::getPageTexts();

        /**
         *  Структура СМУ
         */
        $servants = Servant::getServantsByTypeIdPaginate(Types::getID(Types::CODE_SMU, Servant::table), 6);

        return view('users.pages.science.smu', compact('pt', 'servants'));
    }

    public function contests()
    {
        /**
         * Конкурсы
         */
        $contests = Contest::getContestsByType(Types::CODE_CONTEST, 8);

        /**
         * Гранты
         */
        $grants = Contest::getContestsByType(Types::CODE_GRANT, 8);

        return view('users.pages.science.contests', compact('contests', 'grants'));
    }

    public function contests_more($id)
    {
        $contest = Contest::getOneContestByType(Types::CODE_CONTEST, $id);

        return view('users.pages.science.contests-more', compact('contest'));
    }

    public function grants_more($id)
    {
        $grant = Contest::getOneContestByType(Types::CODE_GRANT, $id);

        return view('users.pages.science.grants-more', compact('grant'));
    }

    public function projects()
    {
        /**
         * Страничные текста
         */
        $pt = PageText::getPageTexts();

        /**
         * Баннеры
         */
        $banners = Banner::getPageBanners();

        /**
         * Получение проектов с пагинацией
         */
        $projects = Post::getPostsByTypeIdPaginate(Types::getID(Types::CODE_ARTICLE, Post::table), 6);


        return view('users.pages.science.projects', compact('pt', 'projects','banners'));
    }

    public function projects_more($id)
    {
        $project = Post::getOnePostByType(Types::CODE_ARTICLE, $id);

        return view('users.pages.science.projects-more', compact('project'));
    }

    public function mugs()
    {
        /**
         * Кружки/Клубы/Проекты с пагинацией
         */
        $clubs = Club::getClubsPaginate(20);

        return view('users.pages.science.mugs', compact('clubs'));
    }
}
