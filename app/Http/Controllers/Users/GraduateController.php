<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\PageText;
use App\Models\Program;
use App\Models\Types;
use Illuminate\Http\Request;

class GraduateController extends Controller
{
    public function bachelor()
    {
        /**
         * Страничные текста
         */
        $pt = PageText::getPageTexts();

        /**
         * Направления бакалавриат
         */
        $bachelor_courses = Course::getCoursesByType(Types::CODE_BACHELOR);

        /**
         * Направления магистратура
         */
        $master_courses = Course::getCoursesByType(Types::CODE_MASTER);

        return view('users.pages.graduates.bachelor',compact('pt','bachelor_courses','master_courses'));
    }

    public function programs()
    {
        /**
         * Направления бакалавриат
         */
        $bachelor_courses = Course::getCoursesByType(Types::CODE_BACHELOR);

        /**
         * Направления магистратура
         */
        $master_courses = Course::getCoursesByType(Types::CODE_MASTER);

        return view('users.pages.graduates.programs',compact('bachelor_courses','master_courses'));
    }
}
