<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\SearchRequest;
use App\Models\Search;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index()
    {
        return view('users.pages.search.index');
    }

    public function postSearch(SearchRequest $request)
    {

        $result = Search::getSearch($request->validated());

        return view('users.pages.search.index',compact('result'));
    }
}
