<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Servant;
use App\Models\Social;
use App\Models\Types;
use Illuminate\Http\Request;

class SocialController extends Controller
{
    public function sd()
    {
        /**
         * Получение информации Студенческий Деканат
         */
        $sd = Social::getOneSocialByTypeId(Types::getID(Types::CODE_DEAN,Social::table));

        /**
         * Получение структуры Студенческий Деканат
         */
        $servants = Servant::getServantsByTypeId(Types::getID(Types::CODE_DEAN,Servant::table));

        return view('users.pages.social.sd',compact('sd','servants'));
    }
    public function kdm()
    {
        /**
         * Получение информации КДМ
         */
        $kdm = Social::getOneSocialByTypeId(Types::getID(Types::CODE_KDM,Social::table));

        /**
         * Получение структуры КДМ
         */
        $servants = Servant::getServantsByTypeId(Types::getID(Types::CODE_KDM,Servant::table));
        return view('users.pages.social.kdm',compact('kdm','servants'));
    }
    public function pk()
    {
        /**
         * Получение информации КДМ
         */
        $pk = Social::getOneSocialByTypeId(Types::getID(Types::CODE_UNION,Social::table));


        /**
         * Получение структуры КДМ
         */
        $servants = Servant::getServantsByTypeId(Types::getID(Types::CODE_UNION,Servant::table));
        return view('users.pages.social.pk',compact('pk','servants'));
    }
}
