<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Club;
use App\Models\Contest;
use App\Models\PageText;
use App\Models\Post;
use App\Models\Types;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        /**
         * Страничные текста
         */
        $pt = PageText::getPageTexts();

        /**
         * Страничные текста
         */
        $banners = Banner::getPageBanners();

        /**
         * Последние новости
         */
        $last_news = Post::getLastNews(3);

        /**
         * Последние объявления
         */
        $last_ads = Post::getLastAds(4);

        /**
         * Гранты
         */
        $grants = Contest::getContestsByType(Types::CODE_GRANT,3);

        /**
         * Конкурсы
         */
        $contests = Contest::getContestsByType(Types::CODE_CONTEST,3);

        /**
         * Проекты
         */
        $projects = Post::getLastArticle(4);

        return view('users.pages.main',compact('pt','last_news','last_ads','contests','grants','projects','banners'));
    }
}
