<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Models\Lesson;
use App\Models\PageText;
use App\Models\ScheduleWeek;
use App\Models\Section;
use App\Models\Types;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function timetable()
    {
        /**
         * Расписание занятий бакалавр заочная
         */
        $ln_dist_bc = Lesson::getLessonsByType(Types::CODE_BACHELOR_DIST);

        /**
         * Расписание занятий бакалавр очная
         */
        $ln_full_bc = Lesson::getLessonsByType(Types::CODE_BACHELOR_FULL);

        /**
         * Расписание занятий бакалавр очно-заочная
         */
        $ln_half_bc = Lesson::getLessonsByType(Types::CODE_BACHELOR_HALF);

        /**
         * Расписание занятий магистр заочная
         */
        $ln_dist_mt = Lesson::getLessonsByType(Types::CODE_CODE_MASTER_DIST);

        /**
         * Расписание занятий магистр очная
         */
        $ln_full_mt = Lesson::getLessonsByType(Types::CODE_CODE_MASTER_FULL);


        /**
         * Расписание занятий магистр очно-заочная
         */
        $ln_half_mt = Lesson::getLessonsByType(Types::CODE_CODE_MASTER_HALF);

        /**
         * Расписание сессий бакалавр заочная
         */
        $exam_dist_bc = Exam::getExamsByType(Types::CODE_BACHELOR_DIST);

        /**
         * Расписание сессий бакалавр очная
         */
        $exam_full_bc = Exam::getExamsByType(Types::CODE_BACHELOR_FULL);

        /**
         * Расписание сессий бакалавр очно-заочная
         */
        $exam_half_bc = Exam::getExamsByType(Types::CODE_BACHELOR_HALF);

        /**
         * Расписание сессий магистр заочная
         */
        $exam_dist_mt = Exam::getExamsByType(Types::CODE_CODE_MASTER_DIST);

        /**
         * Расписание сессий магистр очная
         */
        $exam_full_mt = Exam::getExamsByType(Types::CODE_CODE_MASTER_FULL);

        /**
         * Расписание сессий магистр очно-заочная
         */
        $exam_half_mt = Exam::getExamsByType(Types::CODE_CODE_MASTER_HALF);

        /**
         * Недели
         */
        $weeks = ScheduleWeek::first();


        return view('users.pages.student.timetable',compact('ln_dist_bc', 'ln_full_bc', 'ln_dist_mt', 'ln_full_mt',
            'exam_dist_bc', 'exam_full_bc', 'exam_dist_mt', 'exam_full_mt'
            ,'ln_half_bc','ln_half_mt','exam_half_bc','exam_half_mt','weeks'
        ));
    }

    public function resources()
    {
        $pt = PageText::getPageTexts();
        return view('users.pages.student.resources',compact('pt'));
    }

    public function document()
    {
        /**
         * Получение разделов документов
         */
        $sections = Section::getSections();

        return view('users.pages.student.document',compact('sections'));
    }
}
