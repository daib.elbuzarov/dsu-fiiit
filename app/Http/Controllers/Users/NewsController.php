<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Types;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index()
    {
        /**
         * Получение новостей
         */
        $news = Post::getPostsByTypeId(Types::getID(Types::CODE_NEWS,Post::table));

        /**
         * Получение объявлений
         */
        $ads = Post::getPostsByTypeId(Types::getID(Types::CODE_AD,Post::table));

        return view('users.pages.news.index',compact('news','ads'));
    }

    public function news_more($id)
    {
        $news = Post::getOnePostByType(Types::CODE_NEWS,$id);
        return view('users.pages.news.news-more',compact('news'));
    }
    
    public function advertisement_more($id)
    {
        $ad = Post::getOnePostByType(Types::CODE_AD,$id);

        return view('users.pages.news.advertisement-more',compact('ad'));
    }
}
