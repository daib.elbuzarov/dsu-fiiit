<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\DeputyDirector;
use App\Models\FacultySenate;
use App\Models\PageText;
use App\Models\President;
use App\Models\Types;
use Illuminate\Http\Request;

class StructureController extends Controller
{
    // Структура факультета
    public function index()
    {
        /**
         * Страничные текста
         */
        $pt = PageText::getPageTexts();

        /**
         * Заместители декана
         */
        $directors =  DeputyDirector::getDeputyDirectors(3);

        /**
         * Кафедры
         */
        $departments = Department::getDepartments(3);

        /**
         * Совет факультета
         */
        $faculty_senate = FacultySenate::getSenatesByType(Types::CODE_FACULTY_SENATE,200);

        /**
         * Учебно-методический совет
         */
        $study_senate = FacultySenate::getSenatesByType(Types::CODE_STUDY_SENATE,200);

        /**
         * Декан
         */
        $president = President::first();
        if ($president === null) {
            $president = President::createPresident();
        }



        return view('users.pages.structure.index',compact('pt','directors','departments','faculty_senate','study_senate','president'));
    }
}

