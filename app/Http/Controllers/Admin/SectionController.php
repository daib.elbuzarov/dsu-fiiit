<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Section\AddRequest;
use App\Http\Requests\Admin\Section\EditRequest;
use App\Models\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SectionController extends IndexController
{

    /**
     * Вывод разделов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $sections = Section::getSections();

        return view('admin.pages.sections.list', [
            'sections' => $sections
        ]);
    }


    /**
     * Страница добавления раздела
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        return view('admin.pages.sections.add');
    }


    /**
     * Страница редактирование раздела
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $section = Section::getOneSection($id);

        return view('admin.pages.sections.edit', [
            'section' => $section
        ]);
    }


    /**
     * Запрос на добление раздела
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){

        Section::createSection($request->validated());

        return redirect()->route('admin.sections-list');
    }


    /**
     * Запрос на редактирование раздела
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        Section::updateSection($request->validated());

        return redirect()->route('admin.sections-list');

    }


    /**
     * Запрос на удаление раздела
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        Section::deleteSection($id);

        return redirect()->route('admin.sections-list');
    }
}
