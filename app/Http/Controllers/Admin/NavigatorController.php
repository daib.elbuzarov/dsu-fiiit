<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Navigator\AddRequest;
use App\Http\Requests\Admin\Navigator\EditRequest;
use App\Models\Navigator;
use App\Models\Types;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class NavigatorController extends IndexController
{
    /**
     * Вывод навигаторов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $navigator_bot = Navigator::getNavigatorsForType(Types::getID(Types::CODE_BOTTOM,Navigator::table));

        $navigator_top = Navigator::getNavigatorsForType(Types::getID(Types::CODE_TOP,Navigator::table));



        return view('admin.pages.navigators.list', [
            'navigator_bot' => $navigator_bot,
            'navigator_top' => $navigator_top,

        ]);
    }


    /**
     * Страница добавления навигатора
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        $types = Types::getTypes(Navigator::table);
        return view('admin.pages.navigators.add',compact('types'));
    }


    /**
     * Страница редактирование навигатора
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $navigator = Navigator::getOneNavigator($id);
        $types = Types::getTypes(Navigator::table);

        return view('admin.pages.navigators.edit', [
            'navigator' => $navigator,
            'types'=>$types
        ]);
    }


    /**
     * Запрос на добление навигатора
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){

        Navigator::create($request->validated());

        return redirect()->route('admin.navigators-list');
    }


    /**
     * Запрос на редактирование навигатора
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        Navigator::edit($request->validated());

        return redirect()->route('admin.navigators-list');

    }

    /**
     * Запрос на изменение позиции навигатора вниз
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePosDown($id)
    {
        $navigator = new Navigator();
        $navigator->changePosDown($id);

        return redirect()->route('admin.navigators-list');
    }

    /**
     * Запрос на изменение позиции навигатора вверх
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePosUp($id)
    {
        $navigator = new Navigator();
        $navigator->changePosUp($id);

        return redirect()->route('admin.navigators-list');
    }

    /**
     * Запрос на удаление навигатора
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        Navigator::dropNavigator($id);

        return redirect()->route('admin.navigators-list');
    }
}
