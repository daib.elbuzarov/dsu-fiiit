<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Contest\AddRequest;
use App\Http\Requests\Admin\Contest\EditRequest;
use App\Models\Contest;
use App\Models\Types;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ContestController extends IndexController
{
    /**
     * Вывод конкурсов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $contests = Contest::getContests();



        return view('admin.pages.contests.list', [
            'contests' => $contests,

        ]);
    }


    /**
     * Страница добавления конкурса
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        $types = Types::getTypes(Contest::table);

        return view('admin.pages.contests.add',compact('types'));
    }


    /**
     * Страница редактирование конкурса
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $contest = Contest::getOneContest($id);
        $types = Types::getTypes(Contest::table);
        return view('admin.pages.contests.edit', [
            'contest' => $contest,
            'types'=>$types
        ]);
    }


    /**
     * Запрос на добление конкурса
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){

        Contest::createContest($request->validated());

        return redirect()->route('admin.contests-list');
    }


    /**
     * Запрос на редактирование конкурса
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        Contest::updateContest($request->validated());

        return redirect()->route('admin.contests-list');

    }


    /**
     * Запрос на удаление конкурса
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        Contest::deleteContest($id);

        return redirect()->route('admin.contests-list');
    }
}
