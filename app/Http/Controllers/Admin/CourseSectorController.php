<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CourseSector\AddRequest;
use App\Http\Requests\Admin\CourseSector\EditRequest;
use App\Models\Course;
use App\Models\CourseSector;
use App\Models\Types;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CourseSectorController extends IndexController
{
    /**
     * Вывод подразделов направлений
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index($id)
    {
        $course = Course::getOneCourse($id);

        return view('admin.pages.course_sectors.list', [
            'course' => $course,
        ]);
    }


    /**
     * Страница добавления подраздела направления
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add($id)
    {
        $course = Course::getOneCourse($id);
        return view('admin.pages.course_sectors.add',compact('course'));
    }


    /**
     * Страница редактирование подраздела направления
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $course_sector = CourseSector::getOneCourseSector($id);
        $types = Types::getTypes(CourseSector::table);
        return view('admin.pages.course_sectors.edit', [
            'course_sector' => $course_sector,
            'types'=>$types
        ]);
    }


    /**
     * Запрос на добление подраздела направления
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){

        CourseSector::createCourseSector($request->validated());

        return redirect()->route('admin.course_sectors-list',['id'=>$request->course_id]);
    }


    /**
     * Запрос на редактирование подраздела направления
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        CourseSector::updateCourseSector($request->validated());

        return redirect()->route('admin.course_sectors-list',['id'=>$request->course_id]);

    }


    /**
     * Запрос на удаление подраздела направления
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        $course_id = CourseSector::find($id)->course_id;
        CourseSector::deleteCourseSector($id);

        return redirect()->route('admin.course_sectors-list',['id'=>$course_id]);
    }
}
