<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Partner;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Partner\AddRequest;
use App\Http\Requests\Admin\Partner\EditRequest;
use Illuminate\Support\Facades\Redirect;

class PartnersController extends IndexController
{
    /**
     * Вывод партнеров
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $partners = Partner::getPartners();

        return view('admin.pages.partners.list', [
            'partners' => $partners
        ]);
    }


    /**
     * Страница добавления партнера
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        return view('admin.pages.partners.add');
    }


    /**
     * Страница редактирование партнера
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $partner = Partner::getPartner($id);

        return view('admin.pages.partners.edit', [
            'partner' => $partner
        ]);
    }


    /**
     * Запрос на добление партнера
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){

        Partner::createPartner($request->validated());

        return redirect()->route('admin.partners-list');
    }


    /**
     * Запрос на редактирование партнера
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        Partner::updatePartner($request->validated());

        return redirect()->route('admin.partners-list');

    }


    /**
     * Запрос на удаление партнера
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        Partner::deletePartner($id);

        return redirect()->route('admin.partners-list');
    }
}
