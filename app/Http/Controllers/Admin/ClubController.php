<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Club\AddRequest;
use App\Http\Requests\Admin\Club\EditRequest;
use App\Models\Club;
use App\Models\Types;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ClubController extends IndexController
{

    /**
     * Вывод клубов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $clubs = Club::getClubs();



        return view('admin.pages.clubs.list', [
            'clubs' => $clubs,

        ]);
    }


    /**
     * Страница добавления клуба
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        $types = Types::getTypes(Club::table);

        return view('admin.pages.clubs.add',compact('types'));
    }


    /**
     * Страница редактирование клуба
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $club = Club::getOneClub($id);
        $types = Types::getTypes(Club::table);
        return view('admin.pages.clubs.edit', [
            'club' => $club,
            'types'=>$types
        ]);
    }


    /**
     * Запрос на добление клуба
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){

        Club::createClub($request->validated());

        return redirect()->route('admin.clubs-list');
    }


    /**
     * Запрос на редактирование клуба
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        Club::updateClub($request->validated());

        return redirect()->route('admin.clubs-list');

    }


    /**
     * Запрос на удаление клуба
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        Club::deleteClub($id);

        return redirect()->route('admin.clubs-list');
    }
}
