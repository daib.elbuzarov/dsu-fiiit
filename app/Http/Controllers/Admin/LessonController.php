<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Lesson\AddRequest;
use App\Http\Requests\Admin\Lesson\EditRequest;
use App\Models\Lesson;
use App\Models\ScheduleWeek;
use App\Models\Types;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;

class LessonController extends IndexController
{
    /**
     * Вывод расписания занятий
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $lessons = Lesson::getLessons();

        return view('admin.pages.lessons.list', [
            'lessons' => $lessons,
        ]);
    }


    /**
     * Страница добавления расписания
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        $types = Types::getTypes(Lesson::table);
        return view('admin.pages.lessons.add',compact('types'));
    }


    /**
     * Страница редактирование расписания
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $lesson = Lesson::getOneLesson($id);
        $types = Types::getTypes(Lesson::table);
        return view('admin.pages.lessons.edit', [
            'lesson' => $lesson,
            'types'=>$types
        ]);
    }


    /**
     * Запрос на добление расписания
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){
        Lesson::createLesson($request->validated());

        return redirect()->route('admin.lessons-list');
    }


    /**
     * Запрос на редактирование расписания
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        Lesson::editLesson($request->validated());

        return redirect()->route('admin.lessons-list');

    }


    /**
     * Запрос на удаление расписания
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        Lesson::deleteLesson($id);

        return redirect()->route('admin.lessons-list');
    }


    public function editScheduleWeek()
    {
        $weeks = ScheduleWeek::first();
        return view('admin.pages.weeks.weeks',[
            'weeks'=>$weeks
        ]);
    }

    public function postEditScheduleWeek(Request $request)
    {
        $data = $request->toArray();
        $weeks = ScheduleWeek::first() ?? new ScheduleWeek();


        $path_file = public_path('/files/weeks/');
        $path = "/files/weeks/";
        $fileName = Str::random(30) . '.' . $data['file']->extension();
        $data['file']->move($path_file, $fileName);
        $file = $path . $fileName;

        $weeks->url = $file;
        $weeks->save();

        return redirect()->back();

    }
}
