<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\FacultySenate\AddRequest;
use App\Http\Requests\Admin\FacultySenate\EditRequest;
use App\Models\FacultySenate;
use App\Models\Types;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class FacultySenateController extends IndexController
{
    /**
     * Вывод советов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $senates = FacultySenate::getSenates();

        return view('admin.pages.senate.list', [
            'senates' => $senates
        ]);
    }


    /**
     * Страница добавления совета
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        $types = Types::getTypes(FacultySenate::table);
        return view('admin.pages.senate.add',compact('types'));
    }


    /**
     * Страница редактирование совета
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $senate = FacultySenate::getOneSenate($id);
        $types = Types::getTypes(FacultySenate::table);

        return view('admin.pages.senate.edit', [
            'senate' => $senate,
            'types' => $types
        ]);
    }


    /**
     * Запрос на добление совета
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){

        FacultySenate::createSenate($request->validated());

        return redirect()->route('admin.senates-list');
    }


    /**
     * Запрос на редактирование совета
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        FacultySenate::updateSenate($request->validated());

        return redirect()->route('admin.senates-list');

    }


    /**
     * Запрос на удаление совета
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        FacultySenate::deleteSenate($id);

        return redirect()->route('admin.senates-list');
    }
}
