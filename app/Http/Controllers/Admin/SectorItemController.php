<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SectorItem\AddRequest;
use App\Http\Requests\Admin\SectorItem\EditRequest;
use App\Models\CourseSector;
use App\Models\SectorItem;
use App\Models\Types;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SectorItemController extends IndexController
{
    /**
     * Вывод подразделов направлений
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index($id)
    {
        $course_sector = CourseSector::getOneCourseSector($id);

        return view('admin.pages.sector_items.list', [
            'course_sector' => $course_sector,
        ]);
    }


    /**
     * Страница добавления подраздела направления
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add($id)
    {
        $course_sector = CourseSector::getOneCourseSector($id);
        return view('admin.pages.sector_items.add',compact('course_sector'));
    }


    /**
     * Страница редактирование подраздела направления
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $sector_item = SectorItem::getOneSectorItem($id);
        $types = Types::getTypes(SectorItem::table);
        return view('admin.pages.sector_items.edit', [
            'sector_item' => $sector_item,
            'types'=>$types
        ]);
    }


    /**
     * Запрос на добление подраздела направления
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){

        SectorItem::createSectorItem($request->validated());

        return redirect()->route('admin.sector_items-list',['id'=>$request->sector_id]);
    }


    /**
     * Запрос на редактирование подраздела направления
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        SectorItem::editSectorItem($request->validated());

        return redirect()->route('admin.sector_items-list',['id'=>$request->sector_id]);

    }


    /**
     * Запрос на удаление подраздела направления
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        $sector_id = SectorItem::find($id)->sector_id;
        SectorItem::deleteSectorItem($id);

        return redirect()->route('admin.sector_items-list',['id'=>$sector_id]);
    }
}
