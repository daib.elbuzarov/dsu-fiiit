<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Course\AddRequest;
use App\Http\Requests\Admin\Course\EditRequest;
use App\Models\Course;
use App\Models\Types;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CourseController extends IndexController
{
    /**
     * Вывод направлений
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $courses = Course::getCourses();

        return view('admin.pages.courses.list', [
            'courses' => $courses,
        ]);
    }


    /**
     * Страница добавления направления
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        $types = Types::getTypes(Course::table);
        return view('admin.pages.courses.add',compact('types'));
    }


    /**
     * Страница редактирование направления
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $course = Course::getOneCourse($id);
        $types = Types::getTypes(Course::table);
        return view('admin.pages.courses.edit', [
            'course' => $course,
            'types'=>$types
        ]);
    }


    /**
     * Запрос на добление направления
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){

        Course::createCourse($request->validated());

        return redirect()->route('admin.courses-list');
    }


    /**
     * Запрос на редактирование направления
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        Course::updateCourse($request->validated());

        return redirect()->route('admin.courses-list');

    }


    /**
     * Запрос на удаление направления
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        Course::deleteCourse($id);

        return redirect()->route('admin.courses-list');
    }
}
