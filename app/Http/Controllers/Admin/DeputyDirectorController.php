<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DeputyDirector\AddRequest;
use App\Http\Requests\Admin\DeputyDirector\EditRequest;
use App\Models\DeputyDirector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class DeputyDirectorController extends IndexController
{
    /**
     * Вывод замов декана
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $deputy_directors = DeputyDirector::getAllDeputyDirectors();

        return view('admin.pages.deputy_directors.list', [
            'deputy_directors' => $deputy_directors,
        ]);
    }


    /**
     * Страница добавления зама декана
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        return view('admin.pages.deputy_directors.add');
    }


    /**
     * Страница редактирование зама декана
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $deputy_director = DeputyDirector::getOneDeputyDirector($id);
        return view('admin.pages.deputy_directors.edit', [
            'deputy_director' => $deputy_director,
        ]);
    }


    /**
     * Запрос на добление зама декана
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){

        DeputyDirector::createDeputyDirector($request->validated());

        return redirect()->route('admin.deputy_directors-list');
    }


    /**
     * Запрос на редактирование зама декана
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        DeputyDirector::updateDeputyDirector($request->validated());

        return redirect()->route('admin.deputy_directors-list');

    }


    /**
     * Запрос на удаление зама декана
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        DeputyDirector::deleteDeputyDirector($id);

        return redirect()->route('admin.deputy_directors-list');
    }
}
