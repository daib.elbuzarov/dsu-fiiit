<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Banner\AddRequest;
use App\Http\Requests\Admin\Banner\EditRequest;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class BannerController extends Controller
{
    /**
     * Вывод баннеров
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $banners = Banner::getAllBanners();

        return view('admin.pages.banners.list', [
            'banners' => $banners
        ]);
    }


    /**
     * Страница добавления баннера
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        return view('admin.pages.banners.add');
    }


    /**
     * Страница редактирование баннера
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $banner = Banner::getOneBanner($id);

        return view('admin.pages.banners.edit', [
            'banner' => $banner
        ]);
    }


    /**
     * Запрос на добление баннера
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){

        Banner::createBanner($request->validated());

        return redirect()->route('admin.banners-list');
    }


    /**
     * Запрос на редактирование баннера
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        Banner::updateBanner($request->validated());

        return redirect()->route('admin.banners-list');

    }

    /**
     * Запрос на удаление баннера
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        Banner::deleteBanner($id);

        return redirect()->route('admin.banners-list');
    }
}
