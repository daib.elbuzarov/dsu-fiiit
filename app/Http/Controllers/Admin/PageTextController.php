<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PageText\AddRequest;
use App\Http\Requests\Admin\PageText\EditRequest;
use App\Models\PageText;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PageTextController extends IndexController
{
    /**
     * Вывод текстов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $page_texts = PageText::getAllPageTexts();

        return view('admin.pages.page_texts.list', [
            'page_texts' => $page_texts
        ]);
    }


    /**
     * Страница добавления текста
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        return view('admin.pages.page_texts.add');
    }


    /**
     * Страница редактирование текста
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $page_text = PageText::getOnePageText($id);

        return view('admin.pages.page_texts.edit', [
            'page_text' => $page_text
        ]);
    }


    /**
     * Запрос на добление текста
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){

        PageText::createPageText($request->validated());

        return redirect()->route('admin.page_texts-list');
    }


    /**
     * Запрос на редактирование текста
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        PageText::updatePageText($request->validated());

        return redirect()->route('admin.page_texts-list');

    }

    /**
     * Запрос на удаление текста
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        PageText::deletePageText($id);

        return redirect()->route('admin.page_texts-list');
    }


}
