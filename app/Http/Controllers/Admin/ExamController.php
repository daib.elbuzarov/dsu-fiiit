<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Exam\AddRequest;
use App\Http\Requests\Admin\Exam\EditRequest;
use App\Models\Exam;
use App\Models\Types;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ExamController extends IndexController
{
    /**
     * Вывод сессий
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $exams = Exam::getExams();

        return view('admin.pages.exams.list', [
            'exams' => $exams,
        ]);
    }


    /**
     * Страница добавления сессии
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        $types = Types::getTypes(Exam::table);
        return view('admin.pages.exams.add',compact('types'));
    }


    /**
     * Страница редактирование сессии
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $exam = Exam::getOneExam($id);
        $types = Types::getTypes(Exam::table);
        return view('admin.pages.exams.edit', [
            'exam' => $exam,
            'types'=>$types
        ]);
    }


    /**
     * Запрос на добление сессии
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){

        Exam::createExam($request->validated());

        return redirect()->route('admin.exams-list');
    }


    /**
     * Запрос на редактирование сессии
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        Exam::editExam($request->validated());

        return redirect()->route('admin.exams-list');

    }


    /**
     * Запрос на удаление сессии
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        Exam::deleteExam($id);

        return redirect()->route('admin.exams-list');
    }
}
