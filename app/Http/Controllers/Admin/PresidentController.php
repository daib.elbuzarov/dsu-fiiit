<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\President\EditRequest;
use App\Models\President;
use App\Models\Servant;
use Illuminate\Http\Request;

class PresidentController extends Controller
{

    // Страница редактирования декана
    public function edit()
    {
        $president = President::first();
        if (!isset($president)) {
            $president = President::createPresident();
        }
        return view('admin.pages.president.edit', compact('president'));
    }

    // Запрос на редактирование декана
    public function presidentEdit(EditRequest $request)
    {
        President::updatePresident($request->validated());

        return redirect()->route('admin.president-edit')->with(['message' => 'Информация изменена']);

    }
}

