<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Department\AddRequest;
use App\Http\Requests\Admin\Department\EditRequest;
use App\Models\Department;
use App\Models\Types;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class DepartmentController extends IndexController
{
    /**
     * Вывод кафедр
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $departments = Department::getAllDepartments();

        return view('admin.pages.departments.list', [
            'departments' => $departments,
        ]);
    }


    /**
     * Страница добавления кафедры
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        return view('admin.pages.departments.add');
    }


    /**
     * Страница редактирование кафедры
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $department = Department::getOneDepartment($id);
        return view('admin.pages.departments.edit', [
            'department' => $department,
        ]);
    }


    /**
     * Запрос на добление кафедры
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){

        Department::createDepartment($request->validated());

        return redirect()->route('admin.departments-list');
    }


    /**
     * Запрос на редактирование кафедры
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        Department::updateDepartment($request->validated());

        return redirect()->route('admin.departments-list');

    }


    /**
     * Запрос на удаление кафедры
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        Department::deleteDepartment($id);

        return redirect()->route('admin.departments-list');
    }
}
