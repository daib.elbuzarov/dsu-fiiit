<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Post\AddRequest;
use App\Http\Requests\Admin\Post\EditRequest;
use App\Models\Post;
use App\Models\Types;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PostController extends Controller
{
    /**
     * Вывод постов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index($id)
    {
        $posts = Post::getPostsByTypeId($id);
        $type = Types::find($id);


        return view('admin.pages.posts.list', [
            'posts' => $posts,
            'type'=>$type

        ]);
    }


    /**
     * Страница добавления поста
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add($id)
    {
        $type = Types::find($id);

        return view('admin.pages.posts.add',compact('type'));
    }


    /**
     * Страница редактирование поста
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $post = Post::getOnePost($id);
        return view('admin.pages.posts.edit', [
            'post' => $post,
        ]);
    }


    /**
     * Запрос на добление поста
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){

        Post::createPost($request->validated());

        return redirect()->route('admin.posts-list',['id'=>$request->type_id]);
    }


    /**
     * Запрос на редактирование поста
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        Post::updatePost($request->validated());

        return redirect()->route('admin.posts-list',['id'=>$request->type_id]);

    }


    /**
     * Запрос на удаление поста
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        $type_id = Post::getOnePost($id)->type_id;
        Post::deletePost($id);

        return redirect()->route('admin.posts-list',['id'=>$type_id]);
    }
}
