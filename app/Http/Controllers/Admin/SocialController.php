<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Social\EditRequest;
use App\Models\Social;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SocialController extends Controller
{
    /**
     * Страница редактирование представителя
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $social = Social::getOneSocialByTypeId($id);
        return view('admin.pages.socials.edit', [
            'social' => $social,
        ]);
    }


    /**
     * Запрос на редактирование представителя
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        Social::updateSocial($request->validated());

        return redirect()->route('admin.socials-edit',['id'=>$request->type_id]);

    }


}
