<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Report\AddRequest;
use App\Http\Requests\Admin\Report\EditRequest;
use App\Models\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ReportController extends IndexController
{
    /**
     * Вывод отчетов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $reports = Report::getReports();

        return view('admin.pages.reports.list', [
            'reports' => $reports
        ]);
    }


    /**
     * Страница добавления отчета
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        return view('admin.pages.reports.add');
    }


    /**
     * Страница редактирование отчета
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $report = Report::getOneReport($id);

        return view('admin.pages.reports.edit', [
            'report' => $report
        ]);
    }


    /**
     * Запрос на добление отчета
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){

        Report::createReport($request->validated());

        return redirect()->route('admin.reports-list');
    }


    /**
     * Запрос на редактирование отчета
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        Report::updateReport($request->validated());

        return redirect()->route('admin.reports-list');

    }

    /**
     * Запрос на удаление отчета
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        Report::deleteReport($id);

        return redirect()->route('admin.reports-list');
    }
}
