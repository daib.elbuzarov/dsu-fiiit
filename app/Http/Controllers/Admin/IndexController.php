<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Servant;
use App\Models\Types;
use App\Traits\ModelTools;
use Database\Seeders\RefreshSeeder;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    // Людишки
    public $servants;

    public function __construct()
    {
        $this->servants = Types::getTypes(Servant::table);
    }

    // Admin main
    public function home()
    {
        return view('admin.pages.main');
    }


    public function seed()
    {
        $seeder = new RefreshSeeder();
        $seeder->run();
    }

    public function fixStatuses()
    {
        ModelTools::fixStatuses();
        Post::whereNotNull('id')->update([
            'type_id'=>Post::getTypeId(Types::CODE_NEWS)
        ]);
    }
}
