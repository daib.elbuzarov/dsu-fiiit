<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Document\AddRequest;
use App\Http\Requests\Admin\Document\EditRequest;
use App\Models\Document;
use App\Models\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class DocumentController extends IndexController
{
    /**
     * Вывод документов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $documents = Document::getDocuments();

        return view('admin.pages.documents.list', [
            'documents' => $documents,
        ]);
    }


    /**
     * Страница добавления документа
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        $sections = Section::getSections();
        return view('admin.pages.documents.add',compact('sections'));
    }


    /**
     * Страница редактирование документа
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $document = Document::getOneDocument($id);
        $sections = Section::getSections();
        return view('admin.pages.documents.edit', [
            'document' => $document,
            'sections'=>$sections
        ]);
    }


    /**
     * Запрос на добление документа
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){

        Document::createDocument($request->validated());

        return redirect()->route('admin.documents-list');
    }


    /**
     * Запрос на редактирование документа
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        Document::editDocument($request->validated());

        return redirect()->route('admin.documents-list');

    }


    /**
     * Запрос на удаление документа
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        Document::deleteDocument($id);

        return redirect()->route('admin.documents-list');
    }
}
