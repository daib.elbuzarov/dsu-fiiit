<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Contacts\EditRequest;
use App\Models\Contacts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ContactsController extends IndexController
{
    /**
     * Страница редактирования контактных данных
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $contacts = Contacts::getContacts();

        return view('admin.pages.contacts.list',compact('contacts'));
    }

    /**
     * Запрос на редактирование контактных данных
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        Contacts::updateContact($request->validated());

        return redirect()->route('admin.contacts-list');

    }
}
