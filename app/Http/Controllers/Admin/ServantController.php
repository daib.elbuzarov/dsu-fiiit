<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Servant\AddRequest;
use App\Http\Requests\Admin\Servant\EditRequest;
use App\Models\Servant;
use App\Models\Types;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ServantController extends Controller
{
    /**
     * Вывод работников
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index($id)
    {
        $servants = Servant::getServantsByTypeId($id);
        $type = Types::find($id);


        return view('admin.pages.servants.list', [
            'servants' => $servants,
            'type'=>$type

        ]);
    }


    /**
     * Страница добавления работника
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add($id)
    {
        $type = Types::find($id);

        return view('admin.pages.servants.add',compact('type'));
    }


    /**
     * Страница редактирование работника
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $servant = Servant::getOneServant($id);
        return view('admin.pages.servants.edit', [
            'servant' => $servant,
        ]);
    }


    /**
     * Запрос на добление работника
     * @param  AddRequest  $request
     *
     * @return Redirect
     */
    public function postAdd(AddRequest $request){

        Servant::createServant($request->validated());

        return redirect()->route('admin.servants-list',['id'=>$request->type_id]);
    }


    /**
     * Запрос на редактирование работника
     * @param  EditRequest  $request
     *
     * @return Redirect
     */
    public function postEdit(EditRequest $request){
        Servant::updateServant($request->validated());

        return redirect()->route('admin.servants-list',['id'=>$request->type_id]);

    }


    /**
     * Запрос на удаление работника
     * @param  $id
     *
     * @return Redirect
     */
    public function delete($id){
        $type_id = Servant::getOneServant($id)->type_id;

        Servant::deleteServant($id);

        return redirect()->route('admin.servants-list',['id'=>$type_id]);
    }
}
