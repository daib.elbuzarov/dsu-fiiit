<?php

namespace App\Http\Middleware;

use App\Models\Types;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccessAdministrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();
        //Если нет роли то на заглушку
        if($user->type_id != Types::getID(Types::CODE_ADMINISTRATOR,User::table)) return abort(404);

        return $next($request);
    }
}
