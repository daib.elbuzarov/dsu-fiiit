<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

// Главная
Route::get('/',[\App\Http\Controllers\Users\IndexController::class,'index'])->name('home');


// Факультет - новости и объявления
Route::get('/news',[\App\Http\Controllers\Users\NewsController::class,'index'])->name('news');
Route::get('/news/news-more/{id}',[\App\Http\Controllers\Users\NewsController::class,'news_more'])->name('news.news-more');
Route::get('/news/advertisement-more/{id}',[\App\Http\Controllers\Users\NewsController::class,'advertisement_more'])->name('news.advertisement-more');

// Факультет - структура
Route::get('/structure',[\App\Http\Controllers\Users\StructureController::class,'index'])->name('structure');

// Научная деятельность - СНО
Route::get('/science/sno',[\App\Http\Controllers\Users\ScienceController::class,'sno'])->name('science.sno');

// Научная деятельность - СМУ
Route::get('/science/smu',[\App\Http\Controllers\Users\ScienceController::class,'smu'])->name('science.smu');

// Научная деятельность - Конкурсы и гранты
Route::get('/science/contests',[\App\Http\Controllers\Users\ScienceController::class,'contests'])->name('science.contests');
Route::get('/science/contests-more/{id}',[\App\Http\Controllers\Users\ScienceController::class,'contests_more'])->name('science.contests-more');
Route::get('/science/grants-more/{id}',[\App\Http\Controllers\Users\ScienceController::class,'grants_more'])->name('science.grants-more');


// Научная деятельность - Проектная деятельность
Route::get('/science/projects',[\App\Http\Controllers\Users\ScienceController::class,'projects'])->name('science.projects');
Route::get('/science/projects-more/{id}',[\App\Http\Controllers\Users\ScienceController::class,'projects_more'])->name('science.projects-more');

// Научная деятельность - Научные проекты и кружки
Route::get('/science/mugs',[\App\Http\Controllers\Users\ScienceController::class,'mugs'])->name('science.mugs');

// Абитуриентам *поменять роуты на candidate*
Route::get('/candidate',[\App\Http\Controllers\Users\GraduateController::class,'bachelor'])->name('candidate');
Route::get('/candidate/programs',[\App\Http\Controllers\Users\GraduateController::class,'programs'])->name('candidate.programs');

// Социальная работа - Студенческий деканат
Route::get('/social/sd',[\App\Http\Controllers\Users\SocialController::class,'sd'])->name('social.sd');

// Социальная работа - Комитет по делам молодежи
Route::get('/social/kdm',[\App\Http\Controllers\Users\SocialController::class,'kdm'])->name('social.kdm');

// Социальная работа - Профсоюзный комитет
Route::get('/social/pk',[\App\Http\Controllers\Users\SocialController::class,'pk'])->name('social.pk');

// Студентам - расписание
Route::get('/student/timetable',[\App\Http\Controllers\Users\StudentController::class,'timetable'])->name('timetable');
Route::get('/student/resources',[\App\Http\Controllers\Users\StudentController::class,'resources'])->name('resources');
Route::get('/student/document',[\App\Http\Controllers\Users\StudentController::class,'document'])->name('document');

// Страница поиска
Route::get('/search',[\App\Http\Controllers\Users\SearchController::class,'index'])->name('search');
Route::post('/search',[\App\Http\Controllers\Users\SearchController::class,'postSearch'])->name('post.search');

// Auth
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

//Подключение роутов админки
Route::group(['middleware'=>['web','auth:sanctum','accessAdministrator']],function(){
    include 'include/admin.php';
});
