<?php

//use App\Http\Controllers\Admin\NavigatorController;
use App\Http\Controllers\Admin\SocialController;
use App\Http\Controllers\Admin\CourseController;
use App\Http\Controllers\Admin\ContactsController;
use App\Http\Controllers\Admin\PageTextController;
use App\Http\Controllers\Admin\ClubController;
use App\Http\Controllers\Admin\ContestController;
use App\Http\Controllers\Admin\DepartmentController;
use App\Http\Controllers\Admin\DeputyDirectorController;
use App\Http\Controllers\Admin\CourseSectorController;
use App\Http\Controllers\Admin\DocumentController;
use App\Http\Controllers\Admin\SectionController;
use App\Http\Controllers\Admin\FacultySenateController;
use App\Http\Controllers\Admin\LessonController;
use App\Http\Controllers\Admin\ExamController;
use App\Http\Controllers\Admin\SectorItemController;
use App\Http\Controllers\Admin\ServantController;
use App\Http\Controllers\Admin\PostController;
use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\PresidentController;

//ГЛАВНАЯ
Route::get('/admin',[\App\Http\Controllers\Admin\IndexController::class, 'home'])->name('admin.home');

Route::get('/seed/$2y$10$BaALN2tA1YFS566OXLkFm.XmGp4S10m8KEgnkhArs00rFTIgtS8W6',[\App\Http\Controllers\Admin\IndexController::class, 'seed']);
Route::get('/fix1509/5asetruueutqwe12OXLkFm.XmGp4S10m8KEgnkhArs00rFTIgtS8W6',[\App\Http\Controllers\Admin\IndexController::class, 'fixStatuses']);



//КОНТАКТЫ
Route::get('/admin/contacts',[ContactsController::class, 'index'])->name('admin.contacts-list');
Route::post('/admin/contactsspostedit',[ContactsController::class, 'postEdit'])->name('admin.contacts-post-edit');

//СТРАНИЧНЫЕ ТЕКСТА
Route::get('/admin/page_texts',[PageTextController::class, 'index'])->name('admin.page_texts-list');
Route::get('/admin/page_textsadd',[PageTextController::class, 'add'])->name('admin.page_texts-add');
Route::get('/admin/page_textsedit/{id}',[PageTextController::class, 'edit'])->name('admin.page_texts-edit');
Route::get('/admin/page_textsdelete/{id}',[PageTextController::class, 'delete'])->name('admin.page_texts-delete');
Route::post('/admin/page_textspostadd',[PageTextController::class, 'postAdd'])->name('admin.page_texts-post-add');
Route::post('/admin/page_textspostedit',[PageTextController::class, 'postEdit'])->name('admin.page_texts-post-edit');

//БАННЕРЫ
Route::get('/admin/banners',[BannerController::class, 'index'])->name('admin.banners-list');
Route::get('/admin/bannersadd',[BannerController::class, 'add'])->name('admin.banners-add');
Route::get('/admin/bannersedit/{id}',[BannerController::class, 'edit'])->name('admin.banners-edit');
Route::get('/admin/bannersdelete/{id}',[BannerController::class, 'delete'])->name('admin.banners-delete');
Route::post('/admin/bannerspostadd',[BannerController::class, 'postAdd'])->name('admin.banners-post-add');
Route::post('/admin/bannerspostedit',[BannerController::class, 'postEdit'])->name('admin.banners-post-edit');


//НАВИГАТОРЫ
//Route::get('/admin/navigators',[NavigatorController::class, 'index'])->name('admin.navigators-list');
//Route::get('/admin/navigatorsadd',[NavigatorController::class, 'add'])->name('admin.navigators-add');
//Route::get('/admin/navigatorsedit/{id}',[NavigatorController::class, 'edit'])->name('admin.navigators-edit');
//Route::get('/admin/navigatorsdelete/{id}',[NavigatorController::class, 'delete'])->name('admin.navigators-delete');
//Route::post('/admin/navigatorspostadd',[NavigatorController::class, 'postAdd'])->name('admin.navigators-post-add');
//Route::post('/admin/navigatorspostedit',[NavigatorController::class, 'postEdit'])->name('admin.navigators-post-edit');
//Route::get('/admin/navigatorposup/{id}',[NavigatorController::class, 'changePosUp'])->name('admin.navigators-post-up');
//Route::get('/admin/navigatorposdown/{id}',[NavigatorController::class, 'changePosDown'])->name('admin.navigators-post-down');

//КЛУБЫ
Route::get('/admin/clubs',[ClubController::class, 'index'])->name('admin.clubs-list');
Route::get('/admin/clubsadd',[ClubController::class, 'add'])->name('admin.clubs-add');
Route::get('/admin/clubsedit/{id}',[ClubController::class, 'edit'])->name('admin.clubs-edit');
Route::get('/admin/clubsdelete/{id}',[ClubController::class, 'delete'])->name('admin.clubs-delete');
Route::post('/admin/clubspostadd',[ClubController::class, 'postAdd'])->name('admin.clubs-post-add');
Route::post('/admin/clubspostedit',[ClubController::class, 'postEdit'])->name('admin.clubs-post-edit');

//КОНКУРСЫ
Route::get('/admin/contests',[ContestController::class, 'index'])->name('admin.contests-list');
Route::get('/admin/contestsadd',[ContestController::class, 'add'])->name('admin.contests-add');
Route::get('/admin/contestsedit/{id}',[ContestController::class, 'edit'])->name('admin.contests-edit');
Route::get('/admin/contestsdelete/{id}',[ContestController::class, 'delete'])->name('admin.contests-delete');
Route::post('/admin/contestspostadd',[ContestController::class, 'postAdd'])->name('admin.contests-post-add');
Route::post('/admin/contestspostedit',[ContestController::class, 'postEdit'])->name('admin.contests-post-edit');

//КАФЕДРЫ
Route::get('/admin/departments',[DepartmentController::class, 'index'])->name('admin.departments-list');
Route::get('/admin/departmentsadd',[DepartmentController::class, 'add'])->name('admin.departments-add');
Route::get('/admin/departmentsedit/{id}',[DepartmentController::class, 'edit'])->name('admin.departments-edit');
Route::get('/admin/departmentsdelete/{id}',[DepartmentController::class, 'delete'])->name('admin.departments-delete');
Route::post('/admin/departmentspostadd',[DepartmentController::class, 'postAdd'])->name('admin.departments-post-add');
Route::post('/admin/departmentspostedit',[DepartmentController::class, 'postEdit'])->name('admin.departments-post-edit');

//ЗАМЫ ДЕКАНА
Route::get('/admin/deputy_directors',[DeputyDirectorController::class, 'index'])->name('admin.deputy_directors-list');
Route::get('/admin/deputy_directorsadd',[DeputyDirectorController::class, 'add'])->name('admin.deputy_directors-add');
Route::get('/admin/deputy_directorsedit/{id}',[DeputyDirectorController::class, 'edit'])->name('admin.deputy_directors-edit');
Route::get('/admin/deputy_directorsdelete/{id}',[DeputyDirectorController::class, 'delete'])->name('admin.deputy_directors-delete');
Route::post('/admin/deputy_directorspostadd',[DeputyDirectorController::class, 'postAdd'])->name('admin.deputy_directors-post-add');
Route::post('/admin/deputy_directorspostedit',[DeputyDirectorController::class, 'postEdit'])->name('admin.deputy_directors-post-edit');

//Направления
Route::get('/admin/courses',[CourseController::class, 'index'])->name('admin.courses-list');
Route::get('/admin/coursesadd',[CourseController::class, 'add'])->name('admin.courses-add');
Route::get('/admin/coursesedit/{id}',[CourseController::class, 'edit'])->name('admin.courses-edit');
Route::get('/admin/coursesdelete/{id}',[CourseController::class, 'delete'])->name('admin.courses-delete');
Route::post('/admin/coursespostadd',[CourseController::class, 'postAdd'])->name('admin.courses-post-add');
Route::post('/admin/coursespostedit',[CourseController::class, 'postEdit'])->name('admin.courses-post-edit');

//Подразделы направлений
Route::get('/admin/course_sectors/{id}',[CourseSectorController::class, 'index'])->name('admin.course_sectors-list');
Route::get('/admin/course_sectorsadd/{id}',[CourseSectorController::class, 'add'])->name('admin.course_sectors-add');
Route::get('/admin/course_sectorsedit/{id}',[CourseSectorController::class, 'edit'])->name('admin.course_sectors-edit');
Route::get('/admin/course_sectorsdelete/{id}',[CourseSectorController::class, 'delete'])->name('admin.course_sectors-delete');
Route::post('/admin/course_sectorspostadd',[CourseSectorController::class, 'postAdd'])->name('admin.course_sectors-post-add');
Route::post('/admin/course_sectorspostedit',[CourseSectorController::class, 'postEdit'])->name('admin.course_sectors-post-edit');

//Записи подразделов направлений
Route::get('/admin/sector_items/{id}',[SectorItemController::class, 'index'])->name('admin.sector_items-list');
Route::get('/admin/sector_itemsadd/{id}',[SectorItemController::class, 'add'])->name('admin.sector_items-add');
Route::get('/admin/sector_itemsedit/{id}',[SectorItemController::class, 'edit'])->name('admin.sector_items-edit');
Route::get('/admin/sector_itemsdelete/{id}',[SectorItemController::class, 'delete'])->name('admin.sector_items-delete');
Route::post('/admin/sector_itemspostadd',[SectorItemController::class, 'postAdd'])->name('admin.sector_items-post-add');
Route::post('/admin/sector_itemspostedit',[SectorItemController::class, 'postEdit'])->name('admin.sector_items-post-edit');

//Документы
Route::get('/admin/documents',[DocumentController::class, 'index'])->name('admin.documents-list');
Route::get('/admin/documentsadd',[DocumentController::class, 'add'])->name('admin.documents-add');
Route::get('/admin/documentsedit/{id}',[DocumentController::class, 'edit'])->name('admin.documents-edit');
Route::get('/admin/documentsdelete/{id}',[DocumentController::class, 'delete'])->name('admin.documents-delete');
Route::post('/admin/documentspostadd',[DocumentController::class, 'postAdd'])->name('admin.documents-post-add');
Route::post('/admin/documentspostedit',[DocumentController::class, 'postEdit'])->name('admin.documents-post-edit');

//Разделы документов
Route::get('/admin/sections',[SectionController::class, 'index'])->name('admin.sections-list');
Route::get('/admin/sectionsadd',[SectionController::class, 'add'])->name('admin.sections-add');
Route::get('/admin/sectionsedit/{id}',[SectionController::class, 'edit'])->name('admin.sections-edit');
Route::get('/admin/sectionsdelete/{id}',[SectionController::class, 'delete'])->name('admin.sections-delete');
Route::post('/admin/sectionspostadd',[SectionController::class, 'postAdd'])->name('admin.sections-post-add');
Route::post('/admin/sectionspostedit',[SectionController::class, 'postEdit'])->name('admin.sections-post-edit');

//Совет факультета
Route::get('/admin/senates',[FacultySenateController::class, 'index'])->name('admin.senates-list');
Route::get('/admin/senatesadd',[FacultySenateController::class, 'add'])->name('admin.senates-add');
Route::get('/admin/senatesedit/{id}',[FacultySenateController::class, 'edit'])->name('admin.senates-edit');
Route::get('/admin/senatesdelete/{id}',[FacultySenateController::class, 'delete'])->name('admin.senates-delete');
Route::post('/admin/senatespostadd',[FacultySenateController::class, 'postAdd'])->name('admin.senates-post-add');
Route::post('/admin/senatespostedit',[FacultySenateController::class, 'postEdit'])->name('admin.senates-post-edit');

//Расписания занятий
Route::get('/admin/lessons',[LessonController::class, 'index'])->name('admin.lessons-list');
Route::get('/admin/lessonsadd',[LessonController::class, 'add'])->name('admin.lessons-add');
Route::get('/admin/lessonsedit/{id}',[LessonController::class, 'edit'])->name('admin.lessons-edit');
Route::get('/admin/lessonsdelete/{id}',[LessonController::class, 'delete'])->name('admin.lessons-delete');
Route::post('/admin/lessonspostadd',[LessonController::class, 'postAdd'])->name('admin.lessons-post-add');
Route::post('/admin/lessonspostedit',[LessonController::class, 'postEdit'])->name('admin.lessons-post-edit');

//Расписания сессий
Route::get('/admin/exams',[ExamController::class, 'index'])->name('admin.exams-list');
Route::get('/admin/examsadd',[ExamController::class, 'add'])->name('admin.exams-add');
Route::get('/admin/examsedit/{id}',[ExamController::class, 'edit'])->name('admin.exams-edit');
Route::get('/admin/examsdelete/{id}',[ExamController::class, 'delete'])->name('admin.exams-delete');
Route::post('/admin/examspostadd',[ExamController::class, 'postAdd'])->name('admin.exams-post-add');
Route::post('/admin/examspostedit',[ExamController::class, 'postEdit'])->name('admin.exams-post-edit');


//Работники
Route::get('/admin/servants/{id}',[ServantController::class, 'index'])->name('admin.servants-list');
Route::get('/admin/servantsadd/{id}',[ServantController::class, 'add'])->name('admin.servants-add');
Route::get('/admin/servantsedit/{id}',[ServantController::class, 'edit'])->name('admin.servants-edit');
Route::get('/admin/servantsdelete/{id}',[ServantController::class, 'delete'])->name('admin.servants-delete');
Route::post('/admin/servantspostadd',[ServantController::class, 'postAdd'])->name('admin.servants-post-add');
Route::post('/admin/servantspostedit',[ServantController::class, 'postEdit'])->name('admin.servants-post-edit');


//Посты
Route::get('/admin/posts/{id}',[PostController::class, 'index'])->name('admin.posts-list');
Route::get('/admin/postsadd/{id}',[PostController::class, 'add'])->name('admin.posts-add');
Route::get('/admin/postsedit/{id}',[PostController::class, 'edit'])->name('admin.posts-edit');
Route::get('/admin/postsdelete/{id}',[PostController::class, 'delete'])->name('admin.posts-delete');
Route::post('/admin/postspostadd',[PostController::class, 'postAdd'])->name('admin.posts-post-add');
Route::post('/admin/postspostedit',[PostController::class, 'postEdit'])->name('admin.posts-post-edit');

//Представители
Route::get('/admin/socialsedit/{id}',[SocialController::class, 'edit'])->name('admin.socials-edit');
Route::post('/admin/socialspostedit',[SocialController::class, 'postEdit'])->name('admin.socials-post-edit');

//Декан
Route::get('/admin/president/edit',[PresidentController::class, 'edit'])->name('admin.president-edit');
Route::post('/admin/president/postedit',[PresidentController::class, 'presidentEdit'])->name('admin.presidentpostedit');

// Расписание недель
Route::post('/admin/week/postedit',[LessonController::class, 'postEditScheduleWeek'])->name('admin.post.schedule-weeks');
Route::get('/admin/week/edit',[LessonController::class, 'editScheduleWeek'])->name('admin.schedule-weeks');

Route::get('/project/upd/y6goAHBR9QtSm9Z1ahr.UvyRCbJIQUnCG/lfw.RNyAjrs5cdaNm',function (){
   exec("script.sh");
   return redirect()->home();
});
