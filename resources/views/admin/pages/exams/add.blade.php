@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Добавление расписания</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item">Расписание</li>
                                <li class="breadcrumb-item active">Добавление расписания</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mb-4">Добавление расписания</h4>


                            <form action="{{ route('admin.exams-post-add') }}" method="POST"
                                  enctype="multipart/form-data">
                                @csrf

                                <div class="form-group row">
                                    <label for="example-name-input" class="col-md-2 col-form-label">Название расписания</label>
                                    <div class="col-md-10">
                                        <input class="form-control" name="title" type="text" id="example-name-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-type_id-input" class="col-md-2 col-form-label">Тип</label>
                                    <div class="col-md-10">
                                        <select name="type_id" class="form-control">
                                            @foreach($types as $type)
                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-fio-input" class="col-md-2 col-form-label">Файл</label>
                                    <div class="col-md-10">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="exam" id="customFile">
                                            <label class="custom-file-label" for="customFile">Добавление расписания</label>
                                        </div>
                                    </div>
                                </div>
                                Или
                                <div class="form-group row">
                                    <label for="example-name-input" class="col-md-2 col-form-label">Ссылка</label>
                                    <div class="col-md-10">
                                        <input class="form-control" name="url" type="text" id="example-name-input">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-tel-input" class="col-md-2 col-form-label"></label>
                                    <div class="col-md-10">
                                        <button type="submit" href="{{route('admin.exams-add')}}"
                                                class="btn btn-success waves-effect waves-light"
                                                style="margin-bottom: 10px">
                                            <i class="ri-survey-line align-middle mr-2"></i> ДОБАВИТЬ
                                        </button>
                                    </div>
                                </div>

                            </form>

                        </div>


                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection