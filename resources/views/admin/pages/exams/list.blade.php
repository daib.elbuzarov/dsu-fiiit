
@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Расписание сессий</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item active">Расписание сессий</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <!-- end row -->

            <!-- end row -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mb-4">Список расписаний</h4>

                            <a href="{{route('admin.exams-add')}}" class="btn btn-success waves-effect waves-light" style="margin-bottom: 10px">
                                <i class="ri-survey-line align-middle mr-2"></i> Добавить расписание
                            </a>

                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Тип</th>
                                <th>Расписание</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($exams as $key => $exam)
                                <tr>
                                    <td>{{$exam->title}}</td>
                                    <td>{{$exam->type->name}}</td>
                                    @if($exam->file)<td> <a href="{{asset($exam->file)}} " download>Скачать</a> </td>@endif
                                    @if($exam->url)<td> <a href="{{$exam->url}} " target="_blank" >Перейти</a> </td>@endif
                                    <td>
                                        <div class="icon-demo-content">
                                            <a class="" href="{{route('admin.exams-edit', ['id' => $exam->id])}}">
                                                <i class="ri-edit-2-line"></i>
                                            </a>
                                            <a class="" href="{{route('admin.exams-delete', ['id' => $exam->id])}}">
                                                <i class="ri-delete-bin-2-line"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection