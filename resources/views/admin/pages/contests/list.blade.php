
@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Конкурсы</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item active">Конкурсы</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <!-- end row -->

            <!-- end row -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mb-4">Список конкурсов</h4>

                            <a href="{{route('admin.contests-add')}}" class="btn btn-success waves-effect waves-light" style="margin-bottom: 10px">
                                <i class="ri-star-s-line align-middle mr-2"></i> Добавить конкурс
                            </a>

                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>Тип</th>
                                <th>Название</th>
                                <th>Тег</th>
                                <th>Автор</th>
                                <th>Ссылка</th>
                                <th>Приз</th>
                                <th>Окончание заявок</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($contests as $key => $contest)
                                <tr>
                                    <td>{{$contest->type->name}}</td>
                                    <td>{{$contest->title}}</td>
                                    <td>{{$contest->tag}}</td>
                                    <td>{{$contest->author}}</td>
                                    <td>{{$contest->url}}</td>
                                    <td>{{$contest->prize}}</td>
                                    <td>{{date('d.m.Y', strtotime($contest->date_end))}}</td>
                                    <td>
                                        <div class="icon-demo-content">
                                            <a class="" href="{{route('admin.contests-edit', ['id' => $contest->id])}}">
                                                <i class="ri-edit-2-line"></i>
                                            </a>
                                            <a class="" href="{{route('admin.contests-delete', ['id' => $contest->id])}}">
                                                <i class="ri-delete-bin-2-line"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection