
@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Добавление конкурса</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item">Конкурсы</li>
                                <li class="breadcrumb-item active">Добавление конкурса</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mb-4">Добавление конкурса</h4>


                            <form action="{{ route('admin.contests-post-add') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                    <label for="example-type_id-input" class="col-md-2 col-form-label">Тип</label>
                                    <div class="col-md-10">
                                        <select name="type_id" class="form-control">
                                            @foreach($types as $type)
                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-name-input" class="col-md-2 col-form-label">Название</label>
                                    <div class="col-md-10">
                                        <input class="form-control" name="title" type="text" id="example-name-input">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-post-input" class="col-md-2 col-form-label">Автор</label>
                                    <div class="col-md-10">
                                        <input class="form-control" name="author" type="text" id="example-post-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-post-input" class="col-md-2 col-form-label">Тег</label>
                                    <div class="col-md-10">
                                        <input class="form-control" name="tag" type="text" id="example-post-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-post-input" class="col-md-2 col-form-label">Сайт конкурса</label>
                                    <div class="col-md-10">
                                        <input class="form-control" name="url" type="text" id="example-post-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-post-input" class="col-md-2 col-form-label">Призовой фонд</label>
                                    <div class="col-md-10">
                                        <input class="form-control" name="prize" type="text" id="example-post-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-post-input" class="col-md-2 col-form-label">Подача заявок до</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="date" value="2021-03-01"
                                               id="example-date-input" name="date_end">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-description-input" class="col-md-2 col-form-label">Описание</label>
                                    <div class="col-md-10">
                                        <textarea name="description" class="summernote"></textarea>
                                    </div>
                                </div>



                                <div class="form-group row">
                                    <label for="example-tel-input" class="col-md-2 col-form-label"></label>
                                    <div class="col-md-10">
                                        <button type="submit" href="{{route('admin.contests-add')}}" class="btn btn-success waves-effect waves-light" style="margin-bottom: 10px">
                                            <i class="ri-star-s-line align-middle mr-2"></i> ДОБАВИТЬ
                                        </button>
                                    </div>
                                </div>

                            </form>

                        </div>


                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection