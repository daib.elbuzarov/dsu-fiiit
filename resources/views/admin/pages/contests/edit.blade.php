
@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Редактирование конкурса</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item">Конкурсы</li>
                                <li class="breadcrumb-item active">Редактирование конкурса</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mb-4">Редактирование конкурса</h4>

                            <form action="{{ route('admin.contests-post-edit') }}" method="POST" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group row">
                                    <label for="example-type_id-input" class="col-md-2 col-form-label">Тип</label>
                                    <div class="col-md-10">
                                        <select name="type_id" class="form-control">
                                            @foreach($types as $type)
                                                <option @if($type->id == $contest->type_id) selected @endif value="{{$type->id}}">{{$type->name}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-name-input" class="col-md-2 col-form-label">Название</label>
                                    <div class="col-md-10">
                                        <input class="form-control" value="{{$contest->title}}" name="title" type="text" id="example-name-input">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-post-input" class="col-md-2 col-form-label">Автор</label>
                                    <div class="col-md-10">
                                        <input class="form-control" value="{{$contest->author}}" name="author" type="text" id="example-post-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-post-input" class="col-md-2 col-form-label">Тег</label>
                                    <div class="col-md-10">
                                        <input class="form-control" value="{{$contest->tag}}" name="tag" type="text" id="example-post-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-post-input"  class="col-md-2 col-form-label">Сайт конкурса</label>
                                    <div class="col-md-10">
                                        <input class="form-control" value="{{$contest->url}}" name="url" type="text" id="example-post-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-post-input" class="col-md-2 col-form-label">Призовой фонд</label>
                                    <div class="col-md-10">
                                        <input class="form-control" value="{{$contest->prize}}" name="prize" type="text" id="example-post-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-post-input" class="col-md-2 col-form-label">Подача заявок до</label>
                                    <div class="col-md-10">
                                        <input class="form-control"  type="date" value="{{date('Y-m-d', strtotime($contest->date_end))}}"
                                               id="example-date-input" name="date_end">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-description-input" class="col-md-2 col-form-label">Описание</label>
                                    <div class="col-md-10">
                                        <textarea name="description" class="summernote">{{$contest->description}}</textarea>
                                    </div>
                                </div>

                                <input type="hidden" name="id" value="{{$contest->id}}">



                                <div class="form-group row">
                                    <label for="example-tel-input" class="col-md-2 col-form-label"></label>
                                    <div class="col-md-10">
                                        <button type="submit" href="{{route('admin.contests-add')}}" class="btn btn-primary waves-effect waves-light" style="margin-bottom: 10px">
                                            <i class="ri-edit-2-line align-middle mr-2"></i> СОХРАНИТЬ ИЗМЕНЕНИЯ
                                        </button>
                                    </div>
                                </div>

                            </form>

                        </div>


                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection