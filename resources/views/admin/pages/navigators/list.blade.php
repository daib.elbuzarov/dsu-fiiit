
@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Навигаторы</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item active">Навигаторы</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <!-- end row -->

            <!-- end row -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">



                            <a href="{{route('admin.navigators-add')}}" class="btn btn-success waves-effect waves-light" style="margin-bottom: 10px">
                                <i class="ri-navigation-line align-middle mr-2"></i> Добавить навигатор
                            </a>
                            <h4 class="card-title mb-4">Верхний навигатор</h4>

                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>Ссылка</th>
                                    <th>Действие</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($navigator_top as $key => $navigator)
                                    <tr>
                                        <td>{{$navigator->title}}</td>
                                        <td>{{$navigator->link}}</td>
                                        <td>
                                            <div class="icon-demo-content">
                                                <a class="col-lg-4" href="{{route('admin.navigators-edit', ['id' => $navigator->id])}}">
                                                    <i class="ri-edit-2-line"></i>
                                                </a>
                                                <a class="col-lg-4" href="{{route('admin.navigators-delete', ['id' => $navigator->id])}}">
                                                    <i class="ri-delete-bin-2-line"></i>
                                                </a>
                                                @if($key + 1 < count($navigator_top))
                                                    <a class="col-lg-4"
                                                       href="{{route('admin.navigators-post-down', ['id' => $navigator->id])}}">
                                                        <i class="dripicons-chevron-down"></i>
                                                    </a>
                                                @endif
                                                @if($key > 0)
                                                    <a class="col-lg-4"
                                                       href="{{route('admin.navigators-post-up', ['id' => $navigator->id])}}">
                                                        <i class="dripicons-chevron-up"></i>
                                                    </a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <h4 class="card-title mb-4">Нижний навигатор</h4>
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>Ссылка</th>
                                    <th>Действие</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($navigator_bot as $key => $navigator)
                                    <tr>
                                        <td>{{$navigator->title}}</td>
                                        <td>{{$navigator->link}}</td>
                                        <td>
                                            <div class="icon-demo-content">
                                                <a class="col-lg-4" href="{{route('admin.navigators-edit', ['id' => $navigator->id])}}">
                                                    <i class="ri-edit-2-line"></i>
                                                </a>
                                                <a class="col-lg-4" href="{{route('admin.navigators-delete', ['id' => $navigator->id])}}">
                                                    <i class="ri-delete-bin-2-line"></i>
                                                </a>
                                                @if($key + 1 < count($navigator_bot))
                                                    <a class="col-lg-4"
                                                       href="{{route('admin.navigators-post-down', ['id' => $navigator->id])}}">
                                                        <i class="dripicons-chevron-down"></i>
                                                    </a>
                                                @endif
                                                @if($key > 0)
                                                    <a class="col-lg-4"
                                                       href="{{route('admin.navigators-post-up', ['id' => $navigator->id])}}">
                                                        <i class="dripicons-chevron-up"></i>
                                                    </a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection