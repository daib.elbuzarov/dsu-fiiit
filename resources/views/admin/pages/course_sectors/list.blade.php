
@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">{{$course->title}}</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item active">{{$course->title}}</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <!-- end row -->

            <!-- end row -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mb-4">Список подразделов направленя '{{$course->title}}'</h4>

                            <a href="{{route('admin.course_sectors-add',['id'=>$course->id])}}" class="btn btn-success waves-effect waves-light" style="margin-bottom: 10px">
                                <i class="ri-bar-chart-horizontal-line align-middle mr-2"></i> Добавить подраздел
                            </a>

                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Номер</th>
                                <th>Подробнее</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($course->sectors as $key => $course_sector)
                                <tr>
                                    <td>{{$course_sector->title}}</td>
                                    <td>{{$course_sector->number}}</td>
                                    <td><a href="{{route('admin.sector_items-list',['id'=>$course_sector->id])}}">Подробнее</a></td>

                                    <td>
                                        <div class="icon-demo-content">
                                            <a class="" href="{{route('admin.course_sectors-edit', ['id' => $course_sector->id])}}">
                                                <i class="ri-edit-2-line"></i>
                                            </a>
                                            <a class="" href="{{route('admin.course_sectors-delete', ['id' => $course_sector->id])}}">
                                                <i class="ri-delete-bin-2-line"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection