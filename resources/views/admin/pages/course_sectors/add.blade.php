
@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Добавление подраздела направления</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item">Подраздел направления</li>
                                <li class="breadcrumb-item active">Добавление подраздела направления</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mb-4">Добавление подраздела направления</h4>


                            <form action="{{ route('admin.course_sectors-post-add') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                    <label for="example-title-input" class="col-md-2 col-form-label">Номер</label>
                                    <div class="col-md-10">
                                        <input class="form-control" name="number" type="text" id="example-title-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-title-input" class="col-md-2 col-form-label">Название</label>
                                    <div class="col-md-10">
                                        <input class="form-control" name="title" type="text" id="example-title-input">
                                    </div>
                                </div>
                                <input type="hidden" name="course_id" value="{{$course->id}}">

                                <div class="form-group row">
                                    <label for="example-tel-input" class="col-md-2 col-form-label"></label>
                                    <div class="col-md-10">
                                        <button type="submit" href="#" class="btn btn-success waves-effect waves-light" style="margin-bottom: 10px">
                                            <i class="ri-bar-chart-horizontal-line align-middle mr-2"></i> ДОБАВИТЬ
                                        </button>
                                    </div>
                                </div>

                            </form>

                        </div>


                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection