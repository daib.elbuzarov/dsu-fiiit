
@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Расписание занятий</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item active">Расписание занятий</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <!-- end row -->

            <!-- end row -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mb-4">Список расписаний</h4>

                            <a href="{{route('admin.lessons-add')}}" class="btn btn-success waves-effect waves-light" style="margin-bottom: 10px">
                                <i class="ri-clipboard-line align-middle mr-2"></i> Добавить расписание
                            </a>

                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Тип</th>
                                <th>Расписание</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($lessons as $key => $lesson)
                                <tr>
                                    <td>{{$lesson->title}}</td>
                                    <td>{{$lesson->type->name}}</td>
                                    @if($lesson->file)<td> <a href="{{asset($lesson->file)}} " download>Скачать</a> </td>@endif
                                    @if($lesson->url)<td> <a href="{{$lesson->url}} " target="_blank" >Перейти</a> </td>@endif
                                    <td>
                                        <div class="icon-demo-content">
                                            <a class="" href="{{route('admin.lessons-edit', ['id' => $lesson->id])}}">
                                                <i class="ri-edit-2-line"></i>
                                            </a>
                                            <a class="" href="{{route('admin.lessons-delete', ['id' => $lesson->id])}}">
                                                <i class="ri-delete-bin-2-line"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection