
@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Редактирование баннера</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item">Баннеры</li>
                                <li class="breadcrumb-item active">Редактирование баннера</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mb-4">Редактирование баннера</h4>

                            <form action="{{ route('admin.banners-post-edit') }}" method="POST" enctype="multipart/form-data">
                                @csrf


{{--                                <div class="form-group row">--}}
{{--                                    <label for="example-name-input" class="col-md-2 col-form-label">Название переменой</label>--}}
{{--                                    <div class="col-md-10">--}}
{{--                                        <input class="form-control" name="name" type="text" value="{{$banner->name}}" id="example-name-input">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group row">--}}
{{--                                    <label for="example-url-input" class="col-md-2 col-form-label">URL</label>--}}
{{--                                    <div class="col-md-10">--}}
{{--                                        <input class="form-control" name="url" value="{{$banner->url}}" type="text" id="example-url-input">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="form-group row">
                                    <label for="example-title-input" class="col-md-2 col-form-label">Название</label>
                                    <div class="col-md-10">
                                        <input class="form-control" name="title" value="{{$banner->title}}" type="text" id="example-title-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-title-input" class="col-md-2 col-form-label">Ссылка</label>
                                    <div class="col-md-10">
                                        <input class="form-control" name="link" value="{{$banner->link}}" type="text" id="example-title-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-fio-input" class="col-md-2 col-form-label">Изображение</label>
                                    <div class="col-md-10">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="img" id="customFile">
                                            <label class="custom-file-label" for="customFile">Добавление фото</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-fio-input" class="col-md-2 col-form-label">Изображение(328x162)</label>
                                    <div class="col-md-10">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="small_img" id="customFile">
                                            <label class="custom-file-label" for="customFile">Добавление фото</label>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" name="id" value="{{$banner->id}}">

                                

                                <div class="form-group row">
                                    <label for="example-tel-input" class="col-md-2 col-form-label"></label>
                                    <div class="col-md-10">
                                        <button type="submit" href="{{route('admin.banners-add')}}" class="btn btn-primary waves-effect waves-light" style="margin-bottom: 10px">
                                            <i class="ri-edit-2-line align-middle mr-2"></i> СОХРАНИТЬ ИЗМЕНЕНИЯ
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>


                    </div>
                </div>
            </div>

            <div class="col-xl-6">
                <div class="card">
                    <div class="card-body">

                        <h4 class="card-title">Баннер</h4>

                        <div class="zoom-gallery">
                            <a class="float-left" href="{{asset($banner->headImg->img->url)}}" title="Project 1"><img src="{{asset($banner->headImg->img->url)}}" alt="" width="275"></a>
                            <a class="float-left" href="{{asset($banner->smallImg->img->url)}}" title="Project 1"><img src="{{asset($banner->smallImg->img->url)}}" alt="" width="275"></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection