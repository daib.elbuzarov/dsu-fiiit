
@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Баннеры</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item active">Баннеры</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <!-- end row -->

            <!-- end row -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mb-4">Список баннеров</h4>

{{--                            <a href="{{route('admin.banners-add')}}" class="btn btn-success waves-effect waves-light" style="margin-bottom: 10px">--}}
{{--                                <i class="ri-file-add-line align-middle mr-2"></i> Добавить баннер--}}
{{--                            </a>--}}

                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                <tr>
{{--                                    <th>Название переменой(потом скрыть)</th>--}}
                                    <th>URL</th>
                                    <th>Название</th>
                                    <th>Ссылка</th>
                                    <th>Действие</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($banners as $key => $banner)
                                    <tr>
                                        <td>{{$banner->name}}</td>
                                        <td>{{$banner->url}}</td>
                                        <td>{{$banner->title}}</td>
                                        <td>{{$banner->link}}</td>
                                        <td>
                                            <div class="icon-demo-content">
                                                <a class="col-lg-4" href="{{route('admin.banners-edit', ['id' => $banner->id])}}">
                                                    <i class="ri-edit-2-line"></i>
                                                </a>
{{--                                                <a class="col-lg-4" href="{{route('admin.banners-delete', ['id' => $banner->id])}}">--}}
{{--                                                    <i class="ri-delete-bin-2-line"></i>--}}
{{--                                                </a>--}}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection