@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Редактирование поста</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item">Посты</li>
                                <li class="breadcrumb-item active">Редактирование поста</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">


                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mb-4">Редактирование поста</h4>

                            <form action="{{ route('admin.posts-post-edit') }}" method="POST"
                                  enctype="multipart/form-data">
                                @csrf

                                <div class="form-group row">
                                    <label for="example-fio-input" class="col-md-2 col-form-label">Фото</label>
                                    <div class="col-md-10">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="img[]" multiple id="customFile">
                                            <label class="custom-file-label" for="customFile">Добавление фото</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-name-input" class="col-md-2 col-form-label">Название</label>
                                    <div class="col-md-10">
                                        <input class="form-control" value="{{$post->title}}" name="title" type="text"
                                               id="example-name-input">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-post-input" class="col-md-2 col-form-label">Тег</label>
                                    <div class="col-md-10">
                                        <input class="form-control" value="{{$post->tag}}" name="tag" type="text"
                                               id="example-post-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-description-input"
                                           class="col-md-2 col-form-label">Описание</label>
                                    <div class="col-md-10">
                                        <div class="col-md-10">
                                            <textarea name="description" class="summernote">{{$post->description}}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" name="type_id" value="{{$post->type_id}}">
                                <input type="hidden" name="id" value="{{$post->id}}">


                                <div class="form-group row">
                                    <label for="example-tel-input" class="col-md-2 col-form-label"></label>
                                    <div class="col-md-10">
                                        <button type="submit" href="#" class="btn btn-primary waves-effect waves-light"
                                                style="margin-bottom: 10px">
                                            <i class="ri-edit-2-line align-middle mr-2"></i> СОХРАНИТЬ ИЗМЕНЕНИЯ
                                        </button>
                                    </div>
                                </div>

                            </form>

                        </div>


                    </div>
                </div>


                @if(count($post->sliderImg))
                    <div class="card-body">

                        <h4 class="card-title">Галерея</h4>
                        <div class="popup-gallery">
                            @foreach($post->sliderImg as $key => $join)
                                <a class="float-left" href="{{asset($join->img->url)}}" title="Project {{$key+1}}">
                                    <div class="img-fluid">
                                        <img src="{{asset($join->img->url)}}" alt="" width="120">
                                    </div>
                                </a>
                            @endforeach
                        </div>

                    </div>

                @endif
            </div>


            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection
{{--<div class="carousel-inner" role="listbox">--}}
{{--    <div class="carousel-item">--}}
{{--        <img class="d-block img-fluid" src="assets/images/small/img-4.jpg" alt="First slide">--}}
{{--    </div>--}}
{{--    <div class="carousel-item">--}}
{{--        <img class="d-block img-fluid" src="assets/images/small/img-5.jpg" alt="Second slide">--}}
{{--    </div>--}}
{{--    <div class="carousel-item active">--}}
{{--        <img class="d-block img-fluid" src="assets/images/small/img-6.jpg" alt="Third slide">--}}
{{--    </div>--}}
{{--</div>--}}

{{--<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">--}}
{{--    <span class="carousel-control-prev-icon" aria-hidden="true"></span>--}}
{{--    <span class="sr-only">Previous</span>--}}
{{--</a>--}}

{{--<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">--}}
{{--    <span class="carousel-control-next-icon" aria-hidden="true"></span>--}}
{{--    <span class="sr-only">Next</span>--}}
{{--</a>--}}

{{--<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">--}}
{{--    <div class="carousel-inner" role="listbox">--}}
{{--        <div class="carousel-item active">--}}
{{--            <img class="d-block img-fluid" src="assets/images/small/img-4.jpg" alt="First slide">--}}
{{--        </div>--}}
{{--        <div class="carousel-item">--}}
{{--            <img class="d-block img-fluid" src="assets/images/small/img-5.jpg" alt="Second slide">--}}
{{--        </div>--}}
{{--        <div class="carousel-item">--}}
{{--            <img class="d-block img-fluid" src="assets/images/small/img-6.jpg" alt="Third slide">--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">--}}
{{--        <span class="carousel-control-prev-icon" aria-hidden="true"></span>--}}
{{--        <span class="sr-only">Previous</span>--}}
{{--    </a>--}}
{{--    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">--}}
{{--        <span class="carousel-control-next-icon" aria-hidden="true"></span>--}}
{{--        <span class="sr-only">Next</span>--}}
{{--    </a>--}}
{{--</div>--}}