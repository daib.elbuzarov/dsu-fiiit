
@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Добавление партнера</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item">Партнеры</li>
                                <li class="breadcrumb-item active">Добавление партнера</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mb-4">Добавление партнера</h4>


                            <form action="{{ route('admin.partners-post-add') }}" method="POST" enctype="multipart/form-data">
                           @csrf
                                <div class="form-group row">
                                    <label for="example-title-input" class="col-md-2 col-form-label">Фото</label>
                                    <div class="col-md-10">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="image" id="customFile">
                                            <label class="custom-file-label" for="customFile">Добавление фото</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-title-input" class="col-md-2 col-form-label">Название</label>
                                    <div class="col-md-10">
                                        <input class="form-control" name="title" type="text" id="example-title-input">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-description-input" class="col-md-2 col-form-label">Описание</label>
                                    <div class="col-md-10">
                                        <textarea name="description" class="summernote" placeholder="Описание"></textarea>
                                    </div>
                                </div>



                                <div class="form-group row">
                                    <label for="example-tel-input" class="col-md-2 col-form-label"></label>
                                    <div class="col-md-10">
                                        <button type="submit" href="{{route('admin.partners-add')}}" class="btn btn-success waves-effect waves-light" style="margin-bottom: 10px">
                                            <i class="ri-user-2-fill align-middle mr-2"></i> ДОБАВИТЬ
                                        </button>
                                    </div>
                                </div>

                            </form>

                        </div>


                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection