@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Редактирование контактных данных</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item">Контакты</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">


                            <form action="{{ route('admin.contacts-post-edit') }}" method="POST" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group row">
                                    <label for="example-address-input" class="col-md-2 col-form-label">Адрес</label>
                                    <div class="col-md-10">
                                        <input class="form-control" name="address" value="{{$contacts->address}}" type="text" id="example-address-input">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-post-input" class="col-md-2 col-form-label">Номера</label>
                                    <div class="col-md-10">
                                        <textarea id="basicpill-number-input" name="number" class="form-control" rows="1" style="height: 100px;">{{$contacts->number}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-email-input" class="col-md-2 col-form-label">Эл. почта</label>
                                    <div class="col-md-10">
                                        <input class="form-control" name="email" value="{{$contacts->email}}" type="email" id="example-email-input">
                                    </div>
                                </div>



                                <input type="hidden" name="id" value="{{$contacts->id}}">



                                <div class="form-group row">
                                    <label for="example-tel-input" class="col-md-2 col-form-label"></label>
                                    <div class="col-md-10">
                                        <button type="submit" href="#" class="btn btn-primary waves-effect waves-light" style="margin-bottom: 10px">
                                            <i class="ri-edit-2-line align-middle mr-2"></i> СОХРАНИТЬ ИЗМЕНЕНИЯ
                                        </button>
                                    </div>
                                </div>

                            </form>

                        </div>


                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection