
@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Редактирование представителя</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item">Представители</li>
                                <li class="breadcrumb-item active">Редактирование представителя</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mb-4">Редактирование представителя</h4>

                            <form action="{{ route('admin.socials-post-edit') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <img class="rounded mr-2" alt="200x200" src="@if($social->headImg){{asset($social->headImg->img->url)}} @else {{asset("/assets/images/small/img-4.jpg")}} @endif" data-holder-rendered="true" width="250">
                                </div>

                                <div class="form-group row">
                                    <label for="example-fio-input" class="col-md-2 col-form-label">Фото</label>
                                    <div class="col-md-10">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="img" id="customFile">
                                            <label class="custom-file-label" for="customFile">Добавление фото</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-name-input" class="col-md-2 col-form-label">Имя представителя</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="leader_name" value="{{$social->leader_name}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-name-input" class="col-md-2 col-form-label">Должность представителя</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="leader_post" value="{{$social->leader_post}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-name-input" class="col-md-2 col-form-label">Цель</label>
                                    <div class="col-md-10">
                                        <textarea name="purpose" class="form-control">{{$social->purpose}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-post-input"  class="col-md-2 col-form-label">Как вступить</label>
                                    <div class="col-md-10">
                                        <textarea name="enter" class="form-control">{{$social->enter}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-description-input" class="col-md-2 col-form-label">Постскриптум</label>
                                    <div class="col-md-10">
                                        <textarea name="postscript" class="form-control">{{$social->postscript}}</textarea>
                                    </div>
                                </div>
                                <input type="hidden" name="id" value="{{$social->id}}">
                                <input type="hidden" name="type_id" value="{{$social->type_id}}">



                                <div class="form-group row">
                                    <label for="example-tel-input" class="col-md-2 col-form-label"></label>
                                    <div class="col-md-10">
                                        <button type="submit" href="#" class="btn btn-primary waves-effect waves-light" style="margin-bottom: 10px">
                                            <i class="ri-edit-2-line align-middle mr-2"></i> СОХРАНИТЬ ИЗМЕНЕНИЯ
                                        </button>
                                    </div>
                                </div>

                            </form>

                        </div>


                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection