
@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">{{$course_sector->title}}</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item active">{{$course_sector->title}}</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <!-- end row -->

            <!-- end row -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mb-4">Список записей '{{$course_sector->title}}'</h4>

                            <a href="{{route('admin.sector_items-add',['id'=>$course_sector->id])}}" class="btn btn-success waves-effect waves-light" style="margin-bottom: 10px">
                                <i class="ri-bar-chart-horizontal-line align-middle mr-2"></i> Добавить запись
                            </a>

                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Значение</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($course_sector->sectorItems as $key => $sector_item)
                                <tr>
                                    <td>{{$sector_item->title}}</td>
                                    @if($sector_item->file)<td> <a href="{{asset($sector_item->file)}} " download>Скачать</a> </td>@endif
                                    @if($sector_item->url)<td> <a href="{{$sector_item->url}} " target="_blank" >Перейти</a> </td>@endif

                                    <td>
                                        <div class="icon-demo-content">
                                            <a class="" href="{{route('admin.sector_items-edit', ['id' => $sector_item->id])}}">
                                                <i class="ri-edit-2-line"></i>
                                            </a>
                                            <a class="" href="{{route('admin.sector_items-delete', ['id' => $sector_item->id])}}">
                                                <i class="ri-delete-bin-2-line"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection