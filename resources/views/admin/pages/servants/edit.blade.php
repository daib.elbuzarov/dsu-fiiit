
@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Редактирование работника</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item">Работники</li>
                                <li class="breadcrumb-item active">Редактирование работника</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mb-4">Редактирование работника</h4>

                            <form action="{{ route('admin.servants-post-edit') }}" method="POST" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group row">
                                    <label for="example-fio-input" class="col-md-2 col-form-label">Фото</label>
                                    <div class="col-md-10">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="img" id="customFile">
                                            <label class="custom-file-label" for="customFile">Добавление фото</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-name-input" class="col-md-2 col-form-label">Название</label>
                                    <div class="col-md-10">
                                        <input class="form-control" value="{{$servant->title}}" name="title" type="text" id="example-name-input">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-post-input"  class="col-md-2 col-form-label">Должность</label>
                                    <div class="col-md-10">
                                        <input class="form-control" value="{{$servant->work_type}}" name="work_type" type="text" id="example-post-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-description-input" class="col-md-2 col-form-label">Описание</label>
                                    <div class="col-md-10">
                                        <textarea name="description" class="form-control">{{$servant->description}}</textarea>
                                    </div>
                                </div>
                                <input type="hidden" name="type_id" value="{{$servant->type_id}}">
                                <input type="hidden" name="id" value="{{$servant->id}}">



                                <div class="form-group row">
                                    <label for="example-tel-input" class="col-md-2 col-form-label"></label>
                                    <div class="col-md-10">
                                        <button type="submit" href="#" class="btn btn-primary waves-effect waves-light" style="margin-bottom: 10px">
                                            <i class="ri-edit-2-line align-middle mr-2"></i> СОХРАНИТЬ ИЗМЕНЕНИЯ
                                        </button>
                                    </div>
                                </div>

                            </form>

                        </div>


                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection