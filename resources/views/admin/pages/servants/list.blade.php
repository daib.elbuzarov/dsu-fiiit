
@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">{{mb_strtoupper($type->name)}}</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item active">{{mb_strtoupper($type->name)}}</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <!-- end row -->

            <!-- end row -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mb-4">Список работников</h4>

                            <a href="{{route('admin.servants-add',['id'=>$type->id])}}" class="btn btn-success waves-effect waves-light" style="margin-bottom: 10px">
                                <i class="ri-team-fill align-middle mr-2"></i> Добавить работника
                            </a>

                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Должность</th>
                                <th>Описание</th>
                                <th>Фото</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($servants as $key => $servant)
                                <tr>
                                    <td>{{$servant->title}}</td>
                                    <td>{{$servant->work_type}}</td>
                                    <td>{{$servant->description}}</td>
                                    <td><img @if($servant->headImg) src="{{asset($servant->headImg->img->url)}}" @endif alt="" class="rounded avatar-md"></td>
                                    <td>
                                        <div class="icon-demo-content">
                                            <a class="" href="{{route('admin.servants-edit', ['id' => $servant->id])}}">
                                                <i class="ri-edit-2-line"></i>
                                            </a>
                                            <a class="" href="{{route('admin.servants-delete', ['id' => $servant->id])}}">
                                                <i class="ri-delete-bin-2-line"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection