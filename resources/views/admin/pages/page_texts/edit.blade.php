
@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Редактирование текста</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item">Текста</li>
                                <li class="breadcrumb-item active">Редактирование текста</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mb-4">Редактирование текста</h4>

                            <form action="{{ route('admin.page_texts-post-edit') }}" method="POST" enctype="multipart/form-data">
                                @csrf


{{--                                <div class="form-group row">--}}
{{--                                    <label for="example-name-input" class="col-md-2 col-form-label">Название переменой</label>--}}
{{--                                    <div class="col-md-10">--}}
{{--                                        <input class="form-control" name="name" type="text" value="{{$page_text->name}}" id="example-name-input">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group row">--}}
{{--                                    <label for="example-url-input" class="col-md-2 col-form-label">URL</label>--}}
{{--                                    <div class="col-md-10">--}}
{{--                                        <input class="form-control" name="url" value="{{$page_text->url}}" type="text" id="example-url-input">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="form-group row">
                                    <label for="example-title-input" class="col-md-2 col-form-label">Название</label>
                                    <div class="col-md-10">
                                        <input class="form-control" name="title" value="{{$page_text->title}}" type="text" id="example-title-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-title-input" class="col-md-2 col-form-label">Ссылка</label>
                                    <div class="col-md-10">
                                        <input class="form-control" name="link" value="{{$page_text->link}}" type="text" id="example-title-input">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-value-input" class="col-md-2 col-form-label">Значение</label>
                                    <div class="col-md-10">
                                        <textarea id="basicpill-value-input" name="value" class="form-control" rows="2" style="height: 170px;">{{$page_text->value}}</textarea>
                                    </div>
                                </div>
                                <input type="hidden" name="id" value="{{$page_text->id}}">



                                <div class="form-group row">
                                    <label for="example-tel-input" class="col-md-2 col-form-label"></label>
                                    <div class="col-md-10">
                                        <button type="submit" href="{{route('admin.page_texts-add')}}" class="btn btn-primary waves-effect waves-light" style="margin-bottom: 10px">
                                            <i class="ri-edit-2-line align-middle mr-2"></i> СОХРАНИТЬ ИЗМЕНЕНИЯ
                                        </button>
                                    </div>
                                </div>

                            </form>

                        </div>


                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection