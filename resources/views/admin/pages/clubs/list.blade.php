
@extends('admin.layouts.admin_index')

@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">Клубы</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item">Admin</li>
                                <li class="breadcrumb-item active">Клубы</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <!-- end row -->

            <!-- end row -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title mb-4">Список клубов</h4>

                            <a href="{{route('admin.clubs-add')}}" class="btn btn-success waves-effect waves-light" style="margin-bottom: 10px">
                                <i class="ri-team-fill align-middle mr-2"></i> Добавить клуб
                            </a>

                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>Тип</th>
                                <th>Название</th>
                                <th>Описание</th>
                                <th>Основатель (держатель)</th>
                                <th>Лого</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($clubs as $key => $club)
                                <tr>
                                    <td>{{$club->type->name}}</td>
                                    <td>{{$club->title}}</td>
                                    <td>{!! $club->description !!}</td>
                                    <td>{{$club->author}}</td>
                                    @isset($club->headImg)
                                    <td><img src="{{asset($club->headImg->img->url)}}" alt="" class="rounded avatar-md"></td>
                                    @endisset
                                    <td>
                                        <div class="icon-demo-content">
                                            <a class="" href="{{route('admin.clubs-edit', ['id' => $club->id])}}">
                                                <i class="ri-edit-2-line"></i>
                                            </a>
                                            <a class="" href="{{route('admin.clubs-delete', ['id' => $club->id])}}">
                                                <i class="ri-delete-bin-2-line"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


@endsection
