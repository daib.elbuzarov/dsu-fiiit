<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="{{route('admin.home')}}" class="waves-effect">
                        <i class="ri-dashboard-line"></i>
                        <span>Главная</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.page_texts-list')}}" class="waves-effect">
                        <i class="ri-file-list-2-line"></i>
                        <span>Страничные текста</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.banners-list')}}" class="waves-effect">
                        <i class="ri-file-list-2-line"></i>
                        <span>Баннеры</span>
                    </a>
                </li>
{{--                <li>--}}
{{--                    <a href="{{route('admin.navigators-list')}}" class="waves-effect">--}}
{{--                        <i class="ri-navigation-line"></i>--}}
{{--                        <span>Навигаторы</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
                <li>
                    <a href="{{route('admin.clubs-list')}}" class="waves-effect">
                        <i class="ri-team-fill"></i>
                        <span>Клубы</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.contests-list')}}" class="waves-effect">
                        <i class="ri-star-s-line"></i>
                        <span>Конкурсы/Гранты</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.departments-list')}}" class="waves-effect">
                        <i class="ri-bank-fill"></i>
                        <span>Кафедры</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.president-edit')}}" class="waves-effect">
                        <i class="ri-user-2-fill"></i>
                        <span>Декан</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.deputy_directors-list')}}" class="waves-effect">
                        <i class="ri-user-2-fill"></i>
                        <span>Заместители декана</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.courses-list')}}" class="waves-effect">
                        <i class="ri-git-repository-line"></i>
                        <span>Направления</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.documents-list')}}" class="waves-effect">
                        <i class="ri-file-list-3-line"></i>
                        <span>Документы</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.sections-list')}}" class="waves-effect">
                        <i class="ri-device-fill"></i>
                        <span>Разделы документов</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.senates-list')}}" class="waves-effect">
                        <i class="ri-file-paper-2-fill"></i>
                        <span>Совет факультета</span>
                    </a>

                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ri-clipboard-line"></i>
                        <span>Расписание</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li>
                            <a href="{{route('admin.lessons-list')}}" class="waves-effect">
                                <i class="ri-clipboard-line"></i>
                                <span>Расписание занятий</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.exams-list')}}" class="waves-effect">
                                <i class="ri-survey-line"></i>
                                <span>Расписание сессий</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.schedule-weeks')}}" class="waves-effect">
                                <i class="ri-survey-line"></i>
                                <span>Недели</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ri-user-line"></i>
                        <span>Состав</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        @foreach(\App\Models\Types::getTypes(\App\Models\Servant::table) as $servant)
                            <li>
                                <a href="{{route('admin.servants-list',['id'=>$servant->id])}}" class="waves-effect">
                                    <i class="ri-user-line"></i>
                                    <span>{{mb_strtoupper($servant->name)}}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ri-article-line"></i>
                        <span>Посты</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        @foreach(\App\Models\Types::getTypes(\App\Models\Post::table) as $post)
                            <li>
                                <a href="{{route('admin.posts-list',['id'=>$post->id])}}" class="waves-effect">
                                    <i class="ri-article-line"></i>
                                    <span>{{mb_strtoupper($post->name)}}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ri-user-line"></i>
                        <span>Представители</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        @foreach(\App\Models\Types::getTypes(\App\Models\Social::table) as $social)
                            <li>
                                <a href="{{route('admin.socials-edit',['id'=>$social->id])}}" class="waves-effect">
                                    <i class="ri-user-line"></i>
                                    <span>{{mb_strtoupper($social->name)}}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>



            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->


<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

