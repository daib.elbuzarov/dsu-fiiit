@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <script>document.write(new Date().getFullYear())</script> © DSU.
            </div>
            <div class="col-sm-6">
                <div class="text-sm-right d-none d-sm-block">
                    Crafted with <i class="dripicons-gear text-danger"></i> Daib Elbuzarov
                </div>
            </div>
        </div>
    </div>
</footer>

</div>
<!-- end main content-->
</div>
<!-- END layout-wrapper -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@if(Route::currentRouteName() === 'admin.posts-edit' || Route::currentRouteName() === 'admin.banners-edit')

    <!-- JAVASCRIPT -->
    <script src="{{asset('assets/libs/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/libs/metismenu/metisMenu.min.js')}}"></script>
    <script src="{{asset('assets/libs/simplebar/simplebar.min.js')}}"></script>
    <script src="{{asset('assets/libs/node-waves/waves.min.js')}}"></script>

    <!-- Magnific Popup-->
    <script src="{{asset('assets/libs/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
    <!-- lightbox init js-->
    <script src="{{asset("assets/js/pages/lightbox.init.js")}}"></script>
@endif
<!-- JAVASCRIPT -->
<script src="{{ asset('assets/libs/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ asset('assets/libs/metismenu/metisMenu.min.js')}}"></script>
<script src="{{ asset('assets/libs/simplebar/simplebar.min.js')}}"></script>
<script src="{{ asset('assets/libs/node-waves/waves.min.js')}}"></script>


<!-- apexcharts -->
<script src="{{ asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

<!-- jquery.vectormap map -->
<script src="{{ asset('assets/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{ asset('assets/libs/admin-resources/jquery.vectormap/maps/jquery-jvectormap-us-merc-en.js')}}"></script>


<!-- bs custom file input plugin -->
<script src="{{ asset('assets/libs/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>

<script src="{{ asset('assets/js/pages/form-element.init.js')}}"></script>

<script src="{{ asset('assets/js/pages/dashboard.init.js')}}"></script>

<!-- Summernote js -->
<script src="{{ asset('assets/libs/summernote/summernote-bs4.min.js')}}"></script>

<!--tinymce js-->
<script src="{{ asset('assets/libs/tinymce/tinymce.min.js')}}"></script>

<!-- init js -->
<script src="{{ asset('assets/js/pages/form-editor.init.js')}}"></script>





<script src="{{ asset('assets/js/app.js')}}"></script>
</body>

</html>
