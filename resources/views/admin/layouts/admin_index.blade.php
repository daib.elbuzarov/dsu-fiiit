
    @include('admin.layouts.admin_header')

    @include('admin.layouts.admin_sidebar')

    @yield('content')

    @include('admin.layouts.admin_footer')
