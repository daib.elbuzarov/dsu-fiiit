@extends("users.layouts.index")

@section("content")
    {{--    Руководство факультета--}}
    <div class="d_container">
        <h1 class="d_h1">Структура факультета</h1>
    </div>


        <div class="faculty_worker_wrap">
            <div class="d_container">
            <div class="faculty_worker">

                <div class="worker_img_wrap">
                    <img class="worker_img" src="@if(isset($president->headImg)){{asset($president->headImg->img->url)}}
                    @else {{asset('/icons/structure/dean.jpg')}}
                    @endif" alt="">
                    <img class="worker_icon" src="{{asset('/icons/structure/dean_icon.svg')}}" alt="">
                </div>

                <div class="worker_info">
                    <div class="worker_position">
                        декан
                    </div>
                    <div class="worker_name">
                        {!! $president->surname !!} <br>
                        {!! $president->firstname !!} {!! $president->middle_name !!}
                    </div>
                    <div class="worker_degree">
                        {!! $president->description !!}
                    </div>
                </div>
              <span>{{session('message')}}</span>
            </div>
        </div>
    </div>


    <div class="d_container">
        <div class="dean_deputies">
            <div class="dean_deputies_title">
                Заместители декана
            </div>

            <ul class="dean_deputes_list">
                @foreach($directors as $key => $item)
                <li>
{{--                        <img class="dean_deputies_img" src="" alt="">--}}
                    @isset($item->headImg,$item->headImg->img)
                        <img src="{{asset($item->headImg->img->url)}}" alt="{{$item->title}}" class="dean_deputes-img">
                    @endisset
                    <p class="dean_deputies_name">{{$item->title}}</p>
                    <p  class="dean_deputes_info">{{$item->description}}</p>
                </li>
                @endforeach
{{--                <li>--}}
{{--                        <img class="dean_deputies_img" src="" alt="">--}}

{{--                    <p class="dean_deputies_name">Билалова Елена Мустафаевна</p>--}}
{{--                    <p  class="dean_deputes_info">Заместитель декана по учебно-воспитательной работе</p>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                        <img class="dean_deputies_img" src="" alt="">--}}

{{--                    <p class="dean_deputies_name">Гаджиев Насрулла Курбанмагомедович</p>--}}
{{--                    <p  class="dean_deputes_info">Заместитель декана по научной работе</p>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                        <img class="dean_deputies_img" src="" alt="">--}}

{{--                    <p class="dean_deputies_name">Чапаев Набигуллах Мухтарович</p>--}}
{{--                    <p  class="dean_deputes_info">Заместитель декана по магистратуре</p>--}}
{{--                </li>--}}
            </ul>
        </div>

        {{--Кафедры--}}
        <div class="structure_cathedra">
            <h2 class="structure_h2">Кафедры</h2>
            <ul class="faculty_departments_list">
                @foreach($departments as $key => $item)
                <li><a target="_blank" href="{{$item->link}}"><p>{{$item->title}}</p></a></li>
                @endforeach
{{--                <li><a href="#"><p>Кафедра прикладной информатики</p></a></li>--}}
{{--                <li><a href="#"><p>Кафедра информационных технологий и безопасности компьютерных систем</p></a></li>--}}
            </ul>
        </div>


        {{--Совет факультета--}}
        <div class="structure_council">
            <h2 class="structure_h2">Совет факультета</h2>
            <ul class="structure_docs_list">
                @foreach($faculty_senate as $key => $item)
                <li><a href="{{$item->link ?? asset($item->file)}}" target="_blank">{{$item->text}}</a></li>
                @endforeach
{{--                <li><a href="#">Состав Совета факультета Информатики и информационных технологий ФГБОУ ВО «Дагестанский государственный университет»</a></li>--}}
            </ul>
        </div>

        {{--Учебно-методический совет--}}
        <div class="structure_council">
            <h2 class="structure_h2">Учебно-методический совет</h2>
            <ul class="structure_docs_list">
                @foreach($study_senate as $key => $item)
                    <li><a href="{{$item->link ?? asset($item->file)}}" target="_blank">{{$item->text}}</a></li>
                @endforeach
{{--                <li><a href="#">План работы методического совета Факультета ИиИТ ФГБОУ ВО «Дагестанский государственный университет» на 2019-2020 учебный год</a></li>--}}
{{--                <li><a href="#">Состав учебно-методического Совета факультета Информатики и информационных технологий ФГБОУ ВО «Дагестанский государственный университет»</a></li>--}}
            </ul>
        </div>

    </div>

@endsection
