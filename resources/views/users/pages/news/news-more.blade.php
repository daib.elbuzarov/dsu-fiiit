@extends("users.layouts.index")
@section("content")

<link href="{{asset('/css/news-more.css')}}" rel="stylesheet">

<section class="section-one">
    <div class="d_container">
        <h1 class="section-one__header">Новости и объявления</h1>
    </div>
</section>

<section class="section-two">
    <div class="d_container">
        <div class="section-two__tabs">
            <a href="{{route('news')}}" class="section-two__tab section-two__tab_active">Новости</a>
            <a href="{{route('news')}}" class="section-two__tab">Объявления</a>
        </div>
        <div class="section-two__divider"></div>
    </div>
</section>
<section class="section-three">
    <div class="d_container">
        <div class="section-three__subheader"><span class="section-three__date">{{$news->created_at->format("d.m.Y")}}</span>{{$news->tag}}</div>
        <div class="section-three__header">{{$news->title}}</div>
        <div class="section-three__text">
            {!! $news->description !!}
        </div>
        <div class="section-three__images">
            <div class="section-three__image">
                <img @if($news->headImg) src="{{asset($news->headImg->img->url)}}" @endif alt="image">
            </div>
            @foreach($news->sliderImg as $key => $join)
            <div class="section-three__image">
                <img @if($join) src="{{asset($join->img->url)}}" @endif alt="image">
            </div>
                
            @endforeach
        </div>
    </div>
</section>
{{--<section class="section-three">--}}
{{--    <div class="d_container">--}}
{{--        <div class="section-three__subheader"><span class="section-three__date">20.01.2021</span>Профориентационная работа</div>--}}
{{--        <div class="section-three__header">В школах районов прошли встречи со школьниками</div>--}}
{{--        <div class="section-three__text">--}}
{{--            <p>21 ноября состоялась встреча учащихся 42 школы г. Махачкалы с заведующим кафедрой прикладной информатики, к.э.н, доцентом ДГУ Камиловым Камилем Башировичем и заместителем декана факультета информатики и информационных технологий Ахмедовой Написат Мурадовной.</p>--}}
{{--    ⠀--}}
{{--            <p>Преподаватели посетили пятый профильный класс, послушали идеи совсем юных программистов, поделились с ними своим опытом, рассказали про факультет информатики и информационных технологий, как он поможет решать их задачи, и реализовывать их идеи, которые пока им кажутся только мечтами. Рассказали про Яндекс.Лицей на базе ДГУ.</p>--}}
{{--    ⠀--}}
{{--            <p>Конечно, не оставили в стороне и выпускные классы. Рассказали выпускникам про все направления факультета, показали им реальные проекты наших выпускников и студентов, ответили на вопросы школьников и поделились советами как легко подготовиться к ЕГЭ по информатике.</p>--}}
{{--    ⠀--}}
{{--            <p>Насколько продуктивной была эта встреча, покажет время.</p>--}}
{{--        </div>--}}
{{--        <div class="section-three__images">--}}
{{--            <img src="{{asset('/icons/news/bg.png')}}" alt="image" class="section-three__image">--}}
{{--            <img src="{{asset('/icons/news/bg.png')}}" alt="image" class="section-three__image">--}}
{{--            <img src="{{asset('/icons/news/bg.png')}}" alt="image" class="section-three__image">--}}
{{--            <img src="{{asset('/icons/news/bg.png')}}" alt="image" class="section-three__image">--}}
{{--            <img src="{{asset('/icons/news/bg.png')}}" alt="image" class="section-three__image">--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}

@endsection