@extends("users.layouts.index")

@section("content")

    <link href="{{asset('/css/news.css')}}" rel="stylesheet">
    <script defer src="{{asset('js/news.js')}}"></script>


    <section class="section-one">
        <div class="d_container">
            <h1 class="section-one__header">Новости и объявления</h1>
        </div>
    </section>

    <section class="section-two">
        <div class="d_container">
            <div class="section-two__tabs">
                <div class="section-two__tab section-two__tab_active">Новости</div>
                <div class="section-two__tab">Объявления</div>
            </div>
            <div class="section-two__divider"></div>
        </div>
    </section>

    <section class="section-three section-three_show">
        <div class="d_container">
            <div class="section-three__wrapper">
                    @foreach($news as $key => $item)
                        <div class="news-block" >
                            <a href="{{route('news.news-more',['id'=>$item->id])}}">
                                <div class="news-block__inner">
                                    <div class="news-block__subheader">{{$item->tag}}</div>
                                    <div class="news-block__header">{{$item->title}}</div>
                                    <div class="news-block__date">{{$item->created_at->format("d.m.Y")}}</div>
                                </div>
                                <img src="@if(isset($item->headImg)){{asset($item->headImg->img->url)}} @endif" alt="" class="news-block__image">
                            </a>
                            <div class="news-block__more"><a href="{{route('news.news-more',['id'=>$item->id])}}">Подробнее</a></div>
                        </div>
                    @endforeach
{{--                <div class="news-block">--}}
{{--                    <div class="news-block__subheader">Профориентационная работа</div>--}}
{{--                    <div class="news-block__header">В школах районов прошли встречи со школьниками</div>--}}
{{--                    <div class="news-block__date">20.01.2021</div>--}}
{{--                    <div class="news-block__more"><a href="{{route('news.news-more')}}">Подробнее</a></div>--}}
{{--                </div>--}}
{{--                <div class="news-block">--}}
{{--                    <div class="news-block__subheader">Профориентационная работа</div>--}}
{{--                    <div class="news-block__header">В школах районов прошли встречи со школьниками</div>--}}
{{--                    <div class="news-block__date">20.01.2021</div>--}}
{{--                    <div class="news-block__more"><a href="{{route('news.news-more')}}">Подробнее</a></div>--}}
{{--                </div>--}}
{{--                <div class="news-block">--}}
{{--                    <div class="news-block__subheader">Профориентационная работа</div>--}}
{{--                    <div class="news-block__header">В школах районов прошли встречи со школьниками</div>--}}
{{--                    <div class="news-block__date">20.01.2021</div>--}}
{{--                    <div class="news-block__more"><a href="{{route('news.news-more')}}">Подробнее</a></div>--}}
{{--                </div>--}}
{{--                <div class="news-block">--}}
{{--                    <div class="news-block__subheader">Профориентационная работа</div>--}}
{{--                    <div class="news-block__header">В школах районов прошли встречи со школьниками</div>--}}
{{--                    <div class="news-block__date">20.01.2021</div>--}}
{{--                    <div class="news-block__more"><a href="{{route('news.news-more')}}">Подробнее</a></div>--}}
{{--                </div>--}}
{{--                <div class="news-block">--}}
{{--                    <div class="news-block__subheader">Профориентационная работа</div>--}}
{{--                    <div class="news-block__header">В школах районов прошли встречи со школьниками</div>--}}
{{--                    <div class="news-block__date">20.01.2021</div>--}}
{{--                    <div class="news-block__more"><a href="{{route('news.news-more')}}">Подробнее</a></div>--}}
{{--                </div>--}}
            </div>
        </div>
    </section>

    <section class="section-three">
        <div class="d_container">
            <div class="section-three__advertisements">
                @foreach($ads as $key => $item)
                <div class="advertisement">
                    <div class="advertisement__date">{{$item->created_at->format("d.m.y")}}</div>
                    <div class="advertisement__text">
                        <div class="advertisement__header">{{$item->title}}</div>

                        <div class="advertisement__description">{!! \Illuminate\Support\Str::limit($item->description,50,"...") !!}</div>

                        <a href="{{route('news.advertisement-more',['id'=>$item->id])}}" class="advertisement__more">Читать далее</a>
                    </div>
                </div>
                @endforeach
{{--                <div class="advertisement">--}}
{{--                    <div class="advertisement__date">10.02.21</div>--}}
{{--                    <div class="advertisement__text">--}}
{{--                        <div class="advertisement__header">Студенты проведут встречи со школьниками в школах районов</div>--}}

{{--                        <div class="advertisement__description">14 декабря наши студенты провели очередную встречу со школьниками, где поговорили об IT-сфере и отметили наиболее перспективные направления отрасли...</div>--}}

{{--                        <a href="{{route('news.advertisement-more')}}" class="advertisement__more">Читать далее</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="advertisement">--}}
{{--                    <div class="advertisement__date">10.02.21</div>--}}
{{--                    <div class="advertisement__text">--}}
{{--                        <div class="advertisement__header">Студенты проведут встречи со школьниками в школах районов</div>--}}

{{--                        <div class="advertisement__description">14 декабря наши студенты провели очередную встречу со школьниками, где поговорили об IT-сфере и отметили наиболее перспективные направления отрасли...</div>--}}

{{--                        <a href="{{route('news.advertisement-more')}}" class="advertisement__more">Читать далее</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="advertisement">--}}
{{--                    <div class="advertisement__date">10.02.21</div>--}}
{{--                    <div class="advertisement__text">--}}
{{--                        <div class="advertisement__header">Студенты проведут встречи со школьниками в школах районов</div>--}}

{{--                        <div class="advertisement__description">14 декабря наши студенты провели очередную встречу со школьниками, где поговорили об IT-сфере и отметили наиболее перспективные направления отрасли...</div>--}}

{{--                        <a href="{{route('news.advertisement-more')}}" class="advertisement__more">Читать далее</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="advertisement">--}}
{{--                    <div class="advertisement__date">10.02.21</div>--}}
{{--                    <div class="advertisement__text">--}}
{{--                        <div class="advertisement__header">Студенты проведут встречи со школьниками в школах районов</div>--}}

{{--                        <div class="advertisement__description">14 декабря наши студенты провели очередную встречу со школьниками, где поговорили об IT-сфере и отметили наиболее перспективные направления отрасли...</div>--}}

{{--                        <a href="{{route('news.advertisement-more')}}" class="advertisement__more">Читать далее</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="advertisement">--}}
{{--                    <div class="advertisement__date">10.02.21</div>--}}
{{--                    <div class="advertisement__text">--}}
{{--                        <div class="advertisement__header">Студенты проведут встречи со школьниками в школах районов</div>--}}

{{--                        <div class="advertisement__description">14 декабря наши студенты провели очередную встречу со школьниками, где поговорили об IT-сфере и отметили наиболее перспективные направления отрасли...</div>--}}

{{--                        <a href="{{route('news.advertisement-more')}}" class="advertisement__more">Читать далее</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="advertisement">--}}
{{--                    <div class="advertisement__date">10.02.21</div>--}}
{{--                    <div class="advertisement__text">--}}
{{--                        <div class="advertisement__header">Студенты проведут встречи со школьниками в школах районов</div>--}}

{{--                        <div class="advertisement__description">14 декабря наши студенты провели очередную встречу со школьниками, где поговорили об IT-сфере и отметили наиболее перспективные направления отрасли...</div>--}}

{{--                        <a href="{{route('news.advertisement-more')}}" class="advertisement__more">Читать далее</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="advertisement">--}}
{{--                    <div class="advertisement__date">10.02.21</div>--}}
{{--                    <div class="advertisement__text">--}}
{{--                        <div class="advertisement__header">Студенты проведут встречи со школьниками в школах районов</div>--}}

{{--                        <div class="advertisement__description">14 декабря наши студенты провели очередную встречу со школьниками, где поговорили об IT-сфере и отметили наиболее перспективные направления отрасли...</div>--}}

{{--                        <a href="{{route('news.advertisement-more')}}" class="advertisement__more">Читать далее</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
    </section>
    
    <!-- Pagination for news -->
    <section class="section-four section-four_show">
        <div class="d_container">
            {{$news->links('vendor.pagination.default')}}

            {{--            <div class="pagination">--}}
{{--                <a href="#"> &laquo; </a>--}}
{{--                <a href="#" class="pagination_active">1</a>--}}
{{--                <a href="#">2</a>--}}
{{--                <a href="#">3</a>--}}
{{--                <a href="#">4</a>--}}
{{--                <a href="#">5</a>--}}
{{--                <span>...</span>--}}
{{--                <a href="#">32</a>--}}
{{--                <a href="#"> &raquo; </a>--}}
{{--            </div>--}}
        </div>
    </section>

    <!-- Pagination for advertisement -->
    <section class="section-four">
        <div class="d_container">
            {{$ads->links('vendor.pagination.default')}}

            {{--            <div class="pagination">--}}
{{--                <a href="#"> &laquo; </a>--}}
{{--                <a href="#" class="pagination_active">1</a>--}}
{{--                <a href="#">2</a>--}}
{{--                <a href="#">3</a>--}}
{{--                <a href="#">4</a>--}}
{{--                <a href="#">5</a>--}}
{{--                <span>...</span>--}}
{{--                <a href="#">27</a>--}}
{{--                <a href="#"> &raquo; </a>--}}
{{--            </div>--}}
        </div>
    </section>



@endsection