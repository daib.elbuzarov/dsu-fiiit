@extends("users.layouts.index")

@section("content")

<link href="{{asset('/css/social-sd.css')}}" rel="stylesheet">

<!-- TODO: Исправить margin-top у .section-one__block:nth-child(1) -->
<!-- TODO: Заполнить ссылки -->

    <section class="section-one">
    	<div class="d_container">
    		<h1 class="section-one__header">Социальная работа</h1>
            <h2>Студенческий деканат</h2>
    		<div class="section-one__main">
    			<div class="section-one__information">
    				<div class="section-one__block">
                        {!! $sd->purpose !!}
{{--                    Цель СНО - повышение качества подготовки студентов в соответствии с современными требованиями к высшему образованию, эффективному развитию и использованию творческого потенциала студентов в форме научно-исследовательской работы.--}}
    				</div>
    				<div class="section-one__block">
                        {!! $sd->enter !!}
{{--    					<span>Хочу в Студдеканат!</span>--}}

{{--    					Чтобы вступить в студдеканат, достаточно заполнить форму <a href="#">здесь</a>.--}}
    				</div>
    				<div class="section-one__block">
                        {!! $sd->postscript !!}
{{--    					<span>Следите за новостями!</span>--}}

{{--    					Подписывайтесь на <a href="#">нас в Instagram</a> и будьте в курсе всех новостей и событий.--}}
    				</div>
    			</div>
    			<div class="section-one__image">
    				<div class="section-one__img">
{{--                        <img src="{{asset('/icons/social/section-one-img.svg')}}" alt="img" >--}}
                        <img src="@if($sd->headImg){{asset($sd->headImg->img->url)}}" @endif alt="img" >
                    </div>
    				<div class="section-one__img-info">
						<h3 class="section-one__img-subheader">{{$sd->leader_post}}</h3>
                        <h2 class="section-one__img-header">{{$sd->leader_name}}</h2>
{{--                        <h2 class="section-one__img-header">Саид Саидов</h2>--}}
{{--                        <h3 class="section-one__img-subheader">Студенческий декан</h3>--}}
                    </div>
    			</div>
    		</div>
    	</div>
    </section>

    <section class="section-two">
    	<div class="d_container">
    		<h2 class="section-two__header">Структура Студенческого деканата</h2>
    		<div class="section-two__peoples">
                @foreach($servants as $key => $servant)
                    <div class="people">
                        <div class="people__img"><img  src="@if($servant->headImg){{asset($servant->headImg->img->url)}}@endif" alt="img"></div>
                        <div class="people__header">{{$servant->title}}</div>
                        <div class="people__subheader">{{$servant->work_type}}</div>
                        <div class="people__description">{{$servant->description}}</div>
                    </div>
                @endforeach
{{--    			<div class="people">--}}
{{--	    			<div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--	    			<div class="people__header">Мурад Абдурахманов</div>--}}
{{--	    			<div class="people__subheader">Социальный сектор</div>--}}
{{--	    			<div class="people__description">Защита учебных и социально-экономических прав, бытовых условий и других интересов студентов. Обеспечение членов Студдеканата правовой и социальной защитой.</div>--}}
{{--    			</div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Аида Генжеева</div>--}}
{{--                    <div class="people__subheader">КУЛЬМАССОВЫЙ СЕКТОР</div>--}}
{{--                    <div class="people__description">Раскрытие наукотворческого потенциала студентов, привлечение студентов к научно-исследовательской работе, участию в научных конференциях, смотров и конкурсах научных работ.</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Руслан Чупанов</div>--}}
{{--                    <div class="people__subheader">Пресс-служба</div>--}}
{{--                    <div class="people__description">Раскрытие наукотворческого потенциала студентов, привлечение студентов к научно-исследовательской работе, участию в научных конференциях, смотров и конкурсах научных работ.</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Амалия Рамазанова</div>--}}
{{--                    <div class="people__subheader">НАУЧНый сектор</div>--}}
{{--                    <div class="people__description">Раскрытие наукотворческого потенциала студентов, привлечение студентов к научно-исследовательской работе, участию в научных конференциях, смотров и конкурсах научных работ.</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Магомед Абдуллаев</div>--}}
{{--                    <div class="people__subheader">ИНТЕЛЛЕКТУАЛЬНЫЙ сектор</div>--}}
{{--                    <div class="people__description">Организация различных интеллектуальных--}}
{{--                    игр, мероприятий и проведение викторин, среди которых можно выделить ЧГК, Брейн-Ринг, Своя Игра.</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Джамал Бадрутдинов</div>--}}
{{--                    <div class="people__subheader">учебный сектор</div>--}}
{{--                    <div class="people__description">Организация различных интеллектуальных--}}
{{--                    игр, мероприятий и проведение викторин, среди которых можно выделить ЧГК, Брейн-Ринг, Своя Игра.</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Амалия Рамазанова</div>--}}
{{--                    <div class="people__subheader">Волонтерский сектор</div>--}}
{{--                    <div class="people__description">Раскрытие наукотворческого потенциала студентов, привлечение студентов к научно-исследовательской работе, участию в научных конференциях, смотров и конкурсах научных работ.</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Магомед Абдуллаев</div>--}}
{{--                    <div class="people__subheader">военно-патриотический сектор</div>--}}
{{--                    <div class="people__description">Организация различных интеллектуальных--}}
{{--                    игр, мероприятий и проведение викторин, среди которых можно выделить ЧГК, Брейн-Ринг, Своя Игра.</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Джамал Бадрутдинов</div>--}}
{{--                    <div class="people__subheader">Спортивный сектор</div>--}}
{{--                    <div class="people__description">Организация различных интеллектуальных--}}
{{--                    игр, мероприятий и проведение викторин, среди которых можно выделить ЧГК, Брейн-Ринг, Своя Игра.</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Магомед Абдуллаев</div>--}}
{{--                    <div class="people__subheader">Институт кураторов</div>--}}
{{--                    <div class="people__description">Организация различных интеллектуальных--}}
{{--                    игр, мероприятий и проведение викторин, среди которых можно выделить ЧГК, Брейн-Ринг, Своя Игра.</div>--}}
{{--                </div>--}}
    		</div>
    	</div>
    </section>



@endsection