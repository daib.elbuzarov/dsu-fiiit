@extends("users.layouts.index")
@section("content")

<link href="{{asset('/css/social-kdm.css')}}" rel="stylesheet">

    <section class="section-one">
        <div class="d_container">
            <h1 class="section-one__header">Социальная работа</h1>
            <h2 class="section-one__subheader">Комитет по делам молодежи</h2>
            <div class="section-one__main">
                <div class="section-one__information">
                    <div class="section-one__block">
                        {!! $kdm->purpose !!}
                    </div>
                    <div class="section-one__block">
                        {!! $kdm->enter !!}
{{--                        <span>Как вступить в КДМ IT-факультета?</span>--}}

{{--                        Чтобы вступить в КДМ, достаточно заполнить форму <a href="#">здесь</a>.--}}
                    </div>
                    <div class="section-one__block">
                        {!! $kdm->postscript !!}
{{--                        <span>Будьте в курсе!</span>--}}

{{--                       Подписывайтесь на <a href="#">КДМ в Instagram</a> и не пропускайте интересные события ;)--}}
                    </div>
                </div>
                <div class="section-one__image">
                <div class="section-one__img">
{{--                        <img src="@if($kdm->headImg){{asset('/icons/social/section-one-img.svg')}}" @endif alt="img" > --}}
                        <img src="@if($kdm->headImg){{asset($kdm->headImg->img->url)}}" @endif alt="img" >
                    </div>
    				<div class="section-one__img-info">
{{--                        <h2 class="section-one__img-header">Асхабали Омаров</h2>--}}
                        <h3 class="section-one__img-subheader">{{$kdm->leader_post}}</h3>
                        <h2 class="section-one__img-header">{{$kdm->leader_name}}</h2>
{{--                        <h3 class="section-one__img-subheader">ПРЕДСЕДАТЕЛЬ КДМ ДГУ</h3>--}}
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-two">
        <div class="d_container">
            <h2 class="section-two__header">Структура КДМ ФИиИТ</h2>
            <div class="section-two__peoples">
                @foreach($servants as $key => $servant)
                <div class="people">
                    <div class="people__img"><img  src="@if($servant->headImg){{asset($servant->headImg->img->url)}}@endif" alt="img"></div>
                    <div class="people__header">{{$servant->title}}</div>
                    <div class="people__subheader">{{$servant->work_type}}</div>
                    <div class="people__description">{{$servant->description}}</div>
                </div>
                @endforeach
{{--                    <div class="people">--}}
{{--                        <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                        <div class="people__header">Саид Саидов</div>--}}
{{--                        <div class="people__subheader">пРЕДСЕДАТЕЛЬ КДМ ФИиИТ</div>--}}
{{--                        <div class="people__description">Защита учебных и социально-экономических прав, бытовых условий и других интересов студентов. Обеспечение членов Студдеканата правовой и социальной защитой.</div>--}}
{{--                    </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Тая Гаджиева</div>--}}
{{--                    <div class="people__subheader">информационный сектор</div>--}}
{{--                    <div class="people__description">Защита учебных и социально-экономических прав, бытовых условий и других интересов студентов. Обеспечение членов Студдеканата правовой и социальной защитой.</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Ислам Барышев</div>--}}
{{--                    <div class="people__subheader">интеллектуальный сектор</div>--}}
{{--                    <div class="people__description">Защита учебных и социально-экономических прав, бытовых условий и других интересов студентов. Обеспечение членов Студдеканата правовой и социальной защитой.</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Джамал Бадрутдинов</div>--}}
{{--                    <div class="people__subheader">волонтерский сектор</div>--}}
{{--                    <div class="people__description">Защита учебных и социально-экономических прав, бытовых условий и других интересов студентов. Обеспечение членов Студдеканата правовой и социальной защитой.</div>--}}
{{--                </div> --}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Аида Генжеева</div>--}}
{{--                    <div class="people__subheader">культмассовый сектор</div>--}}
{{--                    <div class="people__description">Защита учебных и социально-экономических прав, бытовых условий и других интересов студентов. Обеспечение членов Студдеканата правовой и социальной защитой.</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Магомедамин Салахбеков</div>--}}
{{--                    <div class="people__subheader">Спортивный сектор</div>--}}
{{--                    <div class="people__description">Защита учебных и социально-экономических прав, бытовых условий и других интересов студентов. Обеспечение членов Студдеканата правовой и социальной защитой.</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Арсен Сулейманов</div>--}}
{{--                    <div class="people__subheader">Военно-патриотический сектор</div>--}}
{{--                    <div class="people__description">Защита учебных и социально-экономических прав, бытовых условий и других интересов студентов. Обеспечение членов Студдеканата правовой и социальной защитой.</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Мухтар Алхасов</div>--}}
{{--                    <div class="people__subheader">Кураторский сектор</div>--}}
{{--                    <div class="people__description">Защита учебных и социально-экономических прав, бытовых условий и других интересов студентов. Обеспечение членов Студдеканата правовой и социальной защитой.</div>--}}
{{--                </div>--}}
            </div>
        </div>
    </section>

@endsection