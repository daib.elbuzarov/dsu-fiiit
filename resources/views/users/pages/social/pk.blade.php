@extends("users.layouts.index")

@section("content")
	<link href="{{asset('/css/social-pk.css')}}" rel="stylesheet">


{{--    <section class="section-one">--}}
{{--    	<div class="d_container">--}}
{{--    		<h1 class="section-one__header">Социальная работа</h1>--}}
{{--            <h2 class="section-one__subheader">Профсоюзный комитет</h2>--}}
{{--    		<div class="section-one__main">--}}
{{--    			<div class="section-one__information">--}}
{{--    				<div class="section-one__block">--}}
{{--    					Цель СНО - повышение качества подготовки студентов в соответствии с современными требованиями к высшему образованию, эффективному развитию и использованию творческого потенциала студентов в форме научно-исследовательской работы.--}}
{{--    				</div>--}}
{{--    				<div class="section-one__block">--}}
{{--    					<span>Как вступить в Профбюро IT-факультета?</span>--}}

{{--    					Достаточно заполнить форму <a href="#">здесь</a>.--}}
{{--    				</div>--}}
{{--    				<div class="section-one__block">--}}
{{--    					<span>Следите за новостями!</span>--}}

{{--    					Подписывайтесь на <a href="#">Профком в Instagram</a> и будьте в курсе всех новостей и событий от Профсоюзного комитета.--}}
{{--    				</div>--}}
{{--    			</div>--}}
{{--    			<div class="section-one__image">--}}
{{--    				<div class="section-one__img">--}}
{{--                        <img src="{{asset('/icons/social/plug.svg')}}" alt="img" >--}}
{{--                    </div>--}}
{{--    				<div class="section-one__img-info">--}}
{{--                        <h2 class="section-one__img-header">Мурад Абдурахманов</h2>--}}
{{--                        <h3 class="section-one__img-subheader">Председатель Профбюро ФИиИТ</h3>--}}
{{--                    </div>--}}
{{--    			</div>--}}
{{--    		</div>--}}
{{--    	</div>--}}
{{--    </section>--}}

    <section class="section-one">
        <div class="d_container">
            <h1 class="section-one__header">Социальная работа</h1>
            <h2 class="section-one__subheader">Профсоюзный комитет</h2>
            <div class="section-one__main">
                <div class="section-one__information">
                    <div class="section-one__block">
                        {!! $pk->purpose !!}
                        {{--                    Цель СНО - повышение качества подготовки студентов в соответствии с современными требованиями к высшему образованию, эффективному развитию и использованию творческого потенциала студентов в форме научно-исследовательской работы.--}}
                    </div>
                    <div class="section-one__block">
                        {!! $pk->enter !!}
                        {{--    					<span>Хочу в Студдеканат!</span>--}}

                        {{--    					Чтобы вступить в студдеканат, достаточно заполнить форму <a href="#">здесь</a>.--}}
                    </div>
                    <div class="section-one__block">
                        {!! $pk->postscript !!}
                        {{--    					<span>Следите за новостями!</span>--}}

                        {{--    					Подписывайтесь на <a href="#">нас в Instagram</a> и будьте в курсе всех новостей и событий.--}}
                    </div>
                </div>
                <div class="section-one__image">
                    <div class="section-one__img">
                        {{--                        <img src="{{asset('/icons/social/section-one-img.svg')}}" alt="img" >--}}
                        <img src="@if($pk->headImg){{asset($pk->headImg->img->url)}}" @endif alt="img" >
                    </div>
                    <div class="section-one__img-info">
                        <h3 class="section-one__img-subheader">{{$pk->leader_post}}</h3>
                        <h2 class="section-one__img-header">{{$pk->leader_name}}</h2>
                        {{--                        <h2 class="section-one__img-header">Саид Саидов</h2>--}}
                        {{--                        <h3 class="section-one__img-subheader">Студенческий декан</h3>--}}
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-two">
        <div class="d_container">
            <h2 class="section-two__header">руководители комиссий</h2>
            <div class="section-two__peoples">
                @foreach($servants as $key => $servant)
                    <div class="people">
                        <div class="people__img"><img  src="@if($servant->headImg){{asset($servant->headImg->img->url)}}@endif" alt="img"></div>
                        <div class="people__header">{{$servant->title}}</div>
                        <div class="people__subheader">{{$servant->work_type}}</div>
                        <div class="people__description">{{$servant->description}}</div>
                    </div>
                @endforeach
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Рагима Исаева</div>--}}
{{--                    <div class="people__description">Заместитель председателя Профбюро ФИиИТ</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Яхъя Абакаров</div>--}}
{{--                    <div class="people__description">Заместитель председателя Профбюро ФИиИТ</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Гасан-Гусейн Магомедов</div>--}}
{{--                    <div class="people__description">Заместитель председателя Профбюро ФИиИТ</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Аида Генжеева</div>--}}
{{--                    <div class="people__description">Заместитель председателя Профбюро ФИиИТ</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Тая Гаджиева</div>--}}
{{--                    <div class="people__description">Заместитель председателя Профбюро ФИиИТ</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Магомед Абдуллаев</div>--}}
{{--                    <div class="people__description">Заместитель председателя Профбюро ФИиИТ</div>--}}
{{--                </div>--}}
{{--                <div class="people">--}}
{{--                    <div class="people__img"><img src="{{asset('/icons/social/section-two-img-1.svg')}}" alt="img"></div>--}}
{{--                    <div class="people__header">Магомедамин Салахбеков</div>--}}
{{--                    <div class="people__description">Заместитель председателя Профбюро ФИиИТ</div>--}}
{{--                </div>--}}

    		</div>
    	</div>
    </section>

@endsection