@extends("users.layouts.index")

@section("content")
    <div class="container">
        <div class="subheader bachelor">
            <ul class="subheader_menu">
                <li class="subheader_menu_item"><a href="{{asset('/bachelor')}}">Бакалавриат и магистратура</a></li>
                <li class="subheader_menu_item"><a href="">Образовательные программы</a></li>
            </ul>
        </div>
    </div>


    <div class="bach-container">
        <div class="d_container">
            <h1 class="d_h1">Абитуриентам</h1>
            <div class="bach-welcome">
                <img class="bach-icon1" src="{{asset('/icons/bachelor/bach_icon1.svg')}}" alt="">
                <img class="bach-icon2" src="{{asset('/icons/bachelor/bach_icon2.svg')}}" alt="">

                <h2 class="d_h2">Добро пожаловать, абитуриент!</h2>

                <p> Если Ваша цель – формирование высокого интеллектуального капитала и
                    достижение успеха в жизни, то мы ждем Вас в Дагестанском государственном
                    университете на Факультете Информатики и Информационных технологий. <br>
                    Ознакомьтесь с направлениями, необходимым количеством баллов для поступления,
                    документацией и стоимостью обучения.</p>
            </div>

            <div class="bach-tabs">
                <div class="d_container">

                </div>
                <input type="radio" name="tab-btn" id="tab-btn-1" value="" checked>
                <label for="tab-btn-1">Бакалавриат</label>
                <input type="radio" name="tab-btn" id="tab-btn-2" value="">
                <label for="tab-btn-2" class="tab_label_last">Магистратура</label>

                <div id="tab-bachelor" class="tab-bach-content">

                    <div class="tab-bach-accordion">

                        <section class="ac-container">
                            @foreach($bachelor_courses as $key => $course)
                                <div class="ac-item">
                                    {{--                        <input id="ac-3" name="accordion-1" type="checkbox" />--}}
                                    <label class="ac-label" onclick="acItemShow(this)">{{$course->title}}</label>
                                    <article class="ac-item-content">
                                        <div class="specialty-description">
                                            @foreach($course->sectors as $sector)
                                                <div class="specialty-title">
                                                    <span class="specialty-number">{{$sector->number}}</span>
                                                    <span>{{$sector->title}}</span>
                                                </div>
                                            @endforeach

                                            <div class="specialty-info">
                                                <p>
                                                    {!! $course->description !!}
                                                </p>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            @endforeach

                            <p>{!! $pt['read']->value !!}</p>
                        </section>
                    </div>

                    <div class="bach-subcontainer">
                        <ul class="specialty-info-list">
                            <li>{!! $pt['bachelor_rules']->value !!}</li>
                            <li>{!! $pt['bachelor_cost']->value !!}</li>
                            <li>{!! $pt['bachelor_blanks']->value !!}</li>
                        </ul>
                    </div>

                    <div class="tab-bach-title">
                        Перечень вступительных испытаний (для всех направлений)
                    </div>

                    <div class="bach-subcontainer">
                        <div class="bach-tests">
                            <ul>
                                <li>Результаты ЕГЭ</li>
                                <li>Русский язык</li>
                                <li>Математика (профиль)</li>
                                <li>Информатика</li>
                            </ul>

                            <ul>
                                <li>Вступительные испытания</li>
                                <li>Русский язык</li>
                                <li>Математика (профиль)</li>
                                <li>Информатика</li>
                            </ul>
                        </div>
                    </div>

                </div>


                {{--            Магистратура               --}}

                <div id="tab-magistracy" class="tab-mag-content">
                    <div class="tab-bach-accordion">

                        <section class="ac-container">
                            @foreach($master_courses as $key => $course)
                                <div class="ac-item">
                                    {{--                        <input id="ac-3" name="accordion-1" type="checkbox" />--}}
                                    <label class="ac-label" onclick="acItemShow(this)">{{$course->title}}</label>
                                    <article class="ac-item-content">
                                        <div class="specialty-description">
                                            @foreach($course->sectors as $sector)
                                                <div class="specialty-title">
                                                    <span class="specialty-number">{{$sector->number}}</span>
                                                    <span>{{$sector->title}}</span>
                                                </div>
                                            @endforeach

                                            <div class="specialty-info">
                                                <p>
                                                    {!! $course->description !!}
                                                </p>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            @endforeach
                        </section>
                    </div>

                    <div class="bach-subcontainer">
                        <ul class="specialty-info-list">
                            <li>{!! $pt['master_rules']->value !!}</li>
                            <li>{!! $pt['master_cost']->value !!}</li>
                            <li>{!! $pt['master_blanks']->value !!}</li>
                        </ul>
                    </div>

                    <div class="tab-bach-title">
                        Перечень вступительных испытаний (для всех направлений)
                    </div>

                    <div class="bach-subcontainer">
                        <div class="bach-tests">
                            <ul>
                                <li>Вступительные испытания</li>
                                <li>Письменный экзамен по направлению</li>
                            </ul>
                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>

@endsection
