@extends("users.layouts.index")


@section("content")
    <div class="d_container">
        <h1 class="d_h1">Абитуриентам</h1>
        <div class="bach-welcome programs-welcome">
            <img class="programs-icon1" src="{{asset('/icons/bachelor/programs_icon1.svg')}}" alt="">
            <img class="programs-icon2" src="{{asset('/icons/bachelor/programs_icon2.svg')}}" alt="">

            <h2 class="d_h2">Образовательные программы</h2>

            <p> Ознакомьтесь с образовательными программами и стандартами высшего профессионального образования по
                направлениям подготовки.</p>
        </div>


        {{--        Бакалавриат         --}}
        <div class="bach-tabs programs-tabs">
            <input type="radio" name="tab-btn" id="tab-btn-1" value="" checked>
            <label for="tab-btn-1">Бакалавриат</label>

            <input type="radio" name="tab-btn" id="tab-btn-2" value="">
            <label for="tab-btn-2" class="tab_label_last">Магистратура</label>

            <div id="tab-bachelor" class="tab-bach-content">
                <div class="tab-bach-accordion">

                    <section class="ac-container ac-container-programs">
                        @foreach($bachelor_courses as $key => $course)
                            <div class="ac-item">
                                <label onclick="acItemShow(this)">{{$course->title}}</label>
                                <article class="ac-item-content">
                                    <div class="specialty-description">
                                        @foreach($course->sectors as $key2 => $sector)
                                            <div class="programs-items-wrap">
                                                <div class="programs-items-title">
                                                    {{$sector->title}}
                                                </div>
                                                @foreach($sector->sectorItems as $key3 => $item)
                                                    <div class="programs-items">
                                                        @if($item->url)<a target="_blank"
                                                                          href="{{$item->url}}">{{$item->title}}</a>@endif
                                                        @if($item->file)<a download=""
                                                                           href="{{asset($item->file)}}">{{$item->title}}</a>@endif
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach

                                    </div>
                                </article>
                            </div>
                        @endforeach

                        <p>Если у вас остались вопросы, напишите нам.</p>
                    </section>
                </div>

            </div>


            {{--            Магистратура               --}}
            <div id="tab-magistracy" class="tab-mag-content">
                <div class="tab-bach-accordion">

                    <section class="ac-container">
                        @foreach($master_courses as $key => $course)
                            <div class="ac-item">
                                <label onclick="acItemShow(this)">{{$course->title}}</label>
                                <article class="ac-item-content">
                                    <div class="specialty-description">
                                        @foreach($course->sectors as $key2 => $sector)
                                            <div class="programs-items-wrap">
                                                <div class="programs-items-title">
                                                    {{$sector->title}}
                                                </div>
                                                @foreach($sector->sectorItems as $key3 => $item)
                                                    <div class="programs-items">
                                                        @if($item->url)<a target="_blank"
                                                                          href="{{$item->url}}">{{$item->title}}</a>@endif
                                                        @if($item->file)<a download=""
                                                                           href="{{asset($item->file)}}">{{$item->title}}</a>@endif
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach


                                    </div>
                                </article>
                            </div>
                        @endforeach

                        <p>Если у вас остались вопросы, напишите нам.</p>
                    </section>
                </div>

            </div>
        </div>

    </div>
@endsection
