@extends("users.layouts.index")
@section("content")
    <div class="d_container">
        <h1 class="search_title d_h1">Поиск</h1>

        <form action="{{route('post.search')}}" method="POST">
            @csrf
            <div class="search_wrap">
                <input class="search_input" name="value" required max="255" type="text" autofocus>
                <img src="{{asset('/icons/search/search_settings.svg')}}" class="search_settings_icon"
                     onclick="showSearchSettings()" alt="">
                <img src="{{asset('/icons/search/search_settings.svg')}}" class="search_settings_icon _mobile"
                     onclick="showSearchSettingsMobile()" alt="">

                <button type="submit" class="search_btn">Найти</button>
                <button type="submit" class="search_btn_mobile">
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.0259 13.8475L18.5951 17.4158L17.4159 18.595L13.8476 15.0258C12.5199 16.0902 10.8684 16.6691 9.16675 16.6667C5.02675 16.6667 1.66675 13.3067 1.66675 9.16666C1.66675 5.02666 5.02675 1.66666 9.16675 1.66666C13.3067 1.66666 16.6667 5.02666 16.6667 9.16666C16.6692 10.8683 16.0903 12.5198 15.0259 13.8475ZM13.3542 13.2292C14.4118 12.1416 15.0025 10.6837 15.0001 9.16666C15.0001 5.94332 12.3892 3.33332 9.16675 3.33332C5.94341 3.33332 3.33341 5.94332 3.33341 9.16666C3.33341 12.3892 5.94341 15 9.16675 15C10.6838 15.0024 12.1417 14.4118 13.2292 13.3542L13.3542 13.2292Z" fill="black"/>
                    </svg>
                </button>

                {{--            меню настройки поиска--}}
                <div onclick="closeSearchSettingsMobile()" class="search_settings_menu_back" id="menu_back">
                </div>
                <div class="search_settings_menu" id="search_settings">
                    <img src="{{asset('/icons/search/search_settings_close_mobile.svg')}}" class="search_settings_close _mobile"
                         onclick="closeSearchSettingsMobile()" alt="close">

                    <div class="search_settings_left">
                        <div class="search_by">
                            <div class="search_settings_title">
                                Искать по:
                            </div>

                            <label class="search_settings_label">
                                <input value="1" type="checkbox" name="title" checked>
                                <span class="search_fake_checkbox"></span>
                                <span class="search_checkbox_text">Заголовкам</span>
                            </label>

                            <label class="search_settings_label">
                                <input value="1" type="checkbox" name="key_words" checked>
                                <span class="search_fake_checkbox"></span>
                                <span class="search_checkbox_text">Ключевым словам</span>
                            </label>

                        </div>

                        <div class="search_among">
                            <div class="search_settings_title">
                                Искать среди:
                            </div>

                            <label class="search_settings_label">
                                <input value="1" type="checkbox" name="item[news]" checked>
                                <span class="search_fake_checkbox"></span>
                                <span class="search_checkbox_text">Новостей</span>
                            </label>

                            <label class="search_settings_label">
                                <input value="1" type="checkbox" name="item[ads]" checked>
                                <span class="search_fake_checkbox"></span>
                                <span class="search_checkbox_text">Объявлений</span>
                            </label>

                            <label class="search_settings_label">
                                <input value="1" type="checkbox" name="item[contests]" checked>
                                <span class="search_fake_checkbox"></span>
                                <span class="search_checkbox_text">Конкурсов и грантов</span>
                            </label>
                        </div>
                    </div>

                    <img src="{{asset('/icons/search/search_settings_close.svg')}}" class="search_settings_close"
                         onclick="closeSearchSettings()" alt="close">
                </div>

            </div>
        </form>

        {{--        если ничего не найдено--}}
        @if(isset($result) && !count($result))
            <div class="search_result_text">
                По вашему запросу ничего не найдено.
            </div>

        @endif

    </div>

    {{--        если найдено --}}
    {{--        <div class="search_result_text">--}}
    {{--            Результаты поиска:--}}
    {{--        </div>--}}

    {{--        Новости  --}}



    @if(isset($result))
        @if(isset($result['news']) && count($result['news']))
            <section class="search_result_section">
                <div class="d_container">
                    <h3 class="search_result_title">Новости</h3>

                    <div class="search_block">
                        <div class="search_items_wrap search_news_wrap">
                            @foreach($result['news'] as $key => $item)
                                <div class="search_item search_item_news">
                                    <img @if($item->headImg->img)src="{{asset($item->headImg->img->url)}}" @endif alt=""
                                         class="search_news_img">
                                    <div class="search_item_info">
                                        <div class="search_news_date">{{$item->created_at->format("d.m.Y")}}</div>
                                        <div class="search_news_description">{{$item->title}}</div>
                                        <div class="search_news_type">{{$item->tag}}</div>
                                        <div class="search_news_more"><a
                                                    href="{{route('news.news-more',['id'=>$item->id])}}">Подробнее</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <a class="show_all_link" href="{{route('news')}}">Все новости</a>
                    </div>

                </div>
            </section>
        @endif

        @if(isset($result['ads']) && count($result['ads']))
            {{--        Объявления  --}}
            <section class="search_result_section">
                <div class="d_container">
                    <h3 class="search_result_title">Объявления</h3>

                    <div class="search_block">
                        <div class="search_items_wrap">
                            @foreach($result['ads'] as $key => $item)
                                <div class="search_item search_item_ads">
                                    <div class="search_ad_date">
                                        <span class="search_ad_day">{{$item->created_at->format('d')}}</span>
                                        <span class="search_ad_month">{{$item->created_at->format('m.Y')}}</span>
                                    </div>
                                    <div class="search_item_info">
                                        <a class="search_ad_title"
                                           href="{{route('news.advertisement-more',['id'=>$item->id])}}">{{$item->title}}</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <a class="show_all_link" href="{{route('news')}}">Все объявления</a>
                    </div>
                </div>
            </section>
        @endif

        @if(isset($result['contests']) && count($result['contests']))
            {{--        Конкурсы и гранты  --}}
            <section class="search_result_section">
                <div class="d_container">
                    <h3 class="search_result_title">Конкурсы и гранты</h3>

                    <div class="search_block">
                        <div class="search_items_wrap">
                            @foreach($result['contests'] as $key => $item)
                                <div class="search_item search_item_contests">
                                    <div class="search_contests_date_wrap">
                                        ДО {{\Carbon\Carbon::createFromDate($item->date_end)->format("d.m")}}
                                    </div>
                                    <div class="search_item_info">
                                        <div class="search_contest_type">{{$item->tag}}</div>
                                        @if($item->type_id ==  \App\Models\Types::getID(\App\Models\Types::CODE_CONTEST,\App\Models\Contest::table))
                                            <a class="search_contest_title"
                                               href="{{route('science.contests-more',['id'=>$item->id])}}">{{$item->title}}</a>@endif
                                        @if($item->type_id ==  \App\Models\Types::getID(\App\Models\Types::CODE_GRANT,\App\Models\Contest::table))
                                            <a class="search_contest_title"
                                               href="{{route('science.grants-more',['id'=>$item->id])}}">{{$item->title}}</a>@endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <a class="show_all_link" href="{{route('science.contests')}}">Все конкурсы и гранты</a>
                    </div>
                </div>
            </section>
        @endif
    @endif

{{--    потом закоментировать--}}

{{--            <section class="search_result_section">--}}
{{--                <div class="d_container">--}}
{{--                    <h3 class="search_result_title">Новости</h3>--}}

{{--                    <div class="search_block">--}}
{{--                        <div class="search_items_wrap search_news_wrap">--}}
{{--                            <div class="search_item search_item_news">--}}
{{--                                <img src="{{asset('/icons/index/scince.svg')}}" alt="" class="search_news_img">--}}
{{--                                <div class="search_item_info">--}}
{{--                                    <div class="search_news_date">13.02.2021</div>--}}
{{--                                    <div class="search_news_description">Встреча с ректором</div>--}}
{{--                                    <div class="search_news_type">Научная деятельность</div>--}}
{{--                                    <div class="search_news_more"><a href="#">Подробнее</a></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="search_item search_item_news">--}}
{{--                                <img src="{{asset('/icons/index/girls.svg')}}" alt="" class="search_news_img">--}}
{{--                                <div class="search_item_info">--}}
{{--                                    <div class="search_news_date">21.03.2020</div>--}}
{{--                                    <div class="search_news_description">В школах районов прошли встречи со школьниками</div>--}}
{{--                                    <div class="search_news_type">Профориентационная работа</div>--}}
{{--                                    <div class="search_news_more"><a href="#">Подробнее</a></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="search_item search_item_news">--}}
{{--                                <img src="{{asset('/icons/index/girls.svg')}}" alt="" class="search_news_img">--}}
{{--                                <div class="search_item_info">--}}
{{--                                    <div class="search_news_date">13.12.2020</div>--}}
{{--                                    <div class="search_news_description">Встреча с ректором</div>--}}
{{--                                    <div class="search_news_type">Профориентационная работа</div>--}}
{{--                                    <div class="search_news_more"><a href="#">Подробнее</a></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="search_item search_item_news">--}}
{{--                                <img src="{{asset('/icons/index/scince.svg')}}" alt="" class="search_news_img">--}}
{{--                                <div class="search_item_info">--}}
{{--                                    <div class="search_news_date">24.12.2020</div>--}}
{{--                                    <div class="search_news_description">В школах районов прошли встречи со школьниками</div>--}}
{{--                                    <div class="search_news_type">Научная деятельность</div>--}}
{{--                                    <div class="search_news_more"><a href="#">Подробнее</a></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <a class="show_all_link" href="#">Все новости</a>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--            </section>--}}

{{--                    Объявления--}}
{{--            <section class="search_result_section">--}}
{{--                <div class="d_container">--}}
{{--                    <h3 class="search_result_title">Объявления</h3>--}}

{{--                    <div class="search_block">--}}
{{--                        <div class="search_items_wrap">--}}
{{--                            <div class="search_item search_item_ads">--}}
{{--                                <div class="search_ad_date">--}}
{{--                                    <span class="search_ad_day">15</span>--}}
{{--                                    <span class="search_ad_month">01.21</span>--}}
{{--                                </div>--}}
{{--                                <div class="search_item_info">--}}
{{--                                    <a class="search_ad_title" href="#">Студенты проведут встречи со школьниками в школах районов</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="search_item search_item_ads">--}}
{{--                                <div class="search_ad_date">--}}
{{--                                    <span class="search_ad_day">15</span>--}}
{{--                                    <span class="search_ad_month">01.21</span>--}}
{{--                                </div>--}}
{{--                                <div class="search_item_info">--}}
{{--                                    <a class="search_ad_title" href="#">Встреча с ректором состоится когда-нибудь</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="search_item search_item_ads">--}}
{{--                                <div class="search_ad_date">--}}
{{--                                    <span class="search_ad_day">15</span>--}}
{{--                                    <span class="search_ad_month">01.21</span>--}}
{{--                                </div>--}}
{{--                                <div class="search_item_info">--}}
{{--                                    <a class="search_ad_title" href="#">Студенты проведут встречи со школьниками в школах районов</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="search_item search_item_ads">--}}
{{--                                <div class="search_ad_date">--}}
{{--                                    <span class="search_ad_day">15</span>--}}
{{--                                    <span class="search_ad_month">01.21</span>--}}
{{--                                </div>--}}
{{--                                <div class="search_item_info">--}}
{{--                                    <a class="search_ad_title" href="#">Студенты проведут встречи со школьниками в школах районов</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <a class="show_all_link" href="#">Все объявления</a>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--            </section>--}}


{{--                    Конкурсы и гранты--}}
{{--            <section class="search_result_section">--}}
{{--                <div class="d_container">--}}
{{--                    <h3 class="search_result_title">Конкурсы и гранты</h3>--}}

{{--                    <div class="search_block">--}}
{{--                        <div class="search_items_wrap">--}}
{{--                            <div class="search_item search_item_contests">--}}
{{--                                <div class="search_contests_date_wrap">--}}
{{--                                    ДО 15.01--}}
{{--                                </div>--}}
{{--                                <div class="search_item_info">--}}
{{--                                    <div class="search_contest_type">Социальные проекты</div>--}}
{{--                                    <a class="search_contest_title" href="#">Конкурс-фестиваль проектов сообщества PRO Женщин PRO WOMEN AWARD</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="search_item search_item_contests">--}}
{{--                                <div class="search_contests_date_wrap">--}}
{{--                                    ДО 10.01--}}
{{--                                </div>--}}
{{--                                <div class="search_item_info">--}}
{{--                                    <div class="search_contest_type">Социальные проекты</div>--}}
{{--                                    <a class="search_contest_title" href="#">Конкурс-фестиваль проектов сообщества PRO Женщин PRO WOMEN AWARD</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="search_item search_item_contests">--}}
{{--                                <div class="search_contests_date_wrap">--}}
{{--                                    ДО 10.01--}}
{{--                                </div>--}}
{{--                                <div class="search_item_info">--}}
{{--                                    <div class="search_contest_type">Социальные проекты</div>--}}
{{--                                    <a class="search_contest_title" href="#">Конкурс-фестиваль проектов сообщества PRO Женщин PRO WOMEN AWARD</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="search_item search_item_contests">--}}
{{--                                <div class="search_contests_date_wrap">--}}
{{--                                    ДО 10.01--}}
{{--                                </div>--}}
{{--                                <div class="search_item_info">--}}
{{--                                    <div class="search_contest_type">Социальные проекты</div>--}}
{{--                                    <a class="search_contest_title" href="#">Конкурс-фестиваль проектов сообщества PRO Женщин PRO WOMEN AWARD</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <a class="show_all_link" href="#">Все конкурсы и гранты</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </section>--}}

@endsection
