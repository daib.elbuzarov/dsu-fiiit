<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ДГУ ФИиИТ</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap"
          rel="stylesheet">
    @include('users.layouts.styles')

</head>
<body>
@yield("content")
<header>
    <div class="main-header__block">
        <div class="container">
			<div class="main-header__content" id="fullscreen">
				<a href="{{route('home')}}"><img src="{{asset('/icons/index/logo.svg')}}" alt="logo" class="header_img"></a>
				<div class="main__header">
					<div class="main__header-faculty">
						<a id="head1" href="{{route('structure')}}" class="main-header__title">Факультет</a>
						<ul class="subheader_menu faculty">
							<li class="main-subheader_menu_item"><a href="{{route('structure')}}">Структура факультета</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('news')}}">Новости и объявления</a></li>
						</ul>

						<ul class="subheader_menu main">
							<li class="main-subheader_menu_item"><a href="{{route('home')}}">На главную</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('news')}}">Новости и объявления</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('timetable')}}">Расписание</a></li>
						</ul>
					</div>

					<div class="main__header-bachelor">
						<a id="head2" href="{{route('candidate')}}" class="main-header__title">Абитуриентам</a>
						<ul class="subheader_menu bachelor">
							<li class="main-subheader_menu_item"><a href="{{route('candidate')}}">Бакалавриат и магистратура</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('candidate.programs')}}">Образовательные программы</a></li>
						</ul>
					</div>

					<div class="main__header-student">
						<a id="head3" href="{{route('timetable')}}" class="main-header__title">Студентам</a>
						<ul class="subheader_menu student">
							<li class="main-subheader_menu_item"><a href="{{route('timetable')}}">Расписание</a></li>
                			<li class="main-subheader_menu_item"><a href="{{route('resources')}}">Учебные ресурсы</a></li>
                			<li class="main-subheader_menu_item"><a href="{{route('document')}}">Образцы документов</a></li>
						</ul>
					</div>

					<div class="main__header-social">
						<a id="head4" href="{{route('social.sd')}}" class="main-header__title">Социальная работа</a>
						<ul class="subheader_menu social">
							<li class="main-subheader_menu_item"><a href="{{route('social.sd')}}">Студенческий деканат</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('social.kdm')}}">Комитет по делам молодежи</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('social.pk')}}">Профсоюзный комитет</a></li>
						</ul>
					</div>

					<div class="main__header-science">
						<a id="head5" href="{{route('science.sno')}}" class="main-header__title">Научная деятельность</a>
						<ul class="subheader_menu study">
							<li class="main-subheader_menu_item"><a href="{{route('science.sno')}}">Студенческое научное общество</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('science.smu')}}">Совет молодых ученых</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('science.mugs')}}">Научные кружки</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('science.contests')}}">Конкурсы и гранты</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('science.projects')}}">Проектная деятельность</a></li>
						</ul>
					</div>
				</div>
				<a href="{{route('search')}}"><img src="{{asset('/icons/index/search1.svg')}}" alt="" class="main-header_search"></a>
			</div>

            <div class="ham__menu">
                    <span id="ham1" class="hamburger"></span>
                    <span id="ham2" class="hamburger"></span>
                    <span id="ham3" class="hamburger"></span>
            </div>

            <div class="main-header__content" id="mobile-screen">
				<div class="main__header">
                    <div id="mainBlock" class="main__header-main">
                            <a id="title6" href="{{route('structure')}}" class="main-header__title">Главная</a>
                            <ul id="main" class="main-subheader_menu main">
                                <li class="main-subheader_menu_item"><a href="{{route('home')}}">На главную</a></li>
                                <li class="main-subheader_menu_item"><a href="{{route('news')}}">Новости и объявления</a></li>
                                <li class="main-subheader_menu_item"><a href="{{route('timetable')}}">Расписание</a></li>
                            </ul>
                    </div>

					<div id="facBlock" class="main__header-faculty">
						<a id="title1" href="{{route('structure')}}" class="main-header__title">Факультет</a>
						<ul id="fac" class="main-subheader_menu faculty">
							<li class="main-subheader_menu_item"><a href="{{route('structure')}}">Структура факультета</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('news')}}">Новости и объявления</a></li>
						</ul>

						<ul class="main-subheader_menu main">
                            <li class="main-subheader_menu_item"><a href="{{route('home')}}">На главную</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('news')}}">Новости и объявления</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('timetable')}}">Расписание</a></li>
						</ul>
					</div>

					<div id="bachBlock" class="main__header-bachelor">
						<a id="title2" href="{{route('candidate')}}" class="main-header__title">Абитуриентам</a>
						<ul id="bach" class="main-subheader_menu bachelor">
							<li class="main-subheader_menu_item"><a href="{{route('candidate')}}">Бакалавриат и магистратура</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('candidate.programs')}}">Образовательные программы</a></li>
						</ul>
					</div>

					<div id="studBlock" class="main__header-student">
						<a id="title3" href="{{route('timetable')}}" class="main-header__title">Студентам</a>
						<ul id="stud" class="main-subheader_menu student">
							<li class="main-subheader_menu_item"><a href="{{route('timetable')}}">Расписание</a></li>
                			<li class="main-subheader_menu_item"><a href="{{route('resources')}}">Учебные ресурсы</a></li>
                			<li class="main-subheader_menu_item"><a href="{{route('document')}}">Образцы документов</a></li>
						</ul>
					</div>

					<div id="socBlock" class="main__header-social">
						<a id="title4" href="{{route('social.sd')}}" class="main-header__title">Социальная работа</a>
						<ul id="soc" class="main-subheader_menu social">
							<li class="main-subheader_menu_item"><a href="{{route('social.sd')}}">Студенческий деканат</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('social.kdm')}}">Комитет по делам молодежи</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('social.pk')}}">Профсоюзный комитет</a></li>
						</ul>
					</div>

					<div id="scienceBlock" class="main__header-science">
						<a id="title5" href="{{route('science.sno')}}" class="main-header__title">Научная деятельность</a>
						<ul id="scien" class="main-subheader_menu study">
							<li class="main-subheader_menu_item"><a href="{{route('science.sno')}}">Студенческое научное общество</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('science.smu')}}">Совет молодых ученых</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('science.mugs')}}">Научные кружки</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('science.contests')}}">Конкурсы и гранты</a></li>
							<li class="main-subheader_menu_item"><a href="{{route('science.projects')}}">Проектная деятельность</a></li>
						</ul>
					</div>
				</div>
			</div>
            <a href="{{route('search')}}" id="search"><img src="{{asset('/icons/index/search1.svg')}}" alt="" class="main-header_search"></a>
        </div>
    </div>
</header>

<section class="promo">
    <img src="{{asset('/icons/index/orangetop.svg')}}" alt="" class="promo__orange">
    <img src="{{asset('/icons/index/circles_elements.svg')}}" alt="" class="promo__circles">
    <img src="{{asset('/icons/index/test23.svg')}}" alt="" class="promo__anim">
    <img src="{{asset('/icons/index/promo_circles375.svg')}}" alt="" class="promo__circles-mobile">
    <div class="container">
            <div class="promo__circle">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <h1 class="promo__title">Факультет Информатики<br> и IT-технологий</h1>

            <div class="promo__info">
                <p class="promo__info-text">
                    <span>Образование в IT</span> - одно из важнейших направлений развития
                    современного образования.
                </p>
                <p class="promo__info-descr">
                    Информационные технологии призваны коренным образом изменить темпы развития общества в третьем
                    тысячелетии и подходы общества к решению новейших образовательных программ.
                </p>
                <img src="{{asset('/icons/index/selebyellow.svg')}}" alt="" class="promo__info-icon">
                <a href="{{$pt['want_study']->link}}" target="_blank">
                    <button class="promo__info-btn">{!! $pt['want_study']->title !!}</button>
                </a>
            </div>
            <img src="{{asset('/icons/index/stars.svg')}}" alt="" class="promo__stars">
    </div>
</section>

<section class="news">
    <div class="container">
        <div class="news__content">
            <div class="news__tidings">
                <div class="news__tidings-title">Новости</div>
                @foreach($last_news as $key => $item)
                    @if($key == 0)
                        <a href="{{route('news.news-more',['id'=>$item->id])}}">
                            <div class="news__tidings-banner">
                                <img src="@if(isset($item->headImg)){{asset($item->headImg->img->url)}}@endif" alt="" class="news__banner-img">
                                <div class="news__banner-date">
                                    <div class="news__banner-num">{{$item->created_at->format('d')}}</div>
                                    <span> {{$item->created_at->format('m.y')}}</span>
                                </div>
                                <div class="news__banner-block">
                                    <div class="news__banner-descr">{{$item->title}}</div>
                                    <div class="news__banner-type">{{$item->tag}}</div>
                                </div>
                            </div>
                        </a>
                    @else
                        <div class="news__tidings-info">
                            <img src="@if(isset($item->headImg)){{asset($item->headImg->img->url)}}@endif" alt="" class="news__info-img">
                            <div class="news__info-block">
                                <div class="news__info-date">{{$item->created_at->format('d.m.y')}}</div>
                                <div class="news__info-item">
                                    <div class="news__info-descr">{{$item->title}}</div>
                                    <div class="news__info-type">{{$item->tag}}</div>
                                </div>
                                <div class="news__info-more"><a href="{{route('news.news-more',['id'=>$item->id])}}">Подробнее</a></div>
                            </div>
                        </div>
                    @endif
                @endforeach

{{--                <div class="news__tidings-info">--}}
{{--                    <img src="{{asset('/icons/index/girls.svg')}}" alt="" class="news__info-img">--}}
{{--                    <div class="news__info-block">--}}
{{--                        <div class="news__info-date">20.01.2021</div>--}}
{{--                        <div class="news__info-descr">Встречи со школьниками</div>--}}
{{--                        <div class="news__info-type">Профориентационная работа</div>--}}
{{--                        <div class="news__info-more"><a>Подробнее</a></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="news__tidings-all">
                    <span><a href="{{route('news')}}">Все новости</a></span>
                    <img src="{{asset('/icons/index/arrow.svg')}}" alt="" class="news__all-img">
                </div>
            </div>
            <div class="news__ads">
                <div class="news__ads-title">Объявления</div>
                <div class="news__ads-event">
                    @foreach($last_ads as $key => $item)
                    <div class="news__event-item">
                        <div class="news__event-date">{{$item->created_at->format('d')}}<span>{{$item->created_at->format('m.y')}}</span></div>
                        <div class="news__event-text"><a href="{{route('news.advertisement-more',['id'=>$item->id])}}">{{$item->title}}</a></div>
                    </div>
                    @endforeach
{{--                    <div class="news__event-item">--}}
{{--                        <div class="news__event-date">13<span>01.21</span></div>--}}
{{--                        <div class="news__event-text">На территории факультета планируется разместить что-то</div>--}}
{{--                    </div>--}}
{{--                    <div class="news__event-item">--}}
{{--                        <div class="news__event-date">13<span>01.21</span></div>--}}
{{--                        <div class="news__event-text"><span>Такое вот интересное событие нас ожидает!</span></div>--}}
{{--                    </div>--}}
{{--                    <div class="news__event-item">--}}
{{--                        <div class="news__event-date">13<span>01.21</span></div>--}}
{{--                        <div class="news__event-text">--}}
{{--                            <span>Студенты проведут встречи со школьниками в школах районов</span></div>--}}
{{--                    </div>--}}
                </div>
                <div class="news__ads-all">
                    <span><a href="{{route('news')}}">Все объявления</a></span>
                    <img src="{{asset('/icons/index/arrow.svg')}}" alt="" class="news__all-img">
                </div>
{{--                <div class="news__ads-content">--}}
{{--                    <img src="{{asset('/icons/index/inst-banner.svg')}}" alt="" class="inst__banner">--}}
{{--                </div>--}}
                @isset($banners['main_banner'],$banners['main_banner']->headImg,$banners['main_banner']->headImg->img)
                <div class="news__ads-content">
                    <img src="{{$banners['main_banner']->headImg->img->url}}" alt="" class="inst__banner">
                </div>
                @endisset
            </div>
        </div>
    </div>
</section>

<section class="contest">
    <div class="container">
        <div class="contest__content">
            <div class="contest__titles">
                <div class="contest__titles-competition">Конкурсы</div>
                <div class="contest__titles-grant">Гранты</div>
            </div>
            <div class="black"></div>
            <hr class="contest__titles-line">

            <div class="contest__competition">
                @foreach($contests as $key => $item)
                <div class="contest__competition-item">
                    <div class="contest__competition-deadline">
                        <div class="contest__deadline-container">
                            <div class="contest__competition-date">ДО {{\Carbon\Carbon::createFromDate($item->date_end)->format('d.m')}}</div>
                        </div>
                    </div>
                    <div class="contest__competition-text">
                        <div class="contest__competition-type">{{$item->tag}}</div>
                        <a href="{{route('science.contests-more',['id'=>$item->id])}}">
                            <div class="contest__competition-descr">{{$item->title}}
                            </div>
                        </a>
                    </div>
                </div>
                @endforeach

{{--                <div class="contest__competition-item">--}}
{{--                    <div class="contest__competition-deadline">--}}
{{--                        <div class="contest__deadline-container">--}}
{{--                            <div class="contest__competition-date">ДО 15.01</div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="contest__competition-text">--}}
{{--                        <div class="contest__competition-type">Социальные проекты</div>--}}
{{--                        <div class="contest__competition-descr">Конкурс-фестиваль проектов сообщества PRO Женщин PRO--}}
{{--                            WOMEN AWARD--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="contest__competition-item">--}}
{{--                    <div class="contest__competition-deadline">--}}
{{--                        <div class="contest__deadline-container">--}}
{{--                            <div class="contest__competition-date">ДО 15.01</div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="contest__competition-text">--}}
{{--                        <div class="contest__competition-type">Социальные проекты</div>--}}
{{--                        <div class="contest__competition-descr">Конкурс-фестиваль проектов сообщества PRO Женщин PRO--}}
{{--                            WOMEN AWARD--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
            <div class="contest__competition contest__grant">
                @foreach($grants as $key => $item)
                <div class="contest__competition-item">
                    <div class="contest__competition-deadline">
                        <div class="contest__deadline-container">
                            <div class="contest__competition-date">ДО {{\Carbon\Carbon::createFromDate($item->date_end)->format('d.m')}}</div>
                        </div>
                    </div>
                    <div class="contest__competition-text">
                        <div class="contest__competition-type">{{$item->tag}}</div>
                        <a href="{{route('science.grants-more',['id'=>$item->id])}}">
                            <div class="contest__competition-descr">{{$item->title}}
                            </div>
                        </a>
                    </div>
                </div>
                @endforeach

{{--                <div class="contest__competition-item">--}}
{{--                    <div class="contest__competition-deadline">--}}
{{--                        <div class="contest__deadline-container">--}}
{{--                            <div class="contest__competition-date">ДО 15.01</div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="contest__competition-text">--}}
{{--                        <div class="contest__competition-type">Социальные проекты</div>--}}
{{--                        <div class="contest__competition-descr">Конкурс-фестиваль проектов сообщества PRO Женщин PRO--}}
{{--                            WOMEN AWARD--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="contest__competition-item">--}}
{{--                    <div class="contest__competition-deadline">--}}
{{--                        <div class="contest__deadline-container">--}}
{{--                            <div class="contest__competition-date">ДО 15.01</div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="contest__competition-text">--}}
{{--                        <div class="contest__competition-type">Социальные проекты</div>--}}
{{--                        <div class="contest__competition-descr">Конкурс-фестиваль проектов сообщества PRO Женщин PRO--}}
{{--                            WOMEN AWARD--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>

            <div class="competition-all">
                <span><a href="{{route('science.contests')}}">Все конкурсы и гранты</a></span>
                <img src="{{asset('/icons/index/arrow.svg')}}" alt="" class="news__all-img">
            </div>
        </div>
    </div>
</section>

<section class="fiiit">
    <div class="container">
        <div class="fiiit__content">
            <!-- <a href="{!! $pt['main_banner']->link !!}" target="_blank"><img src="{{asset('/icons/index/faces.svg')}}"
                alt="" class="fiiit__faces"></a>
                <div class="fiiit__tell">{!! $pt['main_banner']->value!!}</div> -->
            @isset($banners['footer_banner'],$banners['footer_banner']->headImg,$banners['footer_banner']->headImg->img)
            <img src="{{asset($banners['footer_banner']->headImg->img->url)}}" alt="" class="fiiit__banner">
            <img src="{{asset($banners['footer_banner']->smallImg->img->url)}}" alt="" class="fiiit__banner-2">
                                     @endisset
        </div>
    </div>
</section>

<section class="project">
    <div class="container">
        <div class="project__title"><span>Проекты студентов</span></div>
        <div class="project__content">
            @foreach($projects as $key => $item)
            <div class="project__content-item">
                <img @if($item->headImg) src="{{$item->headImg->img->url}}" @endif alt="image" class="project__image">
                <div class="project__content-block">
                    <div class="project__type">{{$item->tag}}</div>
                    <div class="project__descr"><a href="{{route("science.projects-more",['id'=>$item->id])}}">{{$item->title}}</a></div>
                </div>
            </div>
            @endforeach

{{--            <div class="project__content-item second">--}}
{{--                <div class="project__info">--}}
{{--                    <div class="project__type">Интересное</div>--}}
{{--                    <div class="project__descr">Стартер пак студента айтишника</div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="project__content-item third">--}}
{{--                <div class="project__info">--}}
{{--                    <div class="project__type">Интересное</div>--}}
{{--                    <div class="project__descr">Стартер пак студента айтишника</div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="project__content-item fourth">--}}
{{--                <div class="project__info">--}}
{{--                    <div class="project__type">Интересное</div>--}}
{{--                    <div class="project__descr">Стартер пак студента айтишника</div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
    <div class="project-all">
            <span><a href="{{route('science.projects')}}">Все проекты</a></span>
            <img src="{{asset('/icons/index/arrow.svg')}}" alt="" class="news__all-img">
    </div>
</section>

<section class="feedback">
    <div class="container">
        <h2 class="feedback__title"><span>Есть вопросы или предложения?</span></h2>
        <div class="feedback__content">
            <div class="feedback__mail">
                <div class="feedback__mail-write">Напишите нам!</div>
                <div class="feedback__mail-log">{!!  $pt['feedback']->value!!}</div>
            </div>
            <img src="{{asset('/icons/index/mail.svg')}}" alt="" class="feedback-icon">
        </div>
    </div>
</section>


@include('users.layouts.footer')
@include('users.layouts.scripts')
@section('scripts')
@show
</body>
</html>
