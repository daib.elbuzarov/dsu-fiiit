@extends("users.layouts.index")

@section("content")

    <link href="{{asset('/css/contests.css')}}" rel="stylesheet">
    <script defer src="{{asset('js/contests.js')}}"></script>

    <section class="section-one">
        <div class="d_container">
            <h1 class="section-one__header">Научная деятельность</h1>
            <h2 class="section-one__subheader">Конкурсы и гранты</h2>
        </div>
    </section>

    <section class="section-two">
        <div class="d_container">
            <div class="section-two__tabs">
                <div class="section-two__tab section-two__tab_active">Конкурсы</div>
                <div class="section-two__tab">Гранты</div>
            </div>
            <div class="section-two__divider"></div>
        </div>
    </section>

    <section class="section-three section-three_show">
        <div class="d_container">
            <div class="section-three__contests">
                @foreach($contests as $key => $item)
                    <div class="contests">
                        <div class="contests__date">
                            До {{\Carbon\Carbon::createFromDate($item->date_end)->format('d.m')}}</div>
                        <div class="contests__text">
                            <div class="contests__supheader">{{$item->tag}}</div>
                            <a href="{{route('science.contests-more',['id'=>$item->id])}}" class="contests__header">{{$item->title}}</a>
                        </div>
                    </div>
                @endforeach
                {{--                <div class="contests">--}}
                {{--                    <div class="contests__date">До 15.01</div>--}}
                {{--                    <div class="contests__text">--}}
                {{--                        <div class="contests__supheader">Социальные проекты</div>--}}
                {{--                        <a href="{{route('science.contests-more')}}" class="contests__header">Конкурс-фестиваль проектов сообщества PRO Женщин PRO WOMEN AWARD</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="contests">--}}
                {{--                    <div class="contests__date">До 15.01</div>--}}
                {{--                    <div class="contests__text">--}}
                {{--                        <div class="contests__supheader">Социальные проекты</div>--}}
                {{--                        <a href="{{route('science.contests-more')}}" class="contests__header">Первый молодежный цифровой хакатон “HackAtom”</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="contests">--}}
                {{--                    <div class="contests__date">До 15.01</div>--}}
                {{--                    <div class="contests__text">--}}
                {{--                        <div class="contests__supheader">Социальные проекты</div>--}}
                {{--                        <a href="{{route('science.contests-more')}}" class="contests__header">Студенты проведут встречи со школьниками в школах районов</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="contests">--}}
                {{--                    <div class="contests__date">До 15.01</div>--}}
                {{--                    <div class="contests__text">--}}
                {{--                        <div class="contests__supheader">Социальные проекты</div>--}}
                {{--                        <a href="{{route('science.contests-more')}}" class="contests__header">Студенты проведут встречи со школьниками в школах районов</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="contests">--}}
                {{--                    <div class="contests__date">До 15.01</div>--}}
                {{--                    <div class="contests__text">--}}
                {{--                        <div class="contests__supheader">Социальные проекты</div>--}}
                {{--                        <a href="{{route('science.contests-more')}}" class="contests__header">Студенты проведут встречи со школьниками в школах районов</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="contests">--}}
                {{--                    <div class="contests__date">До 15.01</div>--}}
                {{--                    <div class="contests__text">--}}
                {{--                        <div class="contests__supheader">Социальные проекты</div>--}}
                {{--                        <a href="{{route('science.contests-more')}}" class="contests__header">Студенты проведут встречи со школьниками в школах районов</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="contests">--}}
                {{--                    <div class="contests__date">До 15.01</div>--}}
                {{--                    <div class="contests__text">--}}
                {{--                        <div class="contests__supheader">Социальные проекты</div>--}}
                {{--                        <a href="{{route('science.contests-more')}}" class="contests__header">Студенты проведут встречи со школьниками в школах районов</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="contests">--}}
                {{--                    <div class="contests__date">До 15.01</div>--}}
                {{--                    <div class="contests__text">--}}
                {{--                        <div class="contests__supheader">Социальные проекты</div>--}}
                {{--                        <a href="{{route('science.contests-more')}}" class="contests__header">Студенты проведут встречи со школьниками в школах районов</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>
        </div>
    </section>


    <section class="section-three">
        <div class="d_container">
            <div class="section-three__grants">
                @foreach($grants as $key => $item)
                    <div class="grants">
                        <div class="grants__date">
                            До {{\Carbon\Carbon::createFromDate($item->date_end)->format('d.m')}}</div>
                        <div class="grants__text">
                            <div class="grants__supheader">{{$item->tag}}</div>
                            <a href="{{route('science.grants-more',['id'=>$item->id])}}" class="grants__header">{{$item->title}}</a>
                        </div>
                    </div>
                @endforeach
                {{--                <div class="grants">--}}
                {{--                    <div class="grants__date">До 15.01</div>--}}
                {{--                    <div class="grants__text">--}}
                {{--                        <div class="grants__supheader">Социальные проекты</div>--}}
                {{--                        <a href="{{route('science.grants-more')}}" class="grants__header">Стипендиальная программа 2021-2022 «Молодые таланты» (Линия Б) от DAAD</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="grants">--}}
                {{--                    <div class="grants__date">До 15.01</div>--}}
                {{--                    <div class="grants__text">--}}
                {{--                        <div class="grants__supheader">Социальные проекты</div>--}}
                {{--                        <a href="{{route('science.grants-more')}}" class="grants__header">Стипендиальная программа 2021-2022 «Молодые таланты» (Линия Б) от DAAD</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="grants">--}}
                {{--                    <div class="grants__date">До 15.01</div>--}}
                {{--                    <div class="grants__text">--}}
                {{--                        <div class="grants__supheader">Социальные проекты</div>--}}
                {{--                        <a href="{{route('science.grants-more')}}" class="grants__header">Стипендиальная программа 2021-2022 «Молодые таланты» (Линия Б) от DAAD</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="grants">--}}
                {{--                    <div class="grants__date">До 15.01</div>--}}
                {{--                    <div class="grants__text">--}}
                {{--                        <div class="grants__supheader">Социальные проекты</div>--}}
                {{--                        <a href="{{route('science.grants-more')}}" class="grants__header">Стипендиальная программа 2021-2022 «Молодые таланты» (Линия Б) от DAAD</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="grants">--}}
                {{--                    <div class="grants__date">До 15.01</div>--}}
                {{--                    <div class="grants__text">--}}
                {{--                        <div class="grants__supheader">Социальные проекты</div>--}}
                {{--                        <a href="{{route('science.grants-more')}}" class="grants__header">Стипендиальная программа 2021-2022 «Молодые таланты» (Линия Б) от DAAD</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="grants">--}}
                {{--                    <div class="grants__date">До 15.01</div>--}}
                {{--                    <div class="grants__text">--}}
                {{--                        <div class="grants__supheader">Социальные проекты</div>--}}
                {{--                        <a href="{{route('science.grants-more')}}" class="grants__header">Стипендиальная программа 2021-2022 «Молодые таланты» (Линия Б) от DAAD</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="grants">--}}
                {{--                    <div class="grants__date">До 15.01</div>--}}
                {{--                    <div class="grants__text">--}}
                {{--                        <div class="grants__supheader">Социальные проекты</div>--}}
                {{--                        <a href="{{route('science.grants-more')}}" class="grants__header">Стипендиальная программа 2021-2022 «Молодые таланты» (Линия Б) от DAAD</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>

        </div>
    </section>


    <!-- Pagination for contests -->
    <section class="section-four section-four_show">
        <div class="d_container">
            {{$contests->links('vendor.pagination.default')}}
{{--            <div class="pagination">--}}
{{--                --}}
{{--                <a href="#"> &laquo; </a>--}}
{{--                <a href="#" class="pagination_active">1</a>--}}
{{--                <a href="#">2</a>--}}
{{--                <a href="#">3</a>--}}
{{--                <a href="#">4</a>--}}
{{--                <a href="#">5</a>--}}
{{--                <span>...</span>--}}
{{--                <a href="#">32</a>--}}
{{--                <a href="#"> &raquo; </a>--}}
{{--            </div>--}}
        </div>
    </section>

    <!-- Pagination for grants -->
    <section class="section-four">
        <div class="d_container">
            <div class="pagination">
{{--                {{$grants->links('vendor.pagination.default')}}--}}
{{--                <a href="#"> &laquo; </a>--}}
{{--                <a href="#" class="pagination_active">1</a>--}}
{{--                <a href="#">2</a>--}}
{{--                <a href="#">3</a>--}}
{{--                <a href="#">4</a>--}}
{{--                <a href="#">5</a>--}}
{{--                <span>...</span>--}}
{{--                <a href="#">27</a>--}}
{{--                <a href="#"> &raquo; </a>--}}
            </div>
        </div>
    </section>

@endsection