@extends("users.layouts.index")

@section("content")

    <link href="{{asset('/css/projects.css')}}" rel="stylesheet">

    <section class="section-one">
        <div class="d_container">
            <h1 class="section-one__header">Проектная деятельность</h1>
        </div>
    </section>

    <div class="section-two">
        <div class="d_container">
            <div class="section-two__wrapper">
                <picture>
{{--                    <source srcset="{{asset('icons/science/banner-mob.jpg')}}" media="(max-width: 375px)">--}}
{{--                    <img src="{{asset('icons/science/banner.jpg')}}" alt="" class="project-img">--}}
                    @isset($banners['science_banner'],$banners['science_banner']->headImg,$banners['science_banner']->headImg->img)
                    <source srcset="{{asset($banners['science_banner']->smallImg->img->url)}}" media="(max-width: 375px)">
                    <img src="{{asset($banners['science_banner']->headImg->img->url)}}" alt="" class="project-img">
                    @endisset
                </picture>
            </div>
        </div>
    </div>

    <section class="section-three">
        <div class="d_container">
            <div class="section-three__wrapper">
                @foreach($projects as $key => $item)
                    <div class="project">
                        <a href="{{route('science.projects-more',['id'=>$item->id])}}">
                            <div class="project__inner">
                                <div class="project__subheader">{{$item->tag}}</div>
                                <div class="project__header">{{$item->title}}</div>
                            </div>
                            <img @if($item->headImg) src="{{$item->headImg->img->url}}" @endif alt="image" class="project__image">
                        </a>
                        <div class="project__more"><a href="{{route('science.projects-more',['id'=>$item->id])}}">Подробнее</a>
                        </div>
                    </div>
                @endforeach
                {{--                @foreach($projects as $key => $item)--}}
                {{--                    {{$key}}--}}
                {{--                @endforeach--}}

                {{--                <div class="project">--}}
                {{--                    <div class="project__subheader">Интервью</div>--}}
                {{--                    <div class="project__header">Как двое студентов создали приложение</div>--}}
                {{--                    <div class="project__more"><a href="{{route('science.projects-more')}}">Подробнее</a></div>--}}
                {{--                </div>--}}
                {{--                <div class="project">--}}
                {{--                    <div class="project__subheader">Интервью</div>--}}
                {{--                    <div class="project__header">Как двое студентов создали приложение</div>--}}
                {{--                    <div class="project__more"><a href="{{route('science.projects-more')}}">Подробнее</a></div>--}}
                {{--                </div>--}}
                {{--                <div class="project">--}}
                {{--                    <div class="project__subheader">Интервью</div>--}}
                {{--                    <div class="project__header">Как двое студентов создали приложение</div>--}}
                {{--                    <div class="project__more"><a href="{{route('science.projects-more')}}">Подробнее</a></div>--}}
                {{--                </div>--}}
                {{--                <div class="project">--}}
                {{--                    <div class="project__subheader">Интервью</div>--}}
                {{--                    <div class="project__header">Как двое студентов создали приложение</div>--}}
                {{--                    <div class="project__more"><a href="{{route('science.projects-more')}}">Подробнее</a></div>--}}
                {{--                </div>--}}
                {{--                <div class="project">--}}
                {{--                    <div class="project__subheader">Интервью</div>--}}
                {{--                    <div class="project__header">Как двое студентов создали приложение</div>--}}
                {{--                    <div class="project__more"><a href="{{route('science.projects-more')}}">Подробнее</a></div>--}}
                {{--                </div>--}}
                {{--                <div class="project">--}}
                {{--                    <div class="project__subheader">Интервью</div>--}}
                {{--                    <div class="project__header">Как двое студентов создали приложение</div>--}}
                {{--                    <div class="project__more"><a href="{{route('science.projects-more')}}">Подробнее</a></div>--}}
                {{--                </div>--}}
            </div>
        </div>
    </section>

    <section class="section-four">
        <div class="d_container">


            {{$projects->links('vendor.pagination.default')}}
            {{--                        <div class="pagination">--}}
            {{--                            <a href="#"> &laquo; </a>--}}
            {{--                            <a href="#" class="pagination_active">1</a>--}}
            {{--                            <a href="#">2</a>--}}
            {{--                            <a href="#">3</a>--}}
            {{--                            <a href="#">4</a>--}}
            {{--                            <a href="#">5</a>--}}
            {{--                            <span>...</span>--}}
            {{--                            <a href="#">32</a>--}}
            {{--                            <a href="#"> &raquo; </a>--}}
            {{--                        </div>--}}
        </div>
    </section>

@endsection