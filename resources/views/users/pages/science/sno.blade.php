@extends("users.layouts.index")

@section("content")

    <link rel="stylesheet" href="{{asset('/css/sno.css')}}">

    <section class="section-one">
        <div class="d_container">
            <div class="section-one__inner">
                <div class="section-one__text">
                    <div class="section-one__header">Научная деятельность</div>
                    <div class="section-one__subheader">Студенческое научное общество</div>
                </div>
                <img src="{{asset('icons/science/logo.png')}}" alt="" class="section-one__logo">
            </div>
        </div>
    </section>

    <section class="section-two">
        <div class="d_container">
            <div class="section-two__inner">
                <div class="section-two__block">
{{--                    Это добровольное общественное объединение студентов ДГУ, активно занимающихся научно-исследовательской деятельностью.--}}
                    {!! $pt['left_article']->value !!}
                </div>
                <div class="section-two__block">
{{--                    Целью существования СНО является повышение качества получения высшего образования студентов, повышение эффективности развития и использования творческого потенциала студентов в форме научно-исследовательской работы.--}}
                    {!! $pt['right_article']->value !!}

                </div>
            </div>
        </div>
    </section>

    <section class="section-three">
        <div class="d_container">
            <div class="section-three__inner">
                <div class="section-three__block">
                    <div class="section-three__header">Порядок определения рейтинга по научно-исследовательской деятельности студента ДГУ</div>
                    <div class="section-three__divider"></div>
                    <div class="section-three__description">Результаты всех видов научно-исследовательской деятельности студентов оцениваются рейтинговыми баллами. Максимальное количество их равно 100. Рейтинговая оценка научно-исследовательской деятельности студентов определяется суммированием баллов за выполнение следующих работ:

                    <br><br> • Публикация статьи в журнале, сборнике трудов российской, региональной, вузовской конференции - от 5 до 10 балла
                    <br> • Публикация тезисов статьи в сборнике трудов российской, региональной, вузовской конференции, депонирование статьи - от 3 до 5 баллов
                    <br> • Доклады на конференциях: внутривузовских, межвузовских, всероссийских и международных - от 3 до 8 баллов
                    <br> • Доклад на «Неделе студенческой науки» - 5 баллов
                    <br> • Участие в конкурсах грантов: внутривузовский, региональный,         всероссийский и международный - от 10 до 16 баллов
                    <br> • Участие в конкурсах НИРС: внутривузовский, региональный, всероссийский и международный - от 2 до 16 баллов
                    <br> • Участие в изготовлении демонстрационных материалов, наглядных и учебно-методических пособий и т.д. - 5 баллов
                    <br> • Получение патента, свидетельства на охрану интеллектуальной собственности - 15 баллов
                    <br> • Участие в вузовской, межвузовской, всероссийской олимпиадах - от 1 до 16 баллов
                    <br> • Внедрение результатов исследований в учебный, производственный процесс - от 3 до 4 баллов

                        <br> Дополнительно ознакомиться с деятельностью СМУ ДГУ можно на <a href="#">официальном
                            сайте</a>.
                    </div>
                </div>
                <div class="section-three__block">
                    <div class="section-three__header section-three__header_dop">Положения</div>
                    <div class="section-three__divider"></div>
                    <div class="section-three__description">При организации научно-исследовательской студенческой
                        деятельности факультет опирается на следующие положения:

                        <br>• добровольность участия студента в научно-исследовательской работе;
                        <br>• свободный выбор темы исследования и творческой группы;
                        <br>• тематическая преемственность в проведении научной работы
                        <br>• обеспечение возможности участия в научных проектах различного уровня, конференциях,
                        публикации результатов научно-исследовательской работы.

                        <br><br><br><a href="#">ПОЛОЖЕНИЕ О СТУДЕНЧЕСКОМ НАУЧНОМ ОБЩЕСТВЕ В ДАГЕСТАНСКОМ ГОСУДАРСТВЕННОМ
                            УНИВЕРСИТЕТЕ</a>
                    </div>
                    <div class="section-three__divider section-three__divider_dop"></div>
                    <div class="section-three__header">Как вступить в СНО?</div>
                    <div class="section-three__divider"></div>
                    <div class="section-three__description">Для того, чтобы вступить в СНО, достаточно <a href="#">оставить заявку</a>
                    или напрямую связаться с председателем СНО факультета.</div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-four">
        <div class="d_container">
            <div class="section-four__header">Структура СНО IT-факультета</div>

            <div class="section-four__cards cards">
                @foreach($servants as $key => $servant)
                <div class="section-four__card card">
                    <div class="card__img"><img @if($servant->headImg) src="{{asset($servant->headImg->img->url)}}" @endif alt="image"></div>
                    <div class="card__name">{{$servant->title}}</div>
                    <div class="card__description">{{$servant->work_type}}</div>
                </div>
                @endforeach
{{--                <div class="section-four__card card">--}}
{{--                    <div class="card__img"><img src="{{asset('icons/index/1.png')}}" alt="image"></div>--}}
{{--                    <div class="card__name">Сапият Магомедова</div>--}}
{{--                    <div class="card__description">Председатель СНО Дагестанского Государственного Университета</div>--}}
{{--                </div>--}}
{{--                <div class="section-four__card card">--}}
{{--                    <div class="card__img"><img src="{{asset('icons/index/1.png')}}" alt="image"></div>--}}
{{--                    <div class="card__name">Сапият Магомедова</div>--}}
{{--                    <div class="card__description">Председатель СНО Дагестанского Государственного Университета</div>--}}
{{--                </div>--}}
{{--                <div class="section-four__card card">--}}
{{--                    <div class="card__img"><img src="{{asset('icons/index/1.png')}}" alt="image"></div>--}}
{{--                    <div class="card__name">Сапият Магомедова</div>--}}
{{--                    <div class="card__description">Председатель СНО Дагестанского Государственного Университета</div>--}}
{{--                </div>--}}
            </div>
        </div>
    </section>

    <section class="section-five">
        <div class="d_container">
            <div class="section-five__header">Научные проекты, клубы и кружки</div>

            <div class="section-five__blocks">
                @foreach($clubs as $key => $club)
                <a href="#" class="section-five__block">
                    <div class="section-five__img">
                        <img @if($club->headImg) src="{{asset($club->headImg->img->url)}}" @endif alt="image">
                    </div>
                    <div class="section-five__description">{!!  $club->description !!}</div>
                </a>
                @endforeach
{{--                <a href="#" class="section-five__block">--}}
{{--                    <div class="section-five__img">--}}
{{--                        <img src="{{asset('icons/science/mugs-text.png')}}" alt="image">--}}
{{--                    </div>--}}
{{--                    <div class="section-five__description">«DSU talks» - это цикл научно-популярных лекций, мастер-классов и научных кружков по наиболее интересным молодежи современным научным направлениям. </div>--}}
{{--                </a>--}}
{{--                <a href="#" class="section-five__block">--}}
{{--                    <div class="section-five__img">--}}
{{--                        <img src="{{asset('icons/science/mugs-text.png')}}" alt="image">--}}
{{--                    </div>--}}
{{--                    <div class="section-five__description">«DSU talks» - это цикл научно-популярных лекций, мастер-классов и научных кружков по наиболее интересным молодежи современным научным направлениям. </div>--}}
{{--                </a>--}}
{{--                <a href="#" class="section-five__block">--}}
{{--                    <div class="section-five__img">--}}
{{--                        <img src="{{asset('icons/science/mugs-text.png')}}" alt="image">                        --}}
{{--                    </div>--}}
{{--                    <div class="section-five__description">«DSU talks» - это цикл научно-популярных лекций, мастер-классов и научных кружков по наиболее интересным молодежи современным научным направлениям. </div>--}}
{{--                </a>--}}
            </div>
            <div class="project-all circle-all">
                <span><a href="">Все проекты и кружки</a></span>
                <img src="{{asset('/icons/index/arrow.svg')}}" alt="" class="news__all-img">
            </div>
        </div>
    </section>

    <section class="project">
    <div class="container">
        <div class="project__title"><span>Проекты студентов</span></div>
        <div class="project__content">
            @foreach($projects as $key => $item)
            <div class="project__content-item">
                <img @if($item->headImg) src="{{$item->headImg->img->url}}" @endif alt="image" class="project__image">
                <div class="project__content-block">
                    <div class="project__type">{{$item->tag}}</div>
                    <div class="project__descr"><a href="{{route("science.projects-more",['id'=>$item->id])}}">{{$item->title}}</a></div>
                </div>
            </div>
            @endforeach

{{--            <div class="project__content-item second">--}}
{{--                <div class="project__info">--}}
{{--                    <div class="project__type">Интересное</div>--}}
{{--                    <div class="project__descr">Стартер пак студента айтишника</div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="project__content-item third">--}}
{{--                <div class="project__info">--}}
{{--                    <div class="project__type">Интересное</div>--}}
{{--                    <div class="project__descr">Стартер пак студента айтишника</div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="project__content-item fourth">--}}
{{--                <div class="project__info">--}}
{{--                    <div class="project__type">Интересное</div>--}}
{{--                    <div class="project__descr">Стартер пак студента айтишника</div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
    <div class="project-all">
            <span><a href="">Все проекты</a></span>
            <img src="{{asset('/icons/index/arrow.svg')}}" alt="" class="news__all-img">
    </div>
</section>

    <section class="section-seven">
        <div class="d_container">
            <div class="section-seven__header">Новости</div>

            <div class="section-seven__blocks">
                @foreach($news as $key => $item)
                <div class="section-seven__block">
                    <a href="{{route('news.news-more',['id'=>$item->id])}}" class="section-seven__img">
                        <img @if($item->headImg) src="{{asset($item->headImg->img->url)}}" @endif alt="image">
                    </a>
                    <div class="section-seven__text">
                        <div class="section-seven__subtext">
                            <div class="section-seven__date">{{$item->created_at->format("d.m.Y")}}</div>
                            <div class="section-seven__title">{{$item->title}}</div>
                            <div class="section-seven__subtitle">{{$item->tag}}</div>
                        </div>
                        <a href="{{route('news.news-more',['id'=>$item->id])}}" class="section-seven__link">Подробнее</a>
                    </div>
                </div>
                @endforeach
{{--                <div class="section-seven__block">--}}
{{--                    <a href="#" class="section-seven__img">--}}
{{--                        <img src="{{ asset('icons/news/bg.png') }}" alt="image">--}}
{{--                    </a>--}}
{{--                    <div class="section-seven__text">--}}
{{--                        <div class="section-seven__subtext">--}}
{{--                            <div class="section-seven__date">20.01.2021</div>--}}
{{--                            <div class="section-seven__title">Научная сессия ДГУ</div>--}}
{{--                            <div class="section-seven__subtitle">Научная деятельность</div>--}}
{{--                        </div>--}}
{{--                        <a href="#" class="section-seven__link">Подробнее</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="section-seven__block">--}}
{{--                    <a href="#" class="section-seven__img">--}}
{{--                        <img src="{{ asset('icons/news/bg.png') }}" alt="image">--}}
{{--                    </a>--}}
{{--                    <div class="section-seven__text">--}}
{{--                        <div class="section-seven__subtext">--}}
{{--                            <div class="section-seven__date">20.01.2021</div>--}}
{{--                            <div class="section-seven__title">Научная сессия ДГУ</div>--}}
{{--                            <div class="section-seven__subtitle">Научная деятельность</div>--}}
{{--                        </div>--}}
{{--                        <a href="#" class="section-seven__link">Подробнее</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="section-seven__block">--}}
{{--                    <a href="#" class="section-seven__img">--}}
{{--                        <img src="{{ asset('icons/news/bg.png') }}" alt="image">--}}
{{--                    </a>--}}
{{--                    <div class="section-seven__text">--}}
{{--                        <div class="section-seven__subtext">--}}
{{--                            <div class="section-seven__date">20.01.2021</div>--}}
{{--                            <div class="section-seven__title">Научная сессия ДГУ</div>--}}
{{--                            <div class="section-seven__subtitle">Научная деятельность</div>--}}
{{--                        </div>--}}
{{--                        <a href="#" class="section-seven__link">Подробнее</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
            <div class="project-all news-all">
                <span><a href="">Все новости</a></span>
                <img src="{{asset('/icons/index/arrow.svg')}}" alt="" class="news__all-img">
            </div>
        </div>
    </section>
    
    <script src="{{asset('js/science.js')}}"></script>
@endsection