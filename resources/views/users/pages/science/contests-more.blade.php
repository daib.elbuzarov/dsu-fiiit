@extends("users.layouts.index")



@section("content")

    <link href="{{asset('/css/contests-more.css')}}" rel="stylesheet">

    <section class="section-one">
        <div class="d_container">
            <h1 class="section-one__header">Научная деятельность</h1>
            <h2 class="section-one__subheader">Конкурсы и гранты</h2>
        </div>
    </section>

    <section class="section-two">
        <div class="d_container">
            <div class="section-two__tabs">
                <a href="{{route('science.contests')}}" class="section-two__tab section-two__tab_active">Конкурсы</a>
                <a href="{{route('science.contests')}}" class="section-two__tab">Гранты</a>
            </div>
            <div class="section-two__divider"></div>
        </div>
    </section>
    <div class="section-three">
        <div class="d_container">
            <div class="section-three__supheader">{{$contest->tag}}</div>
            <h2 class="section-three__header">{{$contest->title}}</h2>
            <div class="section-three__descriptions">
                <div class="description">
                    <div class="description__title">Организатор:</div>
                    <div class="description__text">{{$contest->author}}</div>
                </div>
                <div class="description">
                    <div class="description__title">Сайт конкурса:</div>
                    <div class="description__text"><a target="_blank" href="{{$contest->url}}">{{$contest->url}}</a></div>
                </div>
                <div class="description">
                    <div class="description__title">Призовой фонд:</div>
                    <div class="description__text">
                        {{$contest->prize}}
                    </div>
                </div>
                <div class="description">
                    <div class="description__title">Подача заявок до:</div>
                    <div class="description__text">{{\Carbon\Carbon::createFromDate($contest->date_end)->format('d.m.Y')}}</div>
                </div>
                <div class="description">
                    <div class="description__title">Описание:</div>
                    <div class="description__text">
                        {!! $contest->description !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--    <div class="section-three">--}}
    {{--        <div class="d_container">--}}
    {{--            <div class="section-three__supheader">Социальные проекты</div>--}}
    {{--            <h2 class="section-three__header">Конкурс социальных проектов «Инносоциум»</h2>--}}
    {{--            <div class="section-three__descriptions">--}}
    {{--                <div class="description">--}}
    {{--                    <div class="description__title">Организатор:</div>--}}
    {{--                    <div class="description__text">Фонд Инносоциум</div>--}}
    {{--                </div>--}}
    {{--                <div class="description">--}}
    {{--                    <div class="description__title">Сайт конкурса:</div>--}}
    {{--                    <div class="description__text"><a href="#">https://innosocium.ru/</a></div>--}}
    {{--                </div>--}}
    {{--                <div class="description">--}}
    {{--                    <div class="description__title">Призовой фонд: </div>--}}
    {{--                    <div class="description__text">Победители награждаются премией в размере 300 000 рублей.--}}
    {{--                    Победителю в каждом тематическом направлении присваивается звание Победителя Всероссийского конкурса социальных проектов «Инносоциум» с вручением соответствующего диплома. Участники, чьи заявки стали финалистами, получают Дипломы.</div>--}}
    {{--                </div>--}}
    {{--                <div class="description">--}}
    {{--                    <div class="description__title">Подача заявок до:</div>--}}
    {{--                    <div class="description__text">15.02.2020</div>--}}
    {{--                </div>--}}
    {{--                <div class="description">--}}
    {{--                    <div class="description__title">Описание:</div>--}}
    {{--                    <div class="description__text">К участию приглашаются студенты и студенческие коллективы, сформированные из действительных студентов российских ВУЗов.--}}

    {{--                        <p>Принимаются проекты в направлениях:</p>--}}

    {{--                        <p>— Женское лидерство (мотивация женщин к карьерному росту, преодоление внутренних барьеров; популяризация философии и успешных историй женского лидерства; развитие женского лидерского потенциала при помощи программ, направленных на профессиональное развитие и рост (менторинг, коучинг, информационный обмен, ролевое моделирование); обмен лучшими практиками, проведение исследований по тематике женского лидерства)</p>--}}


    {{--                        <p>— Поддержка семьи и семейных ценностей (содействие образованию, в том числе дополнительному образованию, мам и детей из уязвимых групп; «здоровая семья»: проекты, направленные на развитие здорового образа жизни всей семьи, включая здоровое питание, физкультуру и спорт, профилактику заболеваний, медосмотры и т.д. семейный досуг: проекты, направленные на расширение возможностей семейного отдыха, включая развитие внутреннего туризма, кружки и секции для представителей разных поколений семьи)</p>--}}


    {{--                        <p>— Активное долголетие (повышение качества жизни людей пожилого возраста; содействие дополнительному образованию, социализации и занятости людей пожилого возраста;--}}
    {{--                    повышение социальной активности людей пожилого возраста, вовлечение их в полноценную жизнь; расширение возможностей участия граждан старшего поколения в культурных, образовательных, физкультурных, оздоровительных и других мероприятиях)</p></div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}


@endsection