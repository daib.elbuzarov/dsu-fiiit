@extends("users.layouts.index")

@section("content")

    <link rel="stylesheet" href="{{asset('css/mugs.css')}}">

    <section class="section-one">
        <div class="d_container">
            <h1 class="section-one__header">Научная деятельность</h1>
            <h2 class="section-one__subheader">Научные проекты и кружки</h2>

            <div class="section-one__cards">
                @foreach($clubs as $key => $item)
                    <div class="card">
                        <div class="card__suptitle">{{$item->type->name}}</div>
                        <img src="@if($item->headImg){{ asset($item->headImg->img->url) }}@endif" alt="img">

                        <div class="card__description">{!! $item->description !!}</div>

                        <div class="card__founder-title">ОСНОВАТЕЛЬ</div>
                        <div class="card__founder">{{$item->author}}</div>
                    </div>
                @endforeach
                {{--            <div class="card">--}}
                {{--                <div class="card__suptitle">Проект</div>--}}
                {{--                <img src="{{ asset('icons/science/mugs-text.png') }}" alt="img">--}}

                {{--                <div class="card__description">«DSU talks» - это цикл научно-популярных лекций, мастер-классов и научных кружков по наиболее интересным молодежи современным научным направлениям. </div>--}}

                {{--                <div class="card__founder-title">ОСНОВАТЕЛЬ</div>--}}
                {{--                <div class="card__founder">Студенческое научное общество ДГУ</div>--}}
                {{--            </div>--}}
                {{--            <div class="card">--}}
                {{--                <div class="card__suptitle">Проект</div>--}}
                {{--                <img src="{{ asset('icons/science/mugs-text.png') }}" alt="img">--}}

                {{--                <div class="card__description">«DSU talks» - это цикл научно-популярных лекций, мастер-классов и научных кружков по наиболее интересным молодежи современным научным направлениям. </div>--}}

                {{--                <div class="card__founder-title">ОСНОВАТЕЛЬ</div>--}}
                {{--                <div class="card__founder">Студенческое научное общество ДГУ</div>--}}
                {{--            </div>--}}
                {{--            <div class="card">--}}
                {{--                <div class="card__suptitle">Проект</div>--}}
                {{--                <img src="{{ asset('icons/science/mugs-text.png') }}" alt="img">--}}

                {{--                <div class="card__description">«DSU talks» - это цикл научно-популярных лекций, мастер-классов и научных кружков по наиболее интересным молодежи современным научным направлениям. </div>--}}

                {{--                <div class="card__founder-title">ОСНОВАТЕЛЬ</div>--}}
                {{--                <div class="card__founder">Студенческое научное общество ДГУ</div>--}}
                {{--            </div>--}}
                {{--            <div class="card">--}}
                {{--                <div class="card__suptitle">Проект</div>--}}
                {{--                <img src="{{ asset('icons/science/mugs-text.png') }}" alt="img">--}}

                {{--                <div class="card__description">«DSU talks» - это цикл научно-популярных лекций, мастер-классов и научных кружков по наиболее интересным молодежи современным научным направлениям. </div>--}}

                {{--                <div class="card__founder-title">ОСНОВАТЕЛЬ</div>--}}
                {{--                <div class="card__founder">Студенческое научное общество ДГУ</div>--}}
                {{--            </div>--}}
                {{--            <div class="card">--}}
                {{--                <div class="card__suptitle">Проект</div>--}}
                {{--                <img src="{{ asset('icons/science/mugs-text.png') }}" alt="img">--}}

                {{--                <div class="card__description">«DSU talks» - это цикл научно-популярных лекций, мастер-классов и научных кружков по наиболее интересным молодежи современным научным направлениям. </div>--}}

                {{--                <div class="card__founder-title">ОСНОВАТЕЛЬ</div>--}}
                {{--                <div class="card__founder">Студенческое научное общество ДГУ</div>--}}
                {{--            </div>--}}
                {{--            <div class="card">--}}
                {{--                <div class="card__suptitle">Проект</div>--}}
                {{--                <img src="{{ asset('icons/science/mugs-text.png') }}" alt="img">--}}

                {{--                <div class="card__description">«DSU talks» - это цикл научно-популярных лекций, мастер-классов и научных кружков по наиболее интересным молодежи современным научным направлениям. </div>--}}

                {{--                <div class="card__founder-title">ОСНОВАТЕЛЬ</div>--}}
                {{--                <div class="card__founder">Студенческое научное общество ДГУ</div>--}}
                {{--            </div>--}}
                {{--            <div class="card">--}}
                {{--                <div class="card__suptitle">Проект</div>--}}
                {{--                <img src="{{ asset('icons/science/mugs-text.png') }}" alt="img">--}}

                {{--                <div class="card__description">«DSU talks» - это цикл научно-популярных лекций, мастер-классов и научных кружков по наиболее интересным молодежи современным научным направлениям. </div>--}}

                {{--                <div class="card__founder-title">ОСНОВАТЕЛЬ</div>--}}
                {{--                <div class="card__founder">Студенческое научное общество ДГУ</div>--}}
                {{--            </div>--}}
                {{--            <div class="card">--}}
                {{--                <div class="card__suptitle">Проект</div>--}}
                {{--                <img src="{{ asset('icons/science/mugs-text.png') }}" alt="img">--}}

                {{--                <div class="card__description">«DSU talks» - это цикл научно-популярных лекций, мастер-классов и научных кружков по наиболее интересным молодежи современным научным направлениям. </div>--}}

                {{--                <div class="card__founder-title">ОСНОВАТЕЛЬ</div>--}}
                {{--                <div class="card__founder">Студенческое научное общество ДГУ</div>--}}
                {{--            </div>--}}
                {{--            <div class="card">--}}
                {{--                <div class="card__suptitle">Проект</div>--}}
                {{--                <img src="{{ asset('icons/science/mugs-text.png') }}" alt="img">--}}

                {{--                <div class="card__description">«DSU talks» - это цикл научно-популярных лекций, мастер-классов и научных кружков по наиболее интересным молодежи современным научным направлениям. </div>--}}

                {{--                <div class="card__founder-title">ОСНОВАТЕЛЬ</div>--}}
                {{--                <div class="card__founder">Студенческое научное общество ДГУ</div>--}}
                {{--            </div>--}}
            </div>
        </div>
    </section>

    <section class="section-two">
        <div class="d_container">
            {{$clubs->links('vendor.pagination.default')}}

            {{--            <div class="pagination">--}}
{{--                <a href="#"> &laquo; </a>--}}
{{--                <a href="#" class="pagination_active">1</a>--}}
{{--                <a href="#">2</a>--}}
{{--                <a href="#">3</a>--}}
{{--                <a href="#">4</a>--}}
{{--                <a href="#">5</a>--}}
{{--                <span>...</span>--}}
{{--                <a href="#">32</a>--}}
{{--                <a href="#"> &raquo; </a>--}}
{{--            </div>--}}
        </div>
    </section>
@endsection