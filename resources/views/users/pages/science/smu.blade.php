@extends("users.layouts.index")

@section("content")

    <link rel="stylesheet" href="{{asset('/css/smu.css')}}">

    <section class="section-one">
        <div class="d_container">
            <div id="test123" class="section-one__header">Научная деятельность</div>
            <div class="section-one__subheader">Совет молодых ученых</div>
        </div>
    </section>

    <section class="section-two">
        <div class="d_container">
            <div class="section-two__inner">
                <div class="section-two__block">
{{--                    Совет молодых ученых Дагестанского государственного университета (СМУ--}}
{{--                    ДГУ) представляет интересы студентов, аспирантов и преподавателей ДГУ –--}}
{{--                    кандидатов и докторов наук в возрасте до 40 лет в сфере их научной деятельности.--}}
                    {!! $pt['left_article']->value !!}

                </div>
                <div class="section-two__block">
{{--                    Деятельность осуществляется в соответствии с федеральным законом «Об--}}
{{--                    образовании в РФ», Уставом ДГУ и на основании <a href="#">Положения</a>.--}}
                    {!! $pt['right_article']->value !!}

                </div>
            </div>
        </div>
    </section>

    <section class="section-three">
        <div class="d_container">
            <div class="section-three__inner">
                <div class="section-three__block">
                    <div class="section-three__header">Направления деятельности СМУ ДГУ</div>
                    <div class="section-three__divider"></div>
                    <div class="section-three__description">СМУ осуществляет деятельность по следующим направлениям:

                        <br> • Участие в организации конкурса на лучшую научную работу студентов по естественным и
                        гуманитарным наукам в ДГУ;
                        <br> • Участие в организации конкурса грантов аспирантов ДГУ;
                        <br> • Организия сбора и рецензирования научных статей студентов, аспирантов и молодых
                        кандидатов наук для их публикации в ежегодном сборнике «Труды молодых ученых ДГУ»;
                        <br> • Организия работы по консультированию молодых ученых ДГУ по вопросам выбора форм и методов
                        организации и правового обеспечения научной деятельности;
                        <br> • Координация работы студенческих научных обществ, научно-студенческих кружков, научных
                        семинаров молодых ученых ДГУ, оказывание им методической помощи и предлажение форм их работы;
                        <br> • Содействие поиску и установлению контактов с руководителями организаций, заинтересованных
                        в использовании результатов научных исследований молодых ученых ДГУ (по согласованию с Ученым
                        советом ДГУ);

                        <br> Дополнительно ознакомиться с деятельностью СМУ ДГУ можно на <a href="#">официальном
                            сайте</a>.
                    </div>
                </div>
                <div class="section-three__block">
                    <div class="section-three__header">Положения</div>
                    <div class="section-three__divider"></div>
                    <div class="section-three__description">При организации научно-исследовательской студенческой
                        деятельности факультет опирается на следующие положения:

                        <br>• добровольность участия студента в научно-исследовательской работе;
                        <br>• свободный выбор темы исследования и творческой группы;
                        <br>• тематическая преемственность в проведении научной работы
                        <br>• обеспечение возможности участия в научных проектах различного уровня, конференциях,
                        публикации результатов научно-исследовательской работы.

                        <br><br><br><a href="#">ПОЛОЖЕНИЕ О СТУДЕНЧЕСКОМ НАУЧНОМ ОБЩЕСТВЕ В ДАГЕСТАНСКОМ ГОСУДАРСТВЕННОМ
                            УНИВЕРСИТЕТЕ</a></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-five">
        <div class="d_container">
            <div class="section-five__header">сОСТАВ СОВЕТА СМУ ФИИИТ</div>
            <div class="section-five__cards">
                @foreach($servants as $key => $servant)

                    <div class="section-four__cards cards">
                        <div class="section-four__card card">
                            <div class="card__img"><img
                                        @if($servant->headImg) src="{{asset($servant->headImg->img->url)}} "
                                        @endif alt=""></div>
                            <div class="card__name">{{$servant->title}}</div>
                            <div class="card__description">{{$servant->work_type}}</div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </section>

    {{--    <section class="section-four">--}}
    {{--        <div class="d_container">--}}
    {{--            <div class="section-four__header">CОСТАВ СОВЕТА СМУ ФИИИТ</div>--}}

    {{--            <div class="section-four__cards cards">--}}
    {{--                <div class="section-four__card card">--}}
    {{--                    <div class="card__img"><img src="{{asset('icons/smu/image.png')}}" alt="" ></div>--}}
    {{--                    <div class="card__name">Касимова Таиса Маллаевна</div>--}}
    {{--                    <div class="card__description">Заместитель председателя СМУ Факультета Информатики и IT-технологий</div>--}}
    {{--                </div>--}}
    {{--                <div class="section-four__card card">--}}
    {{--                    <div class="card__img"><img src="{{asset('icons/smu/image.png')}}" alt="" ></div>--}}
    {{--                    <div class="card__name">Касимова Таиса Маллаевна</div>--}}
    {{--                    <div class="card__description">Заместитель председателя СМУ Факультета Информатики и IT-технологий</div>--}}
    {{--                </div>--}}
    {{--            </div>--}}

    {{--                <div class="section-four__card card">--}}
    {{--                    <div class="card__img"><img src="{{asset('icons/smu/image.png')}}" alt="" ></div>--}}
    {{--                    <div class="card__name">Касимова Таиса Маллаевна</div>--}}
    {{--                    <div class="card__description">Заместитель председателя СМУ Факультета Информатики и IT-технологий</div>--}}
    {{--                </div>--}}
    {{--            @if($key % 4 ==0)  </div>@endif--}}


    {{--            <div class="card2">--}}
    {{--                <div class="card__person person6">--}}
    {{--                    <div class="name__person6">Амалия Рамазанова</div>--}}
    {{--                    <div class="post__person6">Председатель СНО--}}
    {{--                        Факультета Информатики и IT-технологий--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <div class="card2">--}}
    {{--                <div class="card__person person4"></div>--}}
    {{--                <div class="name__person4">Фарида Велиева</div>--}}
    {{--                <div class="post__person4">Председатель СНО--}}
    {{--                    Дагестанского Государственного--}}
    {{--                    Университета--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <div class="card2">--}}
    {{--                <div class="card__person person5"></div>--}}
    {{--                <div class="name__person5">Насрулла <br>--}}
    {{--                    Курбанмагомедович--}}
    {{--                </div>--}}
    {{--                <div class="post__person5">Заместитель декана по научной работе Факультета--}}
    {{--                    Информатики и IT-технологий--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}

    {{--    <section class="section-five">--}}
    {{--        <div class="d_container">--}}
    {{--            <div class="section-five__cards">--}}
    {{--                @foreach($servants as $key => $servant)--}}
    {{--                <div class="section-five__card">--}}
    {{--                    <img @if($servant->headImg)src="{{asset($servant->headImg->img->url)}}" @endif alt="" class="section-five__img">--}}
    {{--                    <div class="section-five__name">Лачинов Нариман Завурович</div>--}}
    {{--                </div>--}}
    {{--                @endforeach--}}
    {{--                <div class="section-five__card">--}}
    {{--                    <img src="{{asset('icons/smu/image.png')}}" alt="" class="section-five__img">--}}
    {{--                    <div class="section-five__name">Лачинов Нариман Завурович</div>--}}
    {{--                </div>--}}
    {{--                <div class="section-five__card">--}}
    {{--                    <img src="{{asset('icons/smu/image.png')}}" alt="" class="section-five__img">--}}
    {{--                    <div class="section-five__name">Лачинов Нариман Завурович</div>--}}
    {{--                </div>--}}
    {{--                <div class="section-five__card">--}}
    {{--                    <img src="{{asset('icons/smu/image.png')}}" alt="" class="section-five__img">--}}
    {{--                    <div class="section-five__name">Лачинов Нариман Завурович</div>--}}
    {{--                </div>--}}
    {{--                <div class="section-five__card">--}}
    {{--                    <img src="{{asset('icons/smu/image.png')}}" alt="" class="section-five__img">--}}
    {{--                    <div class="section-five__name">Лачинов Нариман Завурович</div>--}}
    {{--                </div>--}}
    {{--                <div class="section-five__card">--}}
    {{--                    <img src="{{asset('icons/smu/image.png')}}" alt="" class="section-five__img">--}}
    {{--                    <div class="section-five__name">Лачинов Нариман Завурович</div>--}}
    {{--                </div>--}}
    {{--                <div class="section-five__card">--}}
    {{--                    <img src="{{asset('icons/smu/image.png')}}" alt="" class="section-five__img">--}}
    {{--                    <div class="section-five__name">Лачинов Нариман Завурович</div>--}}
    {{--                </div>--}}
    {{--                <div class="section-five__card">--}}
    {{--                    <img src="{{asset('icons/smu/image.png')}}" alt="" class="section-five__img">--}}
    {{--                    <div class="section-five__name">Лачинов Нариман Завурович</div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}


    <script src="{{asset('js/science.js')}}"></script>
@endsection