@extends("users.layouts.index")

@section("content")

<link href="{{asset('/css/projects-more.css')}}" rel="stylesheet">

<section class="section-one">
    <div class="d_container">
        <h1 class="section-one__header">Проектная деятельность</h1>
    </div>
</section>
<section class="section-two">
    <div class="d_container">
        <div class="section-two__subheader"><span class="section-two__date">{{$project->created_at->format("d.m.Y")}}</span>{{$project->tag}}</div>
        <div class="section-two__header">{{$project->title}}</div>
        <div class="section-two__text">
           {!! $project->description !!}
        </div>
        <div class="section-two__images">
            <div class="section-two__image">
                <img @if($project->headImg) src="{{asset($project->headImg->img->url)}}" @endif alt="image">
            </div>
            
            @foreach($project->sliderImg as $key => $join)
                <div class="section-two__image">
                    <img @if($join) src="{{asset($join->img->url)}}" @endif alt="image">
                </div>
            @endforeach
        </div>
    </div>
</section>
{{--<section class="section-two">--}}
{{--    <div class="d_container">--}}
{{--        <div class="section-two__subheader"><span class="section-two__date">20.01.2021</span>Интервью</div>--}}
{{--        <div class="section-two__header">Как двое студентов создали приложение</div>--}}
{{--        <div class="section-two__text">--}}
{{--            <p>21 ноября состоялась встреча учащихся 42 школы г. Махачкалы с заведующим кафедрой прикладной информатики, к.э.н, доцентом ДГУ Камиловым Камилем Башировичем и заместителем декана факультета информатики и информационных технологий Ахмедовой Написат Мурадовной.</p>--}}
{{--    ⠀--}}
{{--            <p>Преподаватели посетили пятый профильный класс, послушали идеи совсем юных программистов, поделились с ними своим опытом, рассказали про факультет информатики и информационных технологий, как он поможет решать их задачи, и реализовывать их идеи, которые пока им кажутся только мечтами. Рассказали про Яндекс.Лицей на базе ДГУ.</p>--}}
{{--    ⠀--}}
{{--            <p>Конечно, не оставили в стороне и выпускные классы. Рассказали выпускникам про все направления факультета, показали им реальные проекты наших выпускников и студентов, ответили на вопросы школьников и поделились советами как легко подготовиться к ЕГЭ по информатике.</p>--}}
{{--    ⠀--}}
{{--            <p>Насколько продуктивной была эта встреча, покажет время.</p>--}}
{{--        </div>--}}
{{--        <div class="section-two__images">--}}
{{--            <img src="{{asset('/icons/news/bg.png')}}" alt="image" class="section-two__image">--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}

@endsection