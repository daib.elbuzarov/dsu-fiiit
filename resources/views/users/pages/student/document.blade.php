@extends("users.layouts.index")
@section("content")
    <section class="document">
        <div class="container">
            <div class="document__title">Образцы документов</div>
            <div class="document__content">
                @foreach($sections as $key => $section)
                    <div class="document__block">
                        <div class="document__block-title">{{$section->title}}</div>
                        <hr class="document__block-line">
                        <ul class="document__block-list">

                            @foreach($section->documents as $key2 => $document)
                                <li>
                                @if($document->url)<a href="{{$document->url}}" target="_blank" class="document__block-link">{{$document->title}}</a>@endif
                                    @if($document->file)<a href="{{asset($document->file)}}" target="_blank" class="document__block-link">{{$document->title}}</a>@endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach
                {{--                    <div class="document__block">--}}
                {{--                        <div class="document__block-title">Образцы заявлений</div>--}}
                {{--                        <hr class="document__block-line">--}}
                {{--                        <ul class="document__block-list">--}}
                {{--                            <li><a class="document__block-link">Положение о материальном обеспечении</a></li>--}}
                {{--                            <li><a class="document__block-link">Заявление на материнскую помощь</a></li>--}}
                {{--                            <li><a class="document__block-link">Положение о стипендиальном обеспечении</a></li>--}}
                {{--                            <li><a class="document__block-link">Заявление на социальную стипендию</a></li>--}}
                {{--                            <li><a class="document__block-link">Заявление на повышенную социальную стипендию</a></li>--}}
                {{--                            <li><a class="document__block-link">Анкета для назначения повышенной стипендии</a></li>--}}
                {{--                        </ul>--}}
                {{--                    </div>--}}

                {{--                <div class="document__block">--}}
                {{--                    <div class="document__block-title">Написание квалификационных работ</div>--}}
                {{--                    <hr class="document__block-line">--}}
                {{--                    <ul class="document__block-list">--}}
                {{--                        <li><a class="document__block-link">Требования к оформлению курсовых и дипломных работ</a></li>--}}
                {{--                        <li><a class="document__block-link">Критерии оценки курсовой работы</a></li>--}}
                {{--                    </ul>--}}
                {{--                </div>--}}

                {{--                <div class="document__block">--}}
                {{--                    <div class="document__block-title">Образцы заявлений на именную стипендию</div>--}}
                {{--                    <hr class="document__block-line">--}}
                {{--                    <ul class="document__block-list">--}}
                {{--                        <li><a class="document__block-link">Перечень документов, необходимых для участия в отборе </a></li>--}}
                {{--                        <li><a class="document__block-link">Образцы документов (список публикаций, характеристика, перечень документов, удостоверяющих участие в творческих и научных конкурсах)</a></li>--}}
                {{--                    </ul>--}}
                {{--                </div>--}}
            </div>
        </div>
    </section>
@endsection