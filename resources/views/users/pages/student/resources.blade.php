@extends("users.layouts.index")
@section("content")
        <div class="main__title">
            <div class="container">
                <h2 class="student__title">Учебные ресурсы</h2>
            </div>
        </div>
        <div class="resources__content">
            <div class="container">
                <div class="resources__education">
                    <div class="resources__education-lessons">
                        <div class="resources__block-title">Эллектронно-образовательные ресурсы</div>
                        <div class="resources__education-block">
                            <div class="resources__block-bachelor tablet-screen">
                                <p class="resources__block-text">
                                    {!! $pt['eor_info']->value !!}
                                </p>
                            </div>

                            <div class="resources__block-magistr">
                                <p class="resources__block-text">
                                    {!! $pt['eor_link']->value !!}
                                </p>
                            </div>
                        </div>

                        <div class="resources__block-title">Научная библиотека</div>
                        <div class="resources__education-block">

                            <div class="resources__block-bachelor">
                                <p class="resources__block-text">
                                {!! $pt['lib_info']->value !!}
                                </p>
                            </div>

                            <div class="resources__block-magistr">
                                <p class="resources__block-text">
                                    {!! $pt['lib_link']->value !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection