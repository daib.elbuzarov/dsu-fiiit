@extends("users.layouts.index")
@section("content")
        <div class="main__title">
            <div class="container">
                <h2 class="student__title">Расписание</h2>
            </div>
        </div>
        <section class="main-timetable">
            <div class="container">
            <div class="timetable__content">
                <div class="timetable__titles">
                    <div class="timetable__titles-lessons">Занятия</div>
                    <div class="timetable__titles-session">Сессии</div>
                </div>
                <div class="timetable__black"></div>
                <hr class="timetable__titles-line">

                <div class="timetable__education">
                    <div class="timetable__education-lessons">
                        <div class="timetable__block-title">Студентам очной формы обучения</div>

                        <div class="timetable__education-block">

                            <div class="timetable__block-bachelor height">
                                <span>Бакалавриат, очная форма:</span>
                                @foreach($ln_full_bc as $key => $item)
                                @if($item->url)<a class="timetable__link" href="{{$item->url}}" target="_blank">{{$item->title}}</a><br>@endif
                                @if($item->file)<a class="timetable__link" href="{{asset($item->file)}}" target="_blank">{{$item->title}}</a><br>@endif
                                @endforeach
    {{--                            <a class="timetable__link" href="">Расписание занятий, II неделя</a> --}}
                            </div>


                            <div class="timetable__block-magistr">
                               <span> Магистратура, очная форма:</span><br>
    {{--                            <br> <a class="timetable__link" href="">Расписание занятий</a> --}}
                                @foreach($ln_full_mt as $key => $item)
                                    @if($item->url)<a class="timetable__link" href="{{$item->url}}" target="_blank">{{$item->title}}</a><br>@endif
                                    @if($item->file)<a class="timetable__link" target="_blank" href="{{asset($item->file)}}">{{$item->title}}</a><br>@endif
                                @endforeach
                            </div>
                        </div>


                        <div class="timetable__block-title">Студентам заочной формы обучения</div>
                        <div class="timetable__education-block">

                            <div class="timetable__block-bachelor">
                            <span>Бакалавриат, заочная форма:</span><br>
    {{--                            <br> <a class="timetable__link" href="">Расписание занятий</a><br>--}}
                                @foreach($ln_dist_bc as $key => $item)
                                    @if($item->url)<a class="timetable__link" href="{{$item->url}}" target="_blank">{{$item->title}}</a>@endif
                                    @if($item->file)<a class="timetable__link" target="_blank" href="{{asset($item->file)}}">{{$item->title}}</a>@endif
                                @endforeach
                            </div>

                            <div class="timetable__block-magistr">
                            <span> Магистратура, заочная форма:</span><br>
    {{--                            <br> <a class="timetable__link" href="">Расписание занятий</a> --}}
                                @foreach($ln_dist_mt as $key => $item)
                                    @if($item->url)<a class="timetable__link" href="{{$item->url}}" target="_blank">{{$item->title}}</a>@endif
                                    @if($item->file)<a class="timetable__link" target="_blank" href="{{asset($item->file)}}">{{$item->title}}</a>@endif
                                @endforeach
                            </div>
                        </div>

                        <div class="timetable__block-title">Студентам очно-заочной формы обучения</div>
                        <div class="timetable__education-block">

                            <div class="timetable__block-bachelor">
                            <span>Бакалавриат, очно-заочная форма:</span><br>
    {{--                            <br> <a class="timetable__link" href="">Расписание занятий</a><br>--}}
                                @foreach($ln_half_bc as $key => $item)
                                    @if($item->url)<a class="timetable__link" href="{{$item->url}}" target="_blank">{{$item->title}}</a>@endif
                                    @if($item->file)<a class="timetable__link" target="_blank" href="{{asset($item->file)}}">{{$item->title}}</a>@endif
                                @endforeach
                            </div>

                            <div class="timetable__block-magistr">
                            <span> Магистратура, очно-заочная форма:</span><br>
    {{--                            <br> <a class="timetable__link" href="">Расписание занятий</a> --}}
                                @foreach($ln_half_mt as $key => $item)
                                    @if($item->url)<a class="timetable__link" href="{{$item->url}}" target="_blank">{{$item->title}}</a>@endif
                                    @if($item->file)<a class="timetable__link" target="_blank" href="{{asset($item->file)}}">{{$item->title}}</a>@endif
                                @endforeach
                            </div>
                        </div>

                        <div class="timetable__block-title">Недели</div>

                        <div class="timetable__education-block">
                            <div class="timetable__block-weeks">
                                <img src="@isset($weeks,$weeks->url) {{asset($weeks->url)}} @else {{asset('/icons/index/weeks.svg')}} @endisset" alt="" class="timetable__weeks-img">
                            </div>
                        </div>
                    </div>

                    <div class="timetable__education-session" style="display:none">
                        <div class="timetable__block-title">Студентам очной формы обучения</div>
                        <div class="timetable__education-block">
                            <div class="timetable__block-bachelor height">
                            <span>Бакалавриат, очная форма:</span><br>
    {{--                            <a class="timetable__link" href="">Зимняя зачетно-экзаменнационная сессия, 2020-2021</a> --}}
                                @foreach($exam_full_bc as $key => $item)
                                    @if($item->url)<a class="timetable__link" href="{{$item->url}}" target="_blank">{{$item->title}}</a><br>@endif
                                    @if($item->file)<a class="timetable__link" download="" href="{{asset($item->file)}}">{{$item->title}}</a><br>@endif
                                @endforeach
                            </div>

                            <div class="timetable__block-magistr height">
                            <span> Магистратура, очная форма:</span><br>
    {{--                            <a class="timetable__link" href="">Зимняя зачетно-экзаменнационная сессия, 2020-2021</a> --}}
                                @foreach($exam_full_mt as $key => $item)
                                    @if($item->url)<a class="timetable__link" href="{{$item->url}}" target="_blank">{{$item->title}}</a>@endif
                                    @if($item->file)<a class="timetable__link" target="_blank" href="{{asset($item->file)}}">{{$item->title}}</a>@endif
                                @endforeach
                            </div>
                        </div>

                        <div class="timetable__block-title">Студентам заочной формы обучения</div>
                        <div class="timetable__education-block">

                            <div class="timetable__block-bachelor height">
                                <span>Бакалавриат, заочная форма:</span><br>
    {{--                            <a class="timetable__link" href="">Зимняя зачетно-экзаменнационная сессия, 2020-2021</a><br>--}}
                                @foreach($exam_dist_bc as $key => $item)
                                    @if($item->url)<a class="timetable__link" href="{{$item->url}}" target="_blank">{{$item->title}}</a><br>@endif
                                    @if($item->file)<a class="timetable__link" target="_blank" href="{{asset($item->file)}}">{{$item->title}}</a><br>@endif
                                @endforeach
                            </div>

                            <div class="timetable__block-magistr height">
                            <span> Магистратура, заочная форма:</span><br>
    {{--                            <a class="timetable__link" href="">Зимняя зачетно-экзаменнационная сессия, 2020-2021</a> --}}
                                @foreach($exam_dist_mt as $key => $item)
                                    @if($item->url)<a class="timetable__link" href="{{$item->url}}" target="_blank">{{$item->title}}</a><br>@endif
                                    @if($item->file)<a class="timetable__link" target="_blank" href="{{asset($item->file)}}">{{$item->title}}</a><br>@endif
                                @endforeach
                            </div>
                        </div>

                        <div class="timetable__block-title">Студентам очно-заочной формы обучения</div>
                        <div class="timetable__education-block">
                            <div class="timetable__block-bachelor height">
                            <span>Бакалавриат, очно-заочная форма:</span><br>
    {{--                            <a class="timetable__link" href="">Зимняя зачетно-экзаменнационная сессия, 2020-2021</a> --}}
                                @foreach($exam_half_bc as $key => $item)
                                    @if($item->url)<a class="timetable__link" href="{{$item->url}}" target="_blank">{{$item->title}}</a><br>@endif
                                    @if($item->file)<a class="timetable__link" target="_blank" href="{{asset($item->file)}}">{{$item->title}}</a><br>@endif
                                @endforeach
                            </div>

                            <div class="timetable__block-magistr height">
                            <span> Магистратура, очно-заочная форма:</span><br>
    {{--                            <a class="timetable__link" href="">Зимняя зачетно-экзаменнационная сессия, 2020-2021</a> --}}
                                @foreach($exam_half_mt as $key => $item)
                                    @if($item->url)<a class="timetable__link" href="{{$item->url}}" target="_blank">{{$item->title}}</a>@endif
                                    @if($item->file)<a class="timetable__link" target="_blank" href="{{asset($item->file)}}">{{$item->title}}</a>@endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>
@endsection
