<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ДГУ ФИиИТ</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    @include('users.layouts.styles')

</head>
<body>
    <div class="main-page">
        @include('users.layouts.header')
        @yield("content")
        @include('users.layouts.footer')
        @include('users.layouts.scripts')
        @section('scripts')
        @show
    </div>

</body>
</html>
