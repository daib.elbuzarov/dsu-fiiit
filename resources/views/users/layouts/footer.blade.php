<footer class="footer">
            <div class="footer__content">
                <img src="{{asset('/icons/index/icons1.svg')}}" alt="" class="footer__icons1">
                <img src="{{asset('/icons/index/icons2.svg')}}" alt="" class="footer__icons2">
                <div class="footer__content-feedback">
                    <div class="footer__feedback-faculty">Факультет Информатики и IT-технологий</div>
                    <img src="{{asset('/icons/index/faces.svg')}}" alt="" class="footer__content-logos">
                    <div class="footer__feedback-social">
                        <img src="{{asset('/icons/index/inst.svg')}}" alt="inst" class="footer__feedback-inst">
                        <img src="{{asset('/icons/index/vk.svg')}}" alt="vk" class="footer__feedback-vk">
                    </div>
                    <div class="footer__feedback-tel">Телефон: 675910, 1-03</div>
                    <div class="footer__feedback-adress">РД, г. Махачкала, ул. Дзержинского 12</div>
                </div>
                <div class="footer__content-category">
                    <div class="footer__variant">
                        <ul class="footer__variant-list">
                            <li class="footer__variant-title"><a href="{{route('structure')}}">Факультет</a></li>

                            <li class="footer__variant-item"><a href="{{route('structure')}}">Структура факультета</a></li>
                            <li class="footer__variant-item"><a href="{{route('news')}}">Новости и объявления</a></li>
                            <li class="footer__variant-item"><a href="">Блог</a></li>
                        </ul>
                        <ul class="footer__variant-list" id="second">
                            <li class="footer__variant-title"><a href="{{route('timetable')}}">Студентам</a></li>

                            <li class="footer__variant-item"><a href="{{route('timetable')}}">Расписание</a></li>
                            <li class="footer__variant-item"><a href="{{route('resources')}}">Учебные ресурсы</a></li>
                            <li class="footer__variant-item"><a href="{{route('document')}}">Образцы документов</a></li>
                        </ul>
                        <ul class="footer__variant-list" id="third">
                            <li class="footer__variant-title"><a href="{{route('candidate')}}">Абитуриентам</a></li>

                            <li class="footer__variant-item"><a href="{{route('candidate')}}">Бакалавриат, магистратура</a></li>
                            <li class="footer__variant-item"><a href="{{route('candidate.programs')}}">Программы образования</a></li>
                        </ul>
                        <ul class="footer__variant-list" id="fourth">
                            <li class="footer__variant-title" id="title4"><a href="{{route('social.sd')}}">Социальная работа</a></li>

                            <li class="footer__variant-item"><a href="{{route('social.sd')}}">Студенческий деканат</a></li>
                            <li class="footer__variant-item"><a href="{{route('social.kdm')}}">КДМ ФИиИТ</a></li>
                            <li class="footer__variant-item"><a href="{{route('social.pk')}}">Профбюро ФИиИТ</a></li>
                        </ul>
                        <ul class="footer__variant-list" id="fiveth">
                            <li class="footer__variant-title" id="title5"><a href="{{route('science.sno')}}">Научная деятельность</a></li>

                            <li class="footer__variant-item"><a href="{{route('science.sno')}}">СНО</a></li>
                            <li class="footer__variant-item"><a href="{{route('science.smu')}}">Совет молодых ученых</a></li>
                            <li class="footer__variant-item science-item"><a href="{{route('science.mugs')}}">Научные кружки</a></li>
                            <li class="footer__variant-item science-item"><a>Конкурсы и гранты</a></li>
                            <li class="footer__variant-item science-item"><a href="{{route('science.projects')}}">Проектная деятельность</a></li>
                        </ul>
                    </div>
                </div>
                <img src="{{asset('/icons/index/faces.svg')}}" alt="" class="footer__content-logo">
            </div>
    </footer>
