<link href="{{asset('/css/fonts.css')}}" rel="stylesheet">
<link href="{{asset('/css/general.css')}}" rel="stylesheet">
<link href="{{asset('/css/bachelor.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('/css/main.css')}}">
<link rel="stylesheet" href="{{asset('/css/timetable.css')}}">
<link rel="stylesheet" href="{{asset('/css/resources.css')}}">
<link rel="stylesheet" href="{{asset('/css/document.css')}}">
<link href="{{asset('/css/structure.css')}}" rel="stylesheet">
<link href="{{asset('/css/search.css')}}" rel="stylesheet">

