<header>
    <div class="header__block">
        <div class="container">
			<div class="header__content" id="fullscreen">
				<a href="{{route('home')}}"><img src="{{asset('/icons/index/logo.svg')}}" alt="logo" class="header_img"></a>
				<div class="main__header">
					<div class="header-faculty">
						<a id="head1" href="{{route('structure')}}" class="header__title">Факультет</a>
						<ul class="subheader_menu faculty">
							<li class="subheader_menu_item"><a href="{{route('structure')}}">Структура факультета</a></li>
							<li class="subheader_menu_item"><a href="{{route('news')}}">Новости и объявления</a></li>
						</ul>

						<ul class="subheader_menu main">
							<li class="subheader_menu_item"><a href="{{route('home')}}">На главную</a></li> 
							<li class="subheader_menu_item"><a href="{{route('news')}}">Новости и объявления</a></li>
							<li class="subheader_menu_item"><a href="{{route('timetable')}}">Расписание</a></li>
						</ul>
					</div>

					<div class="header-bachelor">
						<a id="head2" href="{{route('candidate')}}" class="header__title">Абитуриентам</a>
						<ul class="subheader_menu bachelor">
							<li class="subheader_menu_item"><a href="{{route('candidate')}}">Бакалавриат и магистратура</a></li>
							<li class="subheader_menu_item"><a href="{{route('candidate.programs')}}">Образовательные программы</a></li>
						</ul>
					</div>

					<div class="header-student">
						<a id="head3" href="{{route('timetable')}}" class="header__title">Студентам</a>
						<ul class="subheader_menu student">
							<li class="subheader_menu_item"><a href="{{route('timetable')}}">Расписание</a></li>
                			<li class="subheader_menu_item"><a href="{{route('resources')}}">Учебные ресурсы</a></li>
                			<li class="subheader_menu_item"><a href="{{route('document')}}">Образцы документов</a></li>
						</ul>
					</div>

					<div class="header-social">
						<a id="head4" href="{{route('social.sd')}}" class="header__title">Социальная работа</a>
						<ul class="subheader_menu social">
							<li class="subheader_menu_item"><a href="{{route('social.sd')}}">Студенческий деканат</a></li>
							<li class="subheader_menu_item"><a href="{{route('social.kdm')}}">Комитет по делам молодежи</a></li>
							<li class="subheader_menu_item"><a href="{{route('social.pk')}}">Профсоюзный комитет</a></li>
						</ul>
					</div>

					<div class="header-science">
						<a id="head5" href="{{route('science.sno')}}" class="header__title">Научная деятельность</a>
						<ul class="subheader_menu study">
							<li class="subheader_menu_item"><a href="{{route('science.sno')}}">Студенческое научное общество</a></li>
							<li class="subheader_menu_item"><a href="{{route('science.smu')}}">Совет молодых ученых</a></li>
							<li class="subheader_menu_item"><a href="{{route('science.mugs')}}">Научные кружки</a></li>
							<li class="subheader_menu_item"><a href="{{route('science.contests')}}">Конкурсы и гранты</a></li>
							<li class="subheader_menu_item"><a href="{{route('science.projects')}}">Проектная деятельность</a></li>
						</ul>
					</div>
				</div>
				<a href="{{route('search')}}"><img src="{{asset('/icons/index/search2.svg')}}" alt="" class="header_search"></a>
			</div>

            <div class="ham__menu">
                    <span id="ham1" class="hamburger other"></span>
                    <span id="ham2" class="hamburger other"></span>
                    <span id="ham3" class="hamburger other"></span>
            </div>

            <div class="header__content" id="mobile-screen">
				<div class="main__header">
					<div id="mainBlock" class="header-main">
						<a id="title6" href="{{route('structure')}}" class="header__title">Главная</a>
						<ul id="main" class="subheader_menu main">
							<li class="subheader_menu_item"><a href="{{route('home')}}">На главную</a></li> 
							<li class="subheader_menu_item"><a href="{{route('news')}}">Новости и объявления</a></li>
							<li class="subheader_menu_item"><a href="{{route('timetable')}}">Расписание</a></li>
						</ul>
					</div>
					<div id="facBlock" class="header-faculty">
						<a id="title1" href="{{route('structure')}}" class="header__title">Факультет</a>
						<ul id="fac" class="subheader_menu faculty">
							<li class="subheader_menu_item"><a href="{{route('structure')}}">Структура факультета</a></li>
							<li class="subheader_menu_item"><a href="{{route('news')}}">Новости и объявления</a></li>
						</ul>

						<ul class="subheader_menu main">
							<li class="subheader_menu_item"><a href="{{route('news')}}">Новости и объявления</a></li>
							<li class="subheader_menu_item"><a href="{{route('timetable')}}">Расписание</a></li>
							<li class="subheader_menu_item"><a href="">ФИ&ИТ в лицах</a></li> 
						</ul>
					</div>

					<div id="bachBlock" class="header-bachelor">
						<a id="title2" href="{{route('candidate')}}" class="header__title">Абитуриентам</a>
						<ul id="bach" class="subheader_menu bachelor">
							<li class="subheader_menu_item"><a href="{{route('candidate')}}">Бакалавриат и магистратура</a></li>
							<li class="subheader_menu_item"><a href="{{route('candidate.programs')}}">Образовательные программы</a></li>
						</ul>
					</div>

					<div id="studBlock" class="header-student">
						<a id="title3" href="{{route('timetable')}}" class="header__title">Студентам</a>
						<ul id="stud" class="subheader_menu student">
							<li class="subheader_menu_item"><a href="{{route('timetable')}}">Расписание</a></li>
                			<li class="subheader_menu_item"><a href="{{route('resources')}}">Учебные ресурсы</a></li>
                			<li class="subheader_menu_item"><a href="{{route('document')}}">Образцы документов</a></li>
						</ul>
					</div>

					<div id="socBlock" class="header-social">
						<a id="title4" href="{{route('social.sd')}}" class="header__title">Социальная работа</a>
						<ul id="soc" class="subheader_menu social">
							<li class="subheader_menu_item"><a href="{{route('social.sd')}}">Студенческий деканат</a></li>
							<li class="subheader_menu_item"><a href="{{route('social.kdm')}}">Комитет по делам молодежи</a></li>
							<li class="subheader_menu_item"><a href="{{route('social.pk')}}">Профсоюзный комитет</a></li>
						</ul>
					</div>

					<div id="scienceBlock" class="header-science">
						<a id="title5" href="{{route('science.sno')}}" class="header__title">Научная деятельность</a>
						<ul id="scien" class="subheader_menu study">
							<li class="subheader_menu_item"><a href="{{route('science.sno')}}">Студенческое научное общество</a></li>
							<li class="subheader_menu_item"><a href="{{route('science.smu')}}">Совет молодых ученых</a></li>
							<li class="subheader_menu_item"><a href="{{route('science.mugs')}}">Научные кружки</a></li>
							<li class="subheader_menu_item"><a href="{{route('science.contests')}}">Конкурсы и гранты</a></li>
							<li class="subheader_menu_item"><a href="{{route('science.projects')}}">Проектная деятельность</a></li>
						</ul>
					</div>
				</div>
			</div>
            <a href="{{route('search')}}" id="search"><img src="{{asset('/icons/index/search2.svg')}}" alt="" class="header_search"></a>
        </div>
    </div>    
</header>