// появление настроек поиска
function showSearchSettings() {
    let searchSettings = document.querySelector('#search_settings')
    searchSettings.style.display = 'flex'
    searchSettings.style.visibility = 'visible'
    searchSettings.style.opacity = '1'
}

// на мобилках
function showSearchSettingsMobile() {
    let searchSettings = document.querySelector('#search_settings')
    let menuBack = document.querySelector('#menu_back')
    menuBack.style.display = 'flex'
    searchSettings.style.visibility = 'visible'
    searchSettings.style.opacity = '1'
    searchSettings.style.transform = 'translateX(0)'
}

// закрытие настроек поиска
function closeSearchSettings() {
    let searchSettings = document.querySelector('#search_settings')

    searchSettings.style.opacity = '0'
    searchSettings.style.visibility = 'hidden'
}

// на мобилках
function closeSearchSettingsMobile() {
    let searchSettings = document.querySelector('#search_settings')
    let menuBack = document.querySelector('#menu_back')

    menuBack.style.display = 'none'
    if (window.innerWidth < 768) {
        searchSettings.style.transform = 'translateX(100%)'
    }
}


// раскрытие гармошки
function acItemShow(item) {
    let itemContent = item.parentNode.querySelector('.ac-item-content');
    itemContent.style.visibility = 'visible';
    itemContent.style.height = 'auto';
    itemContent.style.padding = '16px 0 0 0';
    item.removeAttribute('onclick');
    item.setAttribute('onclick', 'acItemClose(this)');
}

// закрытие гармошки
function acItemClose(item) {
    let itemContent = item.parentNode.querySelector('.ac-item-content');
    itemContent.style.height = '0px';
    itemContent.style.visibility = 'hidden';
    itemContent.style.padding = '0';
    item.removeAttribute('onclick');
    item.setAttribute('onclick', 'acItemShow(this)');
}
