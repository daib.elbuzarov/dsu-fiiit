window.addEventListener('DOMContentLoaded', () => {
    
    const headers = document.querySelectorAll('.section-three__header');
    const descriptions = document.querySelectorAll('.section-three__description');

    headers.forEach((header,index) => {
        header.addEventListener('click', () => {
            const description = descriptions[index];
            if(description.classList.contains('section-three__description_show')) {
                description.classList.remove('section-three__description_show');
            } else {
                description.classList.add('section-three__description_show');
            }
        });
    });

});