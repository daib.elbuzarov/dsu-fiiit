"use strict";

window.addEventListener('DOMContentLoaded', () => {

    const tabs = document.querySelectorAll('.section-two__tab');
    const tabsContent = document.querySelectorAll('.section-three');
    const pagination = document.querySelectorAll('.section-four');
            
    hideTabsContent();
    showTabsContent();

    tabs.forEach(tab => {
        tab.addEventListener('click', (event) => {
            tabs.forEach((item, index) => {
                if (item == tab) {
                    hideTabsContent();
                    showTabsContent(index);
                }
            });
        });
    });

    function hideTabsContent() {
        tabs.forEach( item => {
            item.classList.remove('section-two__tab_active');
        });

        tabsContent.forEach( item => {
            item.classList.remove('section-three_show');
        });

        pagination.forEach( item => {
            item.classList.remove('section-four_show');
        });
    }

    function showTabsContent(index=0) {
        tabs[index].classList.add('section-two__tab_active');
        tabsContent[index].classList.add('section-three_show');
        pagination[index].classList.add('section-four_show');
    }


});