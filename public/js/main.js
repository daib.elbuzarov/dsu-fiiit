// subheader menu generation




const btnFaculty = document.getElementById('head1'),
      btnBachelor = document.getElementById('head2'),
      btnStudent = document.getElementById('head3'),
      btnSocial = document.getElementById('head4'),
      btnScience = document.getElementById('head5'),

      main = document.querySelector('.main'),
      faculty = document.querySelector('.faculty'),
      student = document.querySelector('.student'),
      social = document.querySelector('.social'),
      science = document.querySelector('.study'),
      bachelor = document.querySelector('.bachelor'); 

      if (window.location.href == "http://iit.dgu.ru/") {
            btnFaculty.addEventListener("mouseover", function() {
                  faculty.style.display = "flex";
                  main.style.display = 'none';
                  bachelor.style.display = "none";
                  student.style.display = "none";
                  social.style.display = "none";
                  science.style.display = "none";
            });
      
            faculty.addEventListener("mouseover", function() {
                  faculty.style.display = "flex";
                  main.style.display = 'none';
                  this.addEventListener('mouseout', function() {
                        faculty.style.display = "none";
                        main.style.display = 'flex';
                  });  
            });
      
            btnBachelor.addEventListener("mouseover", function() {
                  bachelor.style.display = "flex";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  student.style.display = "none";
                  social.style.display = "none";
                  science.style.display = "none";
            });
      
            bachelor.addEventListener("mouseover", function() {
                  bachelor.style.display = "flex";
                  main.style.display = 'none';
                  faculty.style.display = 'none';
                  science.style.display = "none";
                  this.addEventListener('mouseout', function() {
                        bachelor.style.display = "none";
                        main.style.display = 'flex';
                  });  
            });
      
            btnStudent.addEventListener("mouseover", function() {
                  student.style.display = "flex";
                  bachelor.style.display = "none";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  social.style.display = "none";
                  science.style.display = "none";
            });
      
            student.addEventListener("mouseover", function() {
                  student.style.display = "flex";
                  bachelor.style.display = "none";
                  main.style.display = 'none';
                  faculty.style.display = 'none';
                  social.style.display = "none";
                  science.style.display = "none";
                  this.addEventListener('mouseout', function() {
                        student.style.display = "none";
                        main.style.display = 'flex';
                  });  
            });
      
            btnSocial.addEventListener("mouseover", function() {
                  social.style.display = "flex";
                  bachelor.style.display = "none";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  student.style.display = "none";
                  science.style.display = "none";
            });
      
            social.addEventListener("mouseover", function() {
                  social.style.display = "flex";
                  bachelor.style.display = "none";
                  main.style.display = 'none';
                  faculty.style.display = 'none';
                  science.style.display = "none";
                  this.addEventListener('mouseout', function() {
                        social.style.display = "none";
                        main.style.display = 'flex';
                  });  
            });
      
            btnScience.addEventListener("mouseover", function() {
                  science.style.display = "flex";
                  bachelor.style.display = "none";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  student.style.display = "none";
                  social.style.display = "none";
            });
      
            science.addEventListener("mouseover", function() {
                  science.style.display = "flex";
                  bachelor.style.display = "none";
                  main.style.display = 'none';
                  faculty.style.display = 'none';
                  this.addEventListener('mouseout', function() {
                        science.style.display = "none";
                        main.style.display = 'flex';
                  });  
            });    
      }

      if (window.location.href == "http://iit.dgu.ru/structure" || window.location.href == "http://iit.dgu.ru/news") {
            main.style.display = "none";
            faculty.style.display = "flex";

            btnFaculty.addEventListener("mouseover", function() {
                  faculty.style.display = "flex";
                  main.style.display = 'none';
                  bachelor.style.display = "none";
                  student.style.display = "none";
                  social.style.display = "none";
                  science.style.display = "none";
            });
      
            faculty.addEventListener("mouseover", function() {
                  faculty.style.display = "flex";
                  this.addEventListener('mouseout', function() {
                        faculty.style.display = "flex";
                  });  
            });
      
            btnBachelor.addEventListener("mouseover", function() {
                  bachelor.style.display = "flex";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  student.style.display = "none";
                  social.style.display = "none";
                  science.style.display = "none";
            });
      
            bachelor.addEventListener("mouseover", function() {
                  bachelor.style.display = "flex";
                  main.style.display = 'none';
                  faculty.style.display = 'none';
                  science.style.display = "none";
                  this.addEventListener('mouseout', function() {
                        bachelor.style.display = "none";
                        faculty.style.display = 'flex';
                  });  
            });
      
            btnStudent.addEventListener("mouseover", function() {
                  student.style.display = "flex";
                  bachelor.style.display = "none";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  social.style.display = "none";
                  science.style.display = "none";
            });
      
            student.addEventListener("mouseover", function() {
                  student.style.display = "flex";
                  bachelor.style.display = "none";
                  main.style.display = 'none';
                  faculty.style.display = 'none';
                  social.style.display = "none";
                  science.style.display = "none";
                  this.addEventListener('mouseout', function() {
                        student.style.display = "none";
                        faculty.style.display = 'flex';
                  });  
            });
      
            btnSocial.addEventListener("mouseover", function() {
                  social.style.display = "flex";
                  bachelor.style.display = "none";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  student.style.display = "none";
                  science.style.display = "none";
            });
      
            social.addEventListener("mouseover", function() {
                  social.style.display = "flex";
                  bachelor.style.display = "none";
                  main.style.display = 'none';
                  faculty.style.display = 'none';
                  science.style.display = "none";
                  this.addEventListener('mouseout', function() {
                        social.style.display = "none";
                        faculty.style.display = 'flex';
                  });  
            });
      
            btnScience.addEventListener("mouseover", function() {
                  science.style.display = "flex";
                  bachelor.style.display = "none";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  student.style.display = "none";
                  social.style.display = "none";
            });
      
            science.addEventListener("mouseover", function() {
                  science.style.display = "flex";
                  bachelor.style.display = "none";
                  main.style.display = 'none';
                  faculty.style.display = 'none';
                  this.addEventListener('mouseout', function() {
                        science.style.display = "none";
                        faculty.style.display = 'flex';
                  });  
            });    
      }

      if (window.location.href == "http://iit.dgu.ru/candidate/programs" || window.location.href == "http://iit.dgu.ru/candidate") {
            main.style.display = "none";
            bachelor.style.display = "flex";

            btnFaculty.addEventListener("mouseover", function() {
                  faculty.style.display = "flex";
                  main.style.display = 'none';
                  bachelor.style.display = "none";
                  student.style.display = "none";
                  social.style.display = "none";
                  science.style.display = "none";
            });
      
            faculty.addEventListener("mouseover", function() {
                  faculty.style.display = "flex";
                  main.style.display = 'none';
                  bachelor.style.display = 'none';
                  this.addEventListener('mouseout', function() {
                        faculty.style.display = "none";
                        bachelor.style.display = 'flex';
                  });  
            });
      
            btnBachelor.addEventListener("mouseover", function() {
                  bachelor.style.display = "flex";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  student.style.display = "none";
                  social.style.display = "none";
                  science.style.display = "none";
            });
      
            bachelor.addEventListener("mouseover", function() {
                  bachelor.style.display = "flex";
                  main.style.display = 'none';
                  faculty.style.display = 'none';
                  science.style.display = "none";
                  this.addEventListener('mouseout', function() {
                        bachelor.style.display = "none";
                        bachelor.style.display = 'flex';
                  });  
            });
      
            btnStudent.addEventListener("mouseover", function() {
                  student.style.display = "flex";
                  bachelor.style.display = "none";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  social.style.display = "none";
                  science.style.display = "none";
            });
      
            student.addEventListener("mouseover", function() {
                  student.style.display = "flex";
                  bachelor.style.display = "none";
                  main.style.display = 'none';
                  faculty.style.display = 'none';
                  social.style.display = "none";
                  science.style.display = "none";
                  this.addEventListener('mouseout', function() {
                        student.style.display = "none";
                        bachelor.style.display = 'flex';
                  });  
            });
      
            btnSocial.addEventListener("mouseover", function() {
                  social.style.display = "flex";
                  bachelor.style.display = "none";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  student.style.display = "none";
                  science.style.display = "none";
            });
      
            social.addEventListener("mouseover", function() {
                  social.style.display = "flex";
                  bachelor.style.display = "none";
                  main.style.display = 'none';
                  faculty.style.display = 'none';
                  science.style.display = "none";
                  this.addEventListener('mouseout', function() {
                        social.style.display = "none";
                        bachelor.style.display = 'flex';
                  });  
            });
      
            btnScience.addEventListener("mouseover", function() {
                  science.style.display = "flex";
                  bachelor.style.display = "none";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  student.style.display = "none";
                  social.style.display = "none";
            });
      
            science.addEventListener("mouseover", function() {
                  science.style.display = "flex";
                  bachelor.style.display = "none";
                  main.style.display = 'none';
                  faculty.style.display = 'none';
                  this.addEventListener('mouseout', function() {
                        science.style.display = "none";
                        bachelor.style.display = 'flex';
                  });  
            });    
      }

      if (window.location.href == "http://iit.dgu.ru/student/timetable" || window.location.href == "http://iit.dgu.ru/student/resources" || window.location.href == "http://iit.dgu.ru/student/document") {
            main.style.display = 'none';
            student.style.display = "flex";

            btnFaculty.addEventListener("mouseover", function() {
                  faculty.style.display = "flex";
                  main.style.display = 'none';
                  bachelor.style.display = "none";
                  student.style.display = "none";
                  social.style.display = "none";
                  science.style.display = "none";
            });
      
            faculty.addEventListener("mouseover", function() {
                  faculty.style.display = "flex";
                  student.style.display = 'none';
                  this.addEventListener('mouseout', function() {
                        faculty.style.display = "none";
                        student.style.display = 'flex';
                  });  
            });
      
            btnBachelor.addEventListener("mouseover", function() {
                  bachelor.style.display = "flex";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  student.style.display = "none";
                  social.style.display = "none";
                  science.style.display = "none";
            });
      
            bachelor.addEventListener("mouseover", function() {
                  bachelor.style.display = "flex";
                  student.style.display = "none";
                  this.addEventListener('mouseout', function() {
                        bachelor.style.display = "none";
                        student.style.display = 'flex';
                  });  
            });
      
            btnStudent.addEventListener("mouseover", function() {
                  student.style.display = "flex";
                  bachelor.style.display = "none";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  social.style.display = "none";
                  science.style.display = "none";
            });
      
            student.addEventListener("mouseover", function() {
                  student.style.display = "flex";
                  bachelor.style.display = "none";
                  main.style.display = 'none';
                  faculty.style.display = 'none';
                  social.style.display = "none";
                  science.style.display = "none";
                  this.addEventListener('mouseout', function() {
                        student.style.display = "none";
                        student.style.display = 'flex';
                  });  
            });
      
            btnSocial.addEventListener("mouseover", function() {
                  social.style.display = "flex";
                  bachelor.style.display = "none";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  student.style.display = "none";
                  science.style.display = "none";
            });
      
            social.addEventListener("mouseover", function() {
                  social.style.display = "flex";
                  student.style.display = "none";
                  this.addEventListener('mouseout', function() {
                        social.style.display = "none";
                        student.style.display = 'flex';
                  });  
            });
      
            btnScience.addEventListener("mouseover", function() {
                  science.style.display = "flex";
                  bachelor.style.display = "none";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  student.style.display = "none";
                  social.style.display = "none";
            });
      
            science.addEventListener("mouseover", function() {
                  science.style.display = "flex";
                  student.style.display = 'none';
                  this.addEventListener('mouseout', function() {
                        science.style.display = "none";
                        student.style.display = 'flex';
                  });  
            });    
      }

      if (window.location.href == "http://iit.dgu.ru/social/sd" || window.location.href == "http://iit.dgu.ru/social/pk" || window.location.href == "http://iit.dgu.ru/social/kdm") {
            social.style.display = "flex";
            main.style.display = 'none';

            btnFaculty.addEventListener("mouseover", function() {
                  faculty.style.display = "flex";
                  main.style.display = 'none';
                  bachelor.style.display = "none";
                  student.style.display = "none";
                  social.style.display = "none";
                  science.style.display = "none";
            });
      
            faculty.addEventListener("mouseover", function() {
                  social.style.display = 'none';
                  faculty.style.display = "flex";
                  main.style.display = 'none';
                  this.addEventListener('mouseout', function() {
                        faculty.style.display = "none";
                        social.style.display = 'flex';
                  });  
            });
      
            btnBachelor.addEventListener("mouseover", function() {
                  bachelor.style.display = "flex";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  student.style.display = "none";
                  social.style.display = "none";
                  science.style.display = "none";
            });
      
            bachelor.addEventListener("mouseover", function() {
                  social.style.display = 'none';
                  bachelor.style.display = "flex";
                  this.addEventListener('mouseout', function() {
                        bachelor.style.display = "none";
                        social.style.display = 'flex';
                  });  
            });
      
            btnStudent.addEventListener("mouseover", function() {
                  student.style.display = "flex";
                  bachelor.style.display = "none";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  social.style.display = "none";
                  science.style.display = "none";
            });
      
            student.addEventListener("mouseover", function() {
                  student.style.display = "flex";
                  bachelor.style.display = "none";
                  main.style.display = 'none';
                  faculty.style.display = 'none';
                  social.style.display = "none";
                  science.style.display = "none";
                  this.addEventListener('mouseout', function() {
                        student.style.display = "none";
                        social.style.display = 'flex';
                  });  
            });
      
            btnSocial.addEventListener("mouseover", function() {
                  social.style.display = "flex";
                  bachelor.style.display = "none";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  student.style.display = "none";
                  science.style.display = "none";
            });
      
            social.addEventListener("mouseover", function() {
                  social.style.display = "flex";
                  bachelor.style.display = "none";
                  main.style.display = 'none';
                  faculty.style.display = 'none';
                  science.style.display = "none";
                  this.addEventListener('mouseout', function() {
                        social.style.display = "none";
                        social.style.display = 'flex';
                  });  
            });
      
            btnScience.addEventListener("mouseover", function() {
                  science.style.display = "flex";
                  bachelor.style.display = "none";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  student.style.display = "none";
                  social.style.display = "none";
            });
      
            science.addEventListener("mouseover", function() {
                  science.style.display = "flex";
                  social.style.display = "none";
                  this.addEventListener('mouseout', function() {
                        science.style.display = "none";
                        social.style.display = 'flex';
                  });  
            });    
      }

      if (window.location.href == "http://iit.dgu.ru/science/sno" || window.location.href == "http://iit.dgu.ru/science/smu" || window.location.href == "http://iit.dgu.ru/science/mugs" || window.location.href == "http://iit.dgu.ru/science/contests" || window.location.href == "http://iit.dgu.ru/science/projects") {
            main.style.display = 'none';
            science.style.display = "flex";

            btnFaculty.addEventListener("mouseover", function() {
                  faculty.style.display = "flex";
                  main.style.display = 'none';
                  bachelor.style.display = "none";
                  student.style.display = "none";
                  social.style.display = "none";
                  science.style.display = "none";
            });
      
            faculty.addEventListener("mouseover", function() {
                  science.style.display = 'none';
                  faculty.style.display = "flex";
                  this.addEventListener('mouseout', function() {
                        faculty.style.display = "none";
                        science.style.display = 'flex';
                  });  
            });
      
            btnBachelor.addEventListener("mouseover", function() {
                  bachelor.style.display = "flex";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  student.style.display = "none";
                  social.style.display = "none";
                  science.style.display = "none";
            });
      
            bachelor.addEventListener("mouseover", function() {
                  science.style.display = 'none';
                  bachelor.style.display = "flex";
                  this.addEventListener('mouseout', function() {
                        bachelor.style.display = "none";
                        science.style.display = 'flex';
                  });  
            });
      
            btnStudent.addEventListener("mouseover", function() {
                  student.style.display = "flex";
                  bachelor.style.display = "none";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  social.style.display = "none";
                  science.style.display = "none";
            });
      
            student.addEventListener("mouseover", function() {
                  science.style.display = 'none';
                  student.style.display = "flex";
                  this.addEventListener('mouseout', function() {
                        student.style.display = "none";
                        science.style.display = 'flex';
                  });  
            });
      
            btnSocial.addEventListener("mouseover", function() {
                  social.style.display = "flex";
                  bachelor.style.display = "none";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  student.style.display = "none";
                  science.style.display = "none";
            });
      
            social.addEventListener("mouseover", function() {
                  science.style.display = 'none';
                  social.style.display = "flex";
                  this.addEventListener('mouseout', function() {
                        social.style.display = "none";
                        science.style.display = 'flex';
                  });  
            });
      
            btnScience.addEventListener("mouseover", function() {
                  science.style.display = "flex";
                  bachelor.style.display = "none";
                  faculty.style.display = "none";
                  main.style.display = "none";
                  student.style.display = "none";
                  social.style.display = "none";
            });
      
            science.addEventListener("mouseover", function() {
                  science.style.display = "flex";
                  this.addEventListener('mouseout', function() {
                        science.style.display = "none";
                        science.style.display = 'flex';
                  });  
            });    
      }
      
      
const fac = document.getElementById('fac'),      
      bach = document.getElementById('bach'),
      stud = document.getElementById('stud'),
      soc = document.getElementById('soc'),
      scien = document.getElementById('scien'),
      mainHeader = document.getElementById('main'),

      ham1 = document.getElementById('ham1'),
      ham2 = document.getElementById('ham2'),
      ham3 = document.getElementById('ham3'),

      hamMenu = document.querySelector(".ham__menu"),
      headerContent = document.getElementById("mobile-screen"),

      headerBlock1 = document.getElementById("facBlock"),
      headerBlock2 = document.getElementById("bachBlock"),
      headerBlock3 = document.getElementById("studBlock"),
      headerBlock4 = document.getElementById("socBlock"),
      headerBlock5 = document.getElementById("scienceBlock"),


      title1 = document.getElementById('title1'),
      title2 = document.getElementById('title2'),
      title3 = document.getElementById('title3'),
      title4 = document.getElementById('title4'),
      title5 = document.getElementById('title5'),
      title6 = document.getElementById('title6');


      title1.addEventListener("click", function (e) {
            e.preventDefault(); 
            headerBlock2.classList.toggle("bachelor__block-active");
            fac.classList.toggle("sub__active");   
            fac.classList.toggle("fac__active"); 
      });

      title2.addEventListener("click", function (e) {
            e.preventDefault();   
            bach.classList.toggle("sub__active"); 
            headerBlock3.classList.toggle("student__block-active");
            bach.classList.toggle("bach__active");

      });

      title3.addEventListener("click", function (e) {
            e.preventDefault();   
            stud.classList.toggle("sub__active"); 
            headerBlock4.classList.toggle("social__block-active");
            stud.classList.toggle("stud__active"); 
      });

      title4.addEventListener("click", function (e) {
            e.preventDefault();   
            soc.classList.toggle("sub__active"); 
            headerBlock5.classList.toggle("science__block-active");
            soc.classList.toggle("soc__active");  
      });

      title5.addEventListener("click", function (e) {
            scien.classList.toggle("sub__active");   
            e.preventDefault(); 
            scien.classList.toggle("science__active");  
      });

      hamMenu.addEventListener("click", () => {
            headerContent.classList.toggle("header__content-active");
            ham1.classList.toggle("ham1__active");
            ham2.classList.toggle("ham2__active");
            ham3.classList.toggle("ham3__active");
      });

      title6.addEventListener("click", function (e) {
            e.preventDefault();
            headerBlock1.classList.toggle("faculty__block-active");
            mainHeader.classList.toggle("sub__active"); 
            mainHeader.classList.toggle("main__active"); 
      });

     

      

const competition = document.querySelector('.contest__titles-competition'),
      grant = document.querySelector('.contest__titles-grant'),
      competitionBlock = document.querySelector('.contest__competition'),
      grantBlock = document.querySelector('.contest__grant');

      grant.addEventListener("click", function() {
      competitionBlock.style.display = "none";
      grantBlock.style.display = "flex";
      document.querySelector('.black').classList.add('black__active');   
      competition.style.opacity = "0.24";
      grant.style.opacity = "1";   
      });

      competition.addEventListener("click", function() {
            grantBlock.style.display = "none";
            competitionBlock.style.display = "flex"; 
            document.querySelector('.black').classList.remove('black__active');
            competition.style.opacity = "1";
            grant.style.opacity = "0.24"; 
      });