
const sessionBtn = document.querySelector('.timetable__titles-session'),
      lessonsBtn = document.querySelector('.timetable__titles-lessons'),
      sessionBlock = document.querySelector('.timetable__education-session'),
      lessonsBlock = document.querySelector('.timetable__education-lessons'),
      line = document.querySelector('.timetable__black');  


sessionBtn.addEventListener('click', function() {
    sessionBtn.style.opacity = "1";
    lessonsBtn.style.opacity = "0.24";
    lessonsBlock.style.display = "none";
    sessionBlock.style.display = "block";
    line.classList.add('timetable__black-active');
});

lessonsBtn.addEventListener('click', function() {
    sessionBtn.style.opacity = "0.24";
    lessonsBtn.style.opacity = "1";
    lessonsBlock.style.display = "block";
    sessionBlock.style.display = "none";
    line.classList.remove('timetable__black-active');
});